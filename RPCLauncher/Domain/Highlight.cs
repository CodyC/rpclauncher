﻿using System;
using System.Collections.Generic;

namespace RPCLauncher.Domain
{
    public class Highlight
    {
        public String Id { get; set; }
        public long UnixTimestamp { get; set; }
        public String HighlightText { get; set; }
        public String Notes { get; set; }
        public String Source { get; set; }
        public List<String> Tags { get; set; }
    }
}
