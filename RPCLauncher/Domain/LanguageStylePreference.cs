﻿using System;
using System.Collections.Generic;

namespace RPCLauncher.Domain
{
    public class LanguageStylePreference
    {
        public String LanguageName { get; set; }
        public String BraceStyleKey { get; set; }
        public String SpacingStyleKey { get; set; }
        public List<String> AvailableLibraries { get; set; }
    }
}
