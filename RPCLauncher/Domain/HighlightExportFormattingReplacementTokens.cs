﻿using System;

namespace RPCLauncher.Domain
{
    public class HighlightExportFormattingReplacementTokens
    {
        public const String ID_REPLACEMENT_TOKEN = "{Id}";
        public const String TIMESTAMP_REPLACEMENT_TOKEN = "{Timestamp}";
        public const String HIGHLIGHT_REPLACEMENT_TOKEN = "{Highlight}";
        public const String NOTES_REPLACEMENT_TOKEN = "{Notes}";
        public const String SOURCE_REPLACEMENT_TOKEN = "{Source}";
        public const String TAGS_REPLACEMENT_TOKEN = "{Tags}";

    }
}
