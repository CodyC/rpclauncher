﻿using System;
using System.Collections.Generic;

namespace RPCLauncher.Domain
{
    public class BraceStyles
    {
        public const String NEXT_LINE = "NEXT_LINE";
        public const String NEXT_LINE_DISPLAY_VALUE = "Next Line";

        public const String SAME_LINE = "SAME_LINE";
        public const String SAME_LINE_DISPLAY_VALUE = "Same Line";

        public static List<KeyValuePair<String, String>> GetAllBraceStylesValueAndDisplayValuePairs()
        {
            var keyValuePairs = new List<KeyValuePair<String, String>>();
            keyValuePairs.Add(new KeyValuePair<String, String>(NEXT_LINE, NEXT_LINE_DISPLAY_VALUE));
            keyValuePairs.Add(new KeyValuePair<String, String>(SAME_LINE, SAME_LINE_DISPLAY_VALUE));

            return keyValuePairs;
        }

        public static List<String> GetAllBraceStyleDisplayValues()
        {
            return
                new List<string>()
                {
                    NEXT_LINE_DISPLAY_VALUE,
                    SAME_LINE_DISPLAY_VALUE
                };
        }
    }
}
