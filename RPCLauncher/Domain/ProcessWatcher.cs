﻿using RPCLauncher.Utils;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Timers;

namespace RPCLauncher.Domain
{
    public static class ProcessWatcher
    {
        private static Timer timer;
        private static IntPtr previousHandle = IntPtr.Zero;
        private static IntPtr previousToLastHandle = IntPtr.Zero;
        //private static readonly List<String> processBlacklist = new List<string>() { "RPCLAUNCHER" };

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern IntPtr FindWindow();


        public static void StartWatch()
        {
            timer = new Timer(5000);
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);

            //Set the last one before we even start
            SetLastActive();
            timer.Start();
        }

        public static void StopWatch()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Dispose();

                timer = null;
            }
        }

        static void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            SetLastActive();
        }

        public static IntPtr LastHandle
        {
            get
            {
                return previousToLastHandle;
            }
            set
            {
                previousHandle = value;
            }
        }

        public static void SetLastActive()
        {
            IntPtr currentHandle = GetForegroundWindow();
            if (currentHandle != previousHandle && currentHandle != IntPtr.Zero)
            {
                Process lastProcess = ProcessUtils.GetProcessByHandle(currentHandle);

                if (lastProcess != null)// && !processBlacklist.Contains(lastProcess.ProcessName.ToUpper()))
                {
                    previousToLastHandle = previousHandle;
                    previousHandle = currentHandle;
                }
            }
        }
    }
}
