﻿namespace RPCLauncher.Domain
{
    public class YouTubeTimestampCaption
    {
        float Start { get; set; }

        float Dur { get; set; }
        float End { get; set; }
        string Text { get; set; }
        //{"start":"0.2","dur":"1.535","text":"All right. What's going on with thatYouTube?"}
    }
}
