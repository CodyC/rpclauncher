﻿
namespace RPCLauncher.Domain
{
    public class YouTubeTranscript
    {
        public string title { get; set; }
        public Caption[] captions { get; set; }
    }

    public class Caption
    {
        public string start { get; set; }
        public string dur { get; set; }
        public string text { get; set; }
    }

}
