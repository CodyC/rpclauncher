﻿using System;

namespace RPCLauncher.Domain
{
    public class PossibleGeneratedStatements
    {
        public String DebugStatement { get; set; }
        public String ObjectDeclaration { get; set; }
        public String CygwinConvertedPath { get; set; }
        public String CurrentUnixTimestamp { get; set; }
        public String RandomGUID { get; set; }
        public String SqlSelectStatement { get; set; }
        public String SqlUpdateStatement { get; set; }
        public String SqlInsertStatement { get; set; }

    }
}
