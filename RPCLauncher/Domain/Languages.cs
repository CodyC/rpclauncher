﻿using System;

namespace RPCLauncher.Domain
{
    public class Languages
    {
        public const String C_SHARP = "C#";
        public const String JAVA = "Java";
        public const String JAVASCRIPT = "JavaScript";
        public const String PYTHON = "Python";
    }
}
