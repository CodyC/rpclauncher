﻿using System;
using System.Collections.Generic;

namespace RPCLauncher.Domain
{
    public class SpacingStyles
    {
        public const String SPACES = "SPACES";
        public const String SPACES_DISPLAY_VALUE = "Spaces";

        public const String TABS = "TABS";
        public const String TABS_DISPLAY_VALUE = "Tabs";

        public static List<KeyValuePair<String, String>> GetAllSpacingStylesValueAndDisplayValuePairs()
        {
            var keyValuePairs = new List<KeyValuePair<String, String>>();
            keyValuePairs.Add(new KeyValuePair<String, String>(SPACES, SPACES_DISPLAY_VALUE));
            keyValuePairs.Add(new KeyValuePair<String, String>(TABS, TABS_DISPLAY_VALUE));

            return keyValuePairs;
        }

        public static List<String> GetAllBraceStyleDisplayValues()
        {
            return
                new List<string>()
                {
                    SPACES_DISPLAY_VALUE,
                    TABS_DISPLAY_VALUE
                };
        }
    }
}
