﻿using System;

namespace RPCLauncher.Domain
{
    /// <summary>
    /// This is created so that a list of this type can be used bind a collection to a DGV
    /// </summary>
    public class DataGridEntry
    {
        public String DataGridItem { get; set; }
    }
}
