﻿using System;

namespace RPCLauncher.Domain
{
    public class PossibleGeneratedTemplates
    {
        public String StringNotNullCheck { get; set; }
        public String NotNullCheck { get; set; }
        public String SwitchStatement { get; set; }
        public String IfStatement { get; set; }
        public String IfElseStatement { get; set; }
        public String IfElseIfStatement { get; set; }
    }
}
