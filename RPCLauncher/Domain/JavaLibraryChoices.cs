﻿using System;

namespace RPCLauncher.Domain
{
    public class JavaLibraryChoices
    {
        public const String LOG_4_J = "Log4J";
        public const String APACHE_STRING_UTILS = "ApacheStringUtils";
    }
}
