﻿using System;

namespace RPCLauncher.Domain
{
    public class DisplayableHighlight : Highlight
    {
        public String DisplayableTags { get; set; }

        public DisplayableHighlight(Highlight highlight)
        {
            if (highlight != null)
            {
                this.Id = highlight.Id;
                this.HighlightText = highlight.HighlightText;
                this.Notes = highlight.Notes;
                this.Source = highlight.Source;
                this.Tags = highlight.Tags;
                this.UnixTimestamp = highlight.UnixTimestamp;

                this.DisplayableTags = "";
                if (highlight.Tags != null)
                {
                    this.DisplayableTags = String.Join(", ", highlight.Tags);
                }
            }
        }

        public DisplayableHighlight()
        {

        }
    }
}
