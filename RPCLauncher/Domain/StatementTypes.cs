﻿using System;


namespace RPCLauncher.Domain
{
    public class StatementTypes
    {
        public const String IF_STATEMENT_TYPE = "if";
        public const String ELSE_STATEMENT_TYPE = "else";
        public const String ELSE_IF_STATEMENT_TYPE = "else if";
    }
}

