The latest version is of this wiki maintained @
https://bitbucket.org/CodyC/rpclauncher/wiki/Home
# How To Install RPC Launcher 

## Fresh Install
* Copy the version you want to run from the downloads section https://bitbucket.org/CodyC/rpclauncher/downloads/

* Unzip the download to the folder of your choice

* Run RPCLauncher.exe

Most users add RPC Launcher to the Windows Startup programs so it will start with your OS.  You can add RPC Launcher to the Windows 
startup by:
	Open from the system tray -> startup tab -> check Launch on Startup? 

## To Update

* Copy the version you want to run from the downloads section https://bitbucket.org/CodyC/rpclauncher/downloads/

* Unzip the download to the folder of your choice

* Replace the old RPCLauncher.exe with the new version from the unzipped folder

## Supported Platfrorms

RPC Launcher is supported and tested on Windows 7 and 10.
___

## How to use RPC Launcher

RPC Launcher will stay in the system tray until the icon is double-clicked or one of the hot-keys are pressed.  Each pop-up allows keyboard navigation, searching and deletion.

## Shortcut Keys 

* All Shortcut Keys are user assingable.  These are the defaults.

* Ctrl Shift v - Shows clipboard history, you can also delete from this menu.

* Ctrl Shift d - Shows possible generated statements, ie logger statements, object declarations and cygwin path conversions (more to come) these statements are generated dynamically depending on the current language, libraries and preferences you have selected.

* Ctrl Shift f - Searches files and directories inside the system root (This is the C:\ in almost every case).  Searches for files and directories whose names start with, contain, or end with the given search string.

* Ctrl Shift h - Shows the highlights menu.  This will allow you to add and edit highlights.

* Ctrl Shift q - Shows the formatting tools menu, escaped and unescaped items.  Currently supports XML, JSON, SQL, Plain text including conversion to and from Camel Case and Pascal Case.

* Ctrl Shift e - Shows the quick paste items.  These items are configurable through the Settings menu and provide templated commonly pasted items

* Ctrl Shift x - Shows the commonly used commands.  These commands are configurable through the Settings menu and provide commands that can be run on the fly.

## Quick Paste Items:

In addition to standard text, you can use the tokens to replaced at run-time with a certain value.  

For example, if "Mars" is in your clipboard and "Hello {clipboard}!" is a quick paste item, when the text was sent from RPC Launcher the output would be:
	"Hello Mars!"

### Quick Paste Templates
* {clipboard} will insert the text in your clipboard
* {guid} will insert a new GUID
* {unixTimeStamp} will insert the current Unix TimeStamp

The settings can be configured by double-clicking on the icon in the system tray and choosing the corresponding tab.

Selected language, Clipboard history and all settings are saved in the config for use between sessions.
