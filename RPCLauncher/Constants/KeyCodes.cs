﻿
namespace RPCLauncher.Constants
{
    public class KeyCodes
    {
        public const string ESCAPE_KEY_CODE = "\u001b";
        public const string CONTROL_KEY_CODE = "\u0006";
    }
}
