﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RPCLauncher.Utils
{
    public static class StringUtils
    {
        /// <summary>
        /// Performs a case-insensitive contains for a given string
        /// </summary>
        /// <returns>the status of whether the string is inside the given regardless of case</returns>
        public static bool Contains(String stringToCheck, String stringToFind)
        {
            bool isContainedInside = false;

            if (!String.IsNullOrWhiteSpace(stringToCheck)
                && !String.IsNullOrWhiteSpace(stringToFind))
            {
                //This is a case insensitive contains 
                //Found at https://stackoverflow.com/a/444818
                isContainedInside =
                    stringToCheck.IndexOf(stringToFind, StringComparison.OrdinalIgnoreCase) >= 0;
            }

            return isContainedInside;
        }

        /// <summary>
        /// Convert a given pascal case string into a space delimited string
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A space delimited string converted from pascal case.  
        /// Returns "" in case of null, empty or whitespace input.</returns>
        public static String ConvertFromPascalCase(String input)
        {
            return ConvertFromCamelCase(input);
        }

        /// <summary>
        /// Convert a given camel case string into a space delimited string
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A space delimited string converted from camel case.  
        /// Returns "" in case of null, empty or whitespace input.</returns>
        public static String ConvertFromCamelCase(String input)
        {
            String convertedString = "";

            if (!String.IsNullOrWhiteSpace(input))
            {
                convertedString = input;
                StringBuilder camelCaseConversionStringBuilder = new StringBuilder();
                const String DELIMITER_STRING = " ";

                if (IsCamelCase(input) || IsPascalCase(input))
                {
                    //skip the first char and break on any capital char
                    bool isFirstChar = true;
                    foreach (char currentChar in input)
                    {
                        if (isFirstChar)
                        {
                            isFirstChar = false;

                            camelCaseConversionStringBuilder.Append(currentChar);
                            continue;
                        }
                        else
                        {
                            if (Char.IsUpper(currentChar))
                            {
                                camelCaseConversionStringBuilder.Append(DELIMITER_STRING);
                            }

                            camelCaseConversionStringBuilder.Append(currentChar);
                        }
                    }
                    convertedString = camelCaseConversionStringBuilder.ToString();
                }
            }

            return convertedString;
        }

        /// <summary>
        /// Determine if a given string is Camel Case
        /// </summary>
        /// <param name="input"></param>
        /// <returns>bool to show status of camel case</returns>
        public static bool IsPascalCase(String input)
        {
            bool isPascalCase = false;
            if (!String.IsNullOrWhiteSpace(input))
            {
                //check if the first char is upper case, 
                //then check the rest of the string to see if it is camel case        
                isPascalCase =
                    Char.IsUpper(input[0]) && IsCamelCase(input.Slice(1));
            }

            return isPascalCase;
        }

        /// <summary>
        /// Determine if a given string is Camel Case
        /// </summary>
        /// <param name="input"></param>
        /// <returns>bool to show status of camel case</returns>
        public static bool IsCamelCase(String input)
        {
            bool isCamelCase = false;
            if (!String.IsNullOrWhiteSpace(input))
            {
                String strippedInput = input.MultiLineTrim(Environment.NewLine);

                bool containsWhitespaceAfterTrim =
                    strippedInput.Contains(" ")
                     || strippedInput.Contains("\t")
                     || strippedInput.Contains("\n");

                bool isFirstLetterLowerCase = Char.IsLower(strippedInput[0]);

                if (!containsWhitespaceAfterTrim
                        && isFirstLetterLowerCase)
                {
                    char[] inputCharArray = strippedInput.ToCharArray();

                    bool containsUpperCaseChars =
                        inputCharArray
                            .Select(Char.IsUpper)
                            .Count() > 0;

                    bool containsLowerCaseChars =
                        inputCharArray
                            .Select(Char.IsLower)
                            .Count() > 0;

                    isCamelCase =
                        isFirstLetterLowerCase
                            && containsUpperCaseChars
                            && containsLowerCaseChars;
                }
            }

            return isCamelCase;
        }

        /// <summary>
        /// Strips all given non-alphanumeric characters, that do not appear in the give whitelist
        /// </summary>
        /// <param name="input"></param>
        /// <returns>String with only alphanumeric and allowed whitelist characters</returns>
        public static String StripSpecialCharacters(String input, List<Char> whitelistCharacters)
        {
            String strippedString = input;
            if (String.IsNullOrWhiteSpace(input))
            {
                bool whiteListIsPopulated = whitelistCharacters != null;
                StringBuilder strippedStringBuilder = new StringBuilder();

                foreach (char currentChar in input)
                {
                    //if the whitelist is valid, and the char is in the whitelist
                    //OR it is a letter/digit
                    bool addCharToOutput =
                            (whiteListIsPopulated && whitelistCharacters.Contains(currentChar))
                                || Char.IsLetterOrDigit(currentChar);

                    if (addCharToOutput)
                    {
                        strippedStringBuilder.Append(currentChar);
                    }
                }

                strippedString = strippedStringBuilder.ToString();
            }

            return strippedString;
        }

        /// <summary>
        /// Strips all given non-alphanumeric characters
        /// </summary>
        /// <param name="input"></param>
        /// <returns>String with only alphanumeric characters</returns>
        public static String StripSpecialCharacters(String input)
        {
            return StripSpecialCharacters(input, null);
        }

        /// <summary>
        /// Convert the given string into a snake_case represenetation of itself
        /// https://en.wikipedia.org/wiki/Solid_Snake
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A snake_case String</returns>
        public static string ConvertToSnakeCase(string input)
        {
            return convertToCharacterDelimitedString(SplitCamelCase(input), "_");
        }

        /// <summary>
        /// Convert the given string into a kebab-case represenetation of itself
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A kebab_case String</returns>
        public static string ConvertToKebabCase(string input)
        {
            return convertToCharacterDelimitedString(input, "-");
        }

        /// <summary>
        /// Convert the given string into a CONSTANT_CASE represenetation of itself
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A CONSTANT_CASE String</returns>
        public static string ConvertToConstantCase(string input)
        {
            return
                convertToCharacterDelimitedString(input, "_")
                ?.ToUpper();
        }

        /// <summary>
        /// Conversts the given string into a delimited string separated by the specified delimiter
        /// </summary>
        /// <param name="input"></param>
        /// <param name="delimiterCharacter"></param>
        /// <returns></returns>
        private static string convertToCharacterDelimitedString(string input, string delimiterCharacter)
        {
            String characterDelimitedString = input;

            if (!String.IsNullOrWhiteSpace(characterDelimitedString))
            {
                characterDelimitedString = lowerCaseFirstCharacter(characterDelimitedString);

                List<String> words = SplitStringIntoWords(characterDelimitedString);

                StringBuilder characterDelimitedStringBuild = new StringBuilder();
                foreach (String currentWord in words)
                {
                    String currentWordLowerCased = currentWord.ToLower();
                    characterDelimitedStringBuild.Append(currentWordLowerCased);
                    characterDelimitedStringBuild.Append(delimiterCharacter);
                }

                //Remove the last delimiter
                int lastIndex = characterDelimitedStringBuild.Length - 1;
                characterDelimitedStringBuild = characterDelimitedStringBuild.Remove(lastIndex, 1);

                characterDelimitedString = characterDelimitedStringBuild.ToString();
            }

            return characterDelimitedString;
        }

        /// <summary>
        /// Convert the given string into a camel case represenetation of itself
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A camelCase String</returns>
        public static string ConvertToCamelCase(string input)
        {
            String camelCaseString = input;

            if (!String.IsNullOrWhiteSpace(camelCaseString))
            {
                camelCaseString = lowerCaseFirstCharacter(camelCaseString);

                List<String> words = SplitStringIntoWords(camelCaseString);

                StringBuilder camelCaseStringBuilder = new StringBuilder();
                foreach (String currentWord in words)
                {
                    String capitalizedCurrentWord = capitalizeFirstCharacter(currentWord);
                    capitalizedCurrentWord = lowerCaseCharactersAfterFirstCharacter(capitalizedCurrentWord);

                    camelCaseStringBuilder.Append(capitalizedCurrentWord);
                }

                camelCaseString = lowerCaseFirstCharacter(camelCaseStringBuilder.ToString());
            }

            return camelCaseString;
        }

        /// <summary>
        /// Splits the given string into words.
        /// </summary>
        /// <param name="camelCaseString"></param>
        /// <returns>String[] of words in the given string</returns>
        public static List<String> SplitStringIntoWords(string stringToSplit)
        {
            String inputString = stringToSplit;
            var wordsFromInput = new List<String>();

            if (!String.IsNullOrWhiteSpace(inputString))
            {
                String[] splitDelimiters =
                    {
                        " ",
                        "\t",
                        "\n",
                        "\r\n",
                        "?",
                        ".",
                        "!",
                        "_",
                        "-"
                    };

                bool containsDelimiter = false;
                foreach (String currentDelimiter in splitDelimiters)
                {
                    containsDelimiter = inputString.Contains(currentDelimiter);
                    if (containsDelimiter)
                    {
                        break;
                    }
                }

                if (!containsDelimiter)
                {
                    inputString = SplitCamelCase(inputString);
                }

                wordsFromInput =
                    inputString
                        .Split(splitDelimiters, StringSplitOptions.RemoveEmptyEntries)
                        .ToList<String>();

            }

            return wordsFromInput;
        }

        /// <summary>
        /// Convert the given string into a pcascal case represenetation of itself
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A PascalCase String</returns>
        public static string ConvertToPascalCase(string input)
        {
            String pascalCaseString = input;

            if (!String.IsNullOrWhiteSpace(pascalCaseString))
            {
                pascalCaseString = ConvertToCamelCase(pascalCaseString);

                pascalCaseString = capitalizeFirstCharacter(pascalCaseString);
            }

            return pascalCaseString;
        }

        /// <summary>
        /// Convert the first character of the given word to lower case.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A string with the first character converted to lower case.</returns>
        public static string lowerCaseFirstCharacter(string input)
        {
            return changeFirstCharacter(input, toLower);
        }

        /// <summary>
        /// Capitalize the first character of the given word.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A string with the first character capaitalized</returns>
        public static string capitalizeFirstCharacter(string input)
        {
            return changeFirstCharacter(input, toUpper);
        }

        /// <summary>
        /// Set all characters to lower case after the first character of the given word.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A string with the first character capaitalized</returns>
        public static string lowerCaseCharactersAfterFirstCharacter(string input)
        {
            String outputValue = "";
            if (!String.IsNullOrEmpty(input))
            {
                char firstCharacter = input[0];
                string loweredInput = input.Slice(1).ToLower();

                outputValue = firstCharacter + loweredInput;
            }

            return outputValue;
        }

        /// <summary>
        /// Wrapper function for ToUpper method.  
        /// This allows it to be passed to other functions and methods easier.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>The given string in all caps</returns>
        private static String toUpper(String input)
        {
            return input.ToUpper();
        }

        /// <summary>
        /// Wrapper function for ToLower method.  
        /// This allows it to be passed to other functions and methods easier.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>The given string in all lower case</returns>
        private static String toLower(String input)
        {
            return input.ToLower();
        }

        /// <summary>
        /// Provides a wrapper for the conversion function that will modify the first character of a string.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="conversionFunction"></param>
        /// <returns></returns>
        private static string changeFirstCharacter(string input, Func<String, String> conversionFunction)
        {
            String convertedString = input;
            if (!String.IsNullOrWhiteSpace(convertedString))
            {
                String convertedFirstCharacter = conversionFunction(convertedString[0].ToString());

                convertedString =
                    convertedFirstCharacter + convertedString.SliceSafe(1);
            }

            return convertedString;
        }

        /// <summary>
        /// Splits a given string on each pascal case word
        /// </summary>
        /// <param name="input"></param>
        /// <returns>String with each pascal case word space delimited</returns>
        public static string SplitPascalCase(string input)
        {
            return SplitCamelCase(input);
        }

        /// <summary>
        /// Splits a given string on each camel case "hump"
        /// </summary>
        /// <param name="input"></param>
        /// <returns>String with each camel case word space delimited</returns>
        public static string SplitCamelCase(string input)
        {
            return Regex
                .Replace(input,
                         "(?<=[a-z])([A-Z])",
                         " $1",
                         RegexOptions.Compiled)
                    .Trim();
        }

        /// <summary>
        /// Use Python-esque string slicing.
        /// </summary>
        public static string Slice(this string currentString, int startIndex)
        {
            int sliceIndex = startIndex;

            if (startIndex < 0) // if we get a negative
            {
                sliceIndex = currentString.Length + startIndex;
            }

            return currentString.Substring(sliceIndex);
        }

        /// <summary>
        /// Use Python-esque string slicing.  Returns "" instead of throwing an excption
        /// </summary>
        public static string SliceSafe(this string currentString, int startIndex)
        {
            try
            {
                return Slice(currentString, startIndex);
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Use Python-esque string slicing.
        /// </summary>
        public static string Slice(this string currentString, int startIndex, int endIndexNonInclusive)
        {
            string slicedString = currentString.Slice(startIndex);

            int charsUntilEndIndex = currentString.Length - endIndexNonInclusive + 1;

            if (endIndexNonInclusive < 0)
            {
                if (startIndex < endIndexNonInclusive)
                {
                    charsUntilEndIndex = slicedString.Length - Math.Abs(endIndexNonInclusive);
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }

            return slicedString.Substring(0, charsUntilEndIndex);
        }

        /// <summary>
        /// Use Python-esque string slicing.  Returns "" instead of throwing an excption
        /// </summary>
        public static string SliceSafe(this string currentString, int startIndex, int endIndexNonInclusive)
        {
            try
            {
                return Slice(currentString, startIndex, endIndexNonInclusive);
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Returns a multiline String with no leading or trailing whitespace for each line.
        /// 
        /// </summary>
        /// <param name="currentString"></param>
        /// <param name="desiredLineEnds"></param>
        /// <returns>String with ecah line trimmed</returns>
        public static string MultiLineTrim(this string currentString, String desiredLineEnds)
        {
            string trimmedString = currentString;

            if (!String.IsNullOrWhiteSpace(currentString))
            {
                trimmedString = currentString.NormalizeLineEndings(desiredLineEnds);
                StringBuilder currentLineStringBuilder = new StringBuilder();

                String[] currentStringLines =
                    trimmedString.Split(new string[] { desiredLineEnds },
                        StringSplitOptions.RemoveEmptyEntries);

                foreach (String currentLine in currentStringLines)
                {
                    currentLineStringBuilder.Append(currentLine.Trim());
                    currentLineStringBuilder.Append(desiredLineEnds);
                }

                trimmedString = currentLineStringBuilder.ToString().Trim();
            }

            return trimmedString;
        }

        /// <summary>
        /// Change the line endings of a string to the desiredLineEnd value.
        /// </summary>
        /// <param name="currentString"></param>
        /// <param name="desiredLineEnd"></param>
        /// <returns>String with normalized line endings</returns>
        public static string NormalizeLineEndings(this string currentString, String desiredLineEnd)
        {
            string normalizedString = currentString;

            if (!String.IsNullOrEmpty(currentString))
            {
                StringBuilder normalizedStringBuilder = new StringBuilder();

                String[] currentStringLines =
                    currentString
                        .Split(new string[] { "\r\n", "\n" },
                               StringSplitOptions.RemoveEmptyEntries);

                foreach (String currentLine in currentStringLines)
                {
                    normalizedStringBuilder.Append(currentLine);
                    normalizedStringBuilder.Append(desiredLineEnd);
                }

                normalizedString = normalizedStringBuilder.ToString();
            }

            return normalizedString;
        }

        /// <summary>
        /// Gets the lower case letter of the alphabet corresponding to the index.
        /// e.g 1 = a, 26 = a
        /// </summary>
        /// <param name="alphabetIndex"></param>
        public static String GetLetterOfAlphabetByNumberLowerCase(int alphabetIndex)
        {
            //97 is a
            //122 is z           
            //Therefore, 96 is the offset to 'a' being 1

            string letterToReturn = "";

            if (alphabetIndex <= 26)
            {
                const int LOWER_CASE_ASCII_CODE_OFFSET = 96;

                int asciiCode = alphabetIndex + LOWER_CASE_ASCII_CODE_OFFSET;

                var alphabetCharacter = (char)asciiCode;
                letterToReturn = alphabetCharacter.ToString();
            }

            return letterToReturn;
        }

        /// <summary>
        /// Gets the upper case letter of the alphabet corresponding to the index.
        /// e.g 1 = a, 26 = a
        /// </summary>
        /// <param name="alphabetIndex"></param>
        public static String GetLetterOfAlphabetByNumberUpperCase(int alphabetIndex)
        {
            string alphabetCharacter = GetLetterOfAlphabetByNumberLowerCase(alphabetIndex);

            if (!String.IsNullOrWhiteSpace(alphabetCharacter))
            {
                alphabetCharacter = alphabetCharacter.ToUpper();
            }

            return alphabetCharacter;
        }

        /// <summary>
        /// Replaces Carriage Returns and Line Feeds with their escaped equivalents.
        /// </summary>
        /// <param name="currentString"></param>
        /// <returns></returns>
        public static string EscapeNewLines(this string currentString)
        {
            string escapedString = currentString;

            if (!String.IsNullOrWhiteSpace(currentString))
            {
                escapedString =
                    currentString
                        .Replace("\r", "\\r")
                        .Replace("\n", "\\n");
            }

            return escapedString;
        }
    }
}
