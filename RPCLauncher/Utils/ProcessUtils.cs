﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace RPCLauncher.Utils
{
    public static class ProcessUtils
    {
        [DllImport("user32.dll")]
        internal static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("kernel32.dll")]
        static extern int GetProcessId(IntPtr handle);

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint processId);

        public delegate bool EnumWindowProc(IntPtr hwnd, IntPtr lParam);
        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumThreadWindows(IntPtr window, EnumWindowProc callback, IntPtr i);

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumWindows(EnumWindowProc callback, IntPtr i);

        public static Process GetProcessByHandle(IntPtr processHandleToFind)
        {
            var proc =
                Process.GetProcesses()
                        .AsParallel()
                        .Where(x => x.MainWindowHandle != IntPtr.Zero)
                        //.Where(x => x.ProcessName.ToUpper() != "RPCLAUNCHER")
                        .FirstOrDefault(x => isProcessWithMatchingHandle(x, processHandleToFind));

            var procChildWindow = FindWindowEx(processHandleToFind, IntPtr.Zero, null, null);

            if (proc == null)
            {
                var test = Process.GetProcesses()
                                .AsParallel()
                                //.Where(x => x.MainWindowHandle != IntPtr.Zero)
                                //.Where(x => x.ProcessName.ToUpper() != "RPCLAUNCHER")
                                .FirstOrDefault(x => isProcessWithMatchingHandle(x, procChildWindow));

                uint procId = 0;
                GetWindowThreadProcessId(procChildWindow, out procId);
                var childWindows = EnumerateProcessWindowHandles((int)procId);
                //var result = EnumThreadWindows(procChildWindow, OnWindowEnum, IntPtr.Zero);

                var childPRocById = Process.GetProcessById((int)(procId));
            }

            return proc;
        }

        static IEnumerable<IntPtr> EnumerateProcessWindowHandles(int processId)
        {
            var handles = new List<IntPtr>();

            foreach (ProcessThread thread in Process.GetProcessById(processId).Threads)
                EnumWindows((hWnd, lParam) => { handles.Add(hWnd); return true; },
                            IntPtr.Zero);

            return handles;
        }

        public static List<int> windowList = new List<int>();
        public static bool OnWindowEnum(int hwnd, int lparam)
        {
            // you can add some conditions here.
            windowList.Add(hwnd);
            return true;
        }

        public static bool isProcessWithMatchingHandle(Process processToCompare, IntPtr processHandleToFind)
        {
            bool isMatch = false;

            try
            {
                isMatch =
                    processToCompare.MainWindowHandle != IntPtr.Zero
                    && (processToCompare.MainWindowHandle.Equals(processHandleToFind)
                            || processToCompare.Handle.Equals(processHandleToFind));
            }
            catch (Win32Exception)
            {
                //We can't control some Windows processes, this is probably a good thing haha.  
                //So we will just ignore them.
                //Console.WriteLine(w32Exc.Message);
            }

            return isMatch;
        }
    }
}
