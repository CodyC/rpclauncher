﻿using System;
using System.Xml.Linq;

namespace RPCLauncher.Utils
{
    public class XMLUtils
    {
        const String SINGLE_QUOTE_TOKEN = "&apos;";
        const String DOUBLE_QUOTE_TOKEN = "&quot;";
        const String AMPERSAND_TOKEN = "&amp;";
        const String GREATER_THAN_TOKEN = "&gt;";
        const String LESS_THAN_TOKEN = "&lt;";

        public static String PrettyPrintXML(String xmlToPrint)
        {
            String formattedXML = xmlToPrint;

            try
            {
                XDocument doc = XDocument.Parse(xmlToPrint);
                formattedXML = doc.ToString();
            }
            catch (Exception)
            {
            }

            return formattedXML;
        }

        public static String UnescapeXML(String xmlToUnescape)
        {
            // To unescapce XML strings:    
            // &apos; is replaced with '
            // &quot; is replaced with "
            // &amp; is replaced with &
            // &lt; is replaced with <
            // &gt; is replaced with >

            String escapedXML = PrettyPrintXML(xmlToUnescape);
            if (!String.IsNullOrWhiteSpace(escapedXML))
            {
                escapedXML =
                    escapedXML
                        .Replace(SINGLE_QUOTE_TOKEN, "'")
                        .Replace(DOUBLE_QUOTE_TOKEN, "\"")
                        .Replace(AMPERSAND_TOKEN, "&")
                        .Replace(GREATER_THAN_TOKEN, "<")
                        .Replace(LESS_THAN_TOKEN, ">")
                        .Trim();
            }

            return escapedXML;
        }

        public static String EscapeXML(String xmlToEscape)
        {
            // To escapce XML strings:    
            //' is replaced with &apos;
            //" is replaced with &quot;
            //& is replaced with &amp;
            //< is replaced with &lt;
            //> is replaced with &gt;

            String escapedXML = PrettyPrintXML(xmlToEscape);
            if (!String.IsNullOrWhiteSpace(escapedXML))
            {
                escapedXML =
                    escapedXML
                        .Replace("'", SINGLE_QUOTE_TOKEN)
                        .Replace("\"", DOUBLE_QUOTE_TOKEN)
                        .Replace("&", AMPERSAND_TOKEN)
                        .Replace("<", GREATER_THAN_TOKEN)
                        .Replace(">", LESS_THAN_TOKEN)
                        .Trim();
            }

            return escapedXML;
        }

        public static bool isXMLString(String stringToTest)
        {
            bool isXMLString = false;

            if (!String.IsNullOrWhiteSpace(stringToTest))
            {
                string trimmedStringToTest = stringToTest.Trim();

                isXMLString = trimmedStringToTest.StartsWith("<");
            }

            return isXMLString;
        }

    }
}
