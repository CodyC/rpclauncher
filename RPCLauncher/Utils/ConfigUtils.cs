﻿using RPCLauncher.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace RPCLauncher.Utils
{
    public class ConfigUtils
    {
        public const string CLIPBOARD_HISTORY_JSON_KEY = "clipboardHistoryJSON";
        public const string LAST_SELECTED_LANGUAGE_KEY = "lastSelectedLanguage";
        public const string PRESERVE_CLIPBOARD_TEXT_KEY = "preserveClipboardText";
        public const string LANGUAGE_STYLE_PREFERENCES_JSON_KEY = "languageStylePrefencesJSON";
        public const string QUICK_PASTE_ITEMS_JSON_KEY = "quickPasteItemsJSON";
        public const string FILE_SEARCH_DIRECTORIES_JSON_KEY = "fileSearchDirectoriesJSON";
        public const string COMMAND_ENTRIES_JSON_KEY = "commandEntriesJSON";
        public const string COMMAND_SHORTCUT_KEYS_JSON_KEY = "commandShortcutKeysJSON";

        public const string LAUNCH_ON_STARTUP_JSON_KEY = "launchOnStartup";

        public const string CLIPBOARD_HISTORY_COMMAND_JSON_KEY = "clipboardHistory";
        public const string QUICK_PASTE_ITEMS_COMMAND_JSON_KEY = "quickPaste";
        public const string GENERATED_STATEMENTS_COMMAND_JSON_KEY = "generatedStatements";

        public const string APPEND_TO_CLIPBOARD_COMMAND_JSON_KEY = "appendToClipboard";
        public const string HIGHLIGHT_COMMAND_JSON_KEY = "highlight";
        public const string FILE_SEARCH_COMMAND_JSON_KEY = "fileSearch";
        public const string FORMATTING_TOOLS_COMMAND_JSON_KEY = "formattingTools";
        public const string COMMAND_EXECUTION_JSON_KEY = "commandExecution";

        public static void UpdateSetting(string key, string value)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            if (configuration.AppSettings.Settings[key] == null)
            {
                configuration.AppSettings.Settings.Add(key, value);
            }
            else
            {
                configuration.AppSettings.Settings[key].Value = value;
            }

            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");
        }

        public static void UpdateLanguageStylePreferencesJSON(string value)
        {
            UpdateSetting(LANGUAGE_STYLE_PREFERENCES_JSON_KEY, value);
        }

        public static List<LanguageStylePreference> GetLanguageStylePreferences()
        {
            return GetListFromConfig<LanguageStylePreference>(LANGUAGE_STYLE_PREFERENCES_JSON_KEY);
        }

        public static List<String> GetQuickPasteItems()
        {
            return GetListFromConfig<String>(QUICK_PASTE_ITEMS_JSON_KEY);
        }

        public static List<String> GetFileSearchItems()
        {
            return GetListFromConfig<String>(FILE_SEARCH_DIRECTORIES_JSON_KEY);
        }

        public static Dictionary<String, Keys> GetShortcutKeysDictionary()
        {
            var keyboardCommandShortcutKeysDict = GetDictionaryFromConfig<String, Keys>(COMMAND_SHORTCUT_KEYS_JSON_KEY);

            if (!keyboardCommandShortcutKeysDict.ContainsKey(CLIPBOARD_HISTORY_COMMAND_JSON_KEY))
            {
                keyboardCommandShortcutKeysDict.Add(CLIPBOARD_HISTORY_COMMAND_JSON_KEY, Keys.V);
            }

            if (!keyboardCommandShortcutKeysDict.ContainsKey(QUICK_PASTE_ITEMS_COMMAND_JSON_KEY))
            {
                keyboardCommandShortcutKeysDict.Add(QUICK_PASTE_ITEMS_COMMAND_JSON_KEY, Keys.E);
            }

            if (!keyboardCommandShortcutKeysDict.ContainsKey(GENERATED_STATEMENTS_COMMAND_JSON_KEY))
            {
                keyboardCommandShortcutKeysDict.Add(GENERATED_STATEMENTS_COMMAND_JSON_KEY, Keys.D);
            }
            
            if (!keyboardCommandShortcutKeysDict.ContainsKey(HIGHLIGHT_COMMAND_JSON_KEY))
            {
                keyboardCommandShortcutKeysDict.Add(HIGHLIGHT_COMMAND_JSON_KEY, Keys.H);
            }
            
            if (!keyboardCommandShortcutKeysDict.ContainsKey(FILE_SEARCH_COMMAND_JSON_KEY))
            {
                keyboardCommandShortcutKeysDict.Add(FILE_SEARCH_COMMAND_JSON_KEY, Keys.F);
            }
            
            if (!keyboardCommandShortcutKeysDict.ContainsKey(FORMATTING_TOOLS_COMMAND_JSON_KEY))
            {
                keyboardCommandShortcutKeysDict.Add(FORMATTING_TOOLS_COMMAND_JSON_KEY, Keys.Q);
            }

            if (!keyboardCommandShortcutKeysDict.ContainsKey(COMMAND_EXECUTION_JSON_KEY))
            {
                keyboardCommandShortcutKeysDict.Add(COMMAND_EXECUTION_JSON_KEY, Keys.X);

            }

            if (!keyboardCommandShortcutKeysDict.ContainsKey(APPEND_TO_CLIPBOARD_COMMAND_JSON_KEY))
            {
                keyboardCommandShortcutKeysDict.Add(APPEND_TO_CLIPBOARD_COMMAND_JSON_KEY, Keys.C);
            }

            UpdateSetting(COMMAND_SHORTCUT_KEYS_JSON_KEY, JSONUtils.Serialize(keyboardCommandShortcutKeysDict));

            return keyboardCommandShortcutKeysDict;
        }

        public static List<StatementToSendDisplayKeyValuePairs> GetCommandEntries()
        {
            try
            {
                return GetListFromConfig<StatementToSendDisplayKeyValuePairs>(COMMAND_ENTRIES_JSON_KEY);
            }
            catch (Exception)
            {
                try
                {
                    //Support the former data types of only strings
                    List<StatementToSendDisplayKeyValuePairs> commandEntryKeyValuePairs = new List<StatementToSendDisplayKeyValuePairs>();
                    List<String> commandEntries = GetListFromConfig<String>(COMMAND_ENTRIES_JSON_KEY);

                    foreach (String currentCommandEntry in commandEntries)
                    {
                        var currentCommandKeyValuePair = new StatementToSendDisplayKeyValuePairs();
                        currentCommandKeyValuePair.Key = currentCommandEntry;
                        currentCommandKeyValuePair.Value = currentCommandEntry;

                        commandEntryKeyValuePairs.Add(currentCommandKeyValuePair);
                    }

                    return commandEntryKeyValuePairs;
                }
                catch (Exception)
                {
                    //Ok, I have no clue what they stored.  Just send back an empty list
                    return new List<StatementToSendDisplayKeyValuePairs>();
                }
            }
        }

        public static List<T> GetListFromConfig<T>(string appSettingsKey)
        {
            //load the prefences for the selected language
            String listConfigValueJSON = GetConfigValue(appSettingsKey);

            var configValueList = new List<T>();

            if (!String.IsNullOrWhiteSpace(listConfigValueJSON))
            {
                configValueList = JSONUtils.Deserialize<List<T>>(listConfigValueJSON);
            }

            if (configValueList == null)
            {
                configValueList = new List<T>();
            }

            return configValueList;
        }

        public static Dictionary<T, R> GetDictionaryFromConfig<T, R>(string appSettingsKey)
        {
            //load the prefences for the selected language
            String dictionaryConfigValueJSON = GetConfigValue(appSettingsKey);

            var configValueList = new Dictionary<T, R>();

            if (!String.IsNullOrWhiteSpace(dictionaryConfigValueJSON))
            {
                configValueList = JSONUtils.Deserialize<Dictionary<T, R>>(dictionaryConfigValueJSON);
            }

            if (configValueList == null)
            {
                configValueList = new Dictionary<T, R>();
            }

            return configValueList;
        }

        public static bool GetLaunchOnStartupConfigValue()
        {
            String launchOnStartupJson = GetConfigValue(LAUNCH_ON_STARTUP_JSON_KEY);

            bool launchOnStartupValue = false;

            if (!String.IsNullOrWhiteSpace(launchOnStartupJson))
            {
                try
                {
                    launchOnStartupValue = JSONUtils.Deserialize<bool>(launchOnStartupJson);
                }
                catch
                {
                }
            }

            return launchOnStartupValue;
        }

        public static String GetConfigValue(String appSettingsKey)
        {
            return ConfigurationManager.AppSettings[appSettingsKey];
        }
    }
}
