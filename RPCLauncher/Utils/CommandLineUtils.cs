﻿using System;

namespace CommandLineUtilsLib
{
    public static class CommandLineUtils
    {
        public static string ExecuteCommandSync(String command)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;

                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // procStartInfo.CreateNoWindow = false;

                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                //proc.WaitForExit();

                // Get the output into a string
                string result = proc.StandardOutput.ReadToEnd();

                return result.Trim();
            }
            catch (Exception exc)
            {
                //MessageBox.Show("Error in CommandLine" + DblNewLine + exc.Message);
                //return null;

                throw exc;
                // Log the exception
            }
        }
    }
}
