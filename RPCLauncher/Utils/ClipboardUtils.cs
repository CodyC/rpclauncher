﻿using System;
using System.Windows.Forms;

namespace RPCLauncher.Utils
{
    public class ClipboardUtils
    {
        /// <summary>
        /// Returns the text in the clipboard with a trim.  
        /// If the clipboard contains non-text or is empty, the method returns ""
        /// 
        /// </summary>
        /// <returns>The clipboard content trimmed, or "" if the clipboard is empty</returns>
        public static string GetTrimmedClipboardText()
        {
            String clipboardText = "";
            if (Clipboard.ContainsText())
            {
                clipboardText = Clipboard.GetText().Trim();
            }

            return clipboardText;
        }
    }
}
