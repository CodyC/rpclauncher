﻿using RPCLauncher.Domain;
using RPCLauncher.GlobalMontiors;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace RPCLauncher.Utils
{
    public class SendKeysUtils
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        internal static extern IntPtr FindWindowExW(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll")]
        internal static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("User32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int uMsg, int wParam, string lParam);

        [DllImport("user32.dll")]
        static extern IntPtr CloseClipboard();

        /// <summary>
        /// Escapes the given text into a format which will be typed through the keyboard emulator.
        /// if preserveTextInClipboard == true, the text that 
        /// was in the clipboard before the call will remain in the clipboard after the call.
        /// </summary>
        /// <param name="textToSend"></param>
        /// <param name="preserveTextInClipboard"></param>
        public static void SendText(String textToSend, bool preserveTextInClipboard)
        {
            if (!String.IsNullOrEmpty(textToSend))
            {
                string existingClipboardText = "";
                if (preserveTextInClipboard)
                {
                    //Store what was in the clipboard
                    existingClipboardText = Clipboard.GetText();
                }

                ClipboardMonitor.Stop();

                try
                {
                    //I am adding this to handle the exceptions that are thrown when adding text to the clipboard.
                    CloseClipboard();
                }
                catch (Exception)
                {
                    //Intentionally leaving this.
                    //It is better to not send the text to the window than have the entire program crash.
                }

                int attemptCounter = 0;
                bool wasClipboardSetSuccessful = false;
                while (wasClipboardSetSuccessful == false || attemptCounter >= 12)
                {
                    try
                    {
                        Clipboard.SetDataObject(textToSend, true, 13, 10);

                        if (Clipboard.GetText() == textToSend)
                        {
                            wasClipboardSetSuccessful = true;
                            break;
                        }
                    }
                    catch (Exception)
                    {
                        wasClipboardSetSuccessful = false;
                    }

                    attemptCounter++;
                }
  
                SendPasteCommand();

                if (preserveTextInClipboard && !String.IsNullOrEmpty(existingClipboardText))
                {
                    //Reassign clipboard text
                    Clipboard.SetText(existingClipboardText);
                }

                ClipboardMonitor.Start();
            }
        }

        public static void SendCopyCommand()
        {
            SendKeys.SendWait("^{c}");
        }

        public static void SendPasteCommand()
        {
            //Send Control + v for paste, 
            //this is far more performant and reliable than send keys with the text 
            SendKeys.SendWait("^{v}");
        }

        public static void SendTextToWindow(IntPtr windowHandleToReceiveText,
                                            String textToSend,
                                            bool preserveTextInClipboard)
        {
            Process processToReceiveText = ProcessUtils.GetProcessByHandle(windowHandleToReceiveText);

            nint processHandleToEngage;

            if (processToReceiveText != null)
            {
                processHandleToEngage = processToReceiveText.MainWindowHandle;
            }
            else
            {
                processHandleToEngage = ProcessWatcher.LastHandle;
            }

            //The window needs to be in focus for the keyboard emulator to send input
            SetForegroundWindow(processHandleToEngage);

            SendText(textToSend, preserveTextInClipboard);

            //Reset the last found handle so we can find the next window
            ProcessWatcher.LastHandle = IntPtr.Zero;
        }
    }
}
