﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace RPCLauncher.Utils
{
    public class SqlUtils
    {
        public static string PrettyPrintSql(string sqlStatement)
        {
            //Ok, the idea is to take a normal SQL statement and make it nice to display

            //Each SQL statement clause will essentially get its own line
            //Keywords are capitalized (SELECT, FROM, WHERE)

            //SELECT Clause
            //FROM Clause
            //WHERE Clause
            //GROUP BY Clause
            //ORDER BY Clause

            List<String> sqlKeywords =
            [
                ",",
                "AND",
                "OR",
                .. getSqlClauseKeywords(),
            ];

            return prettyPrintSqlClauses(sqlStatement, sqlKeywords);
        }

        private static List<String> getSqlClauseKeywords()
        {
            return new List<string>()
            {
                "SELECT",
                "DROP",
                "FROM",
                "WHERE",
                "ORDER BY",
                "UPDATE",
                "INSERT",
                "SET",
                "COLUMNS",
                "DELETE",
                "TRUNC",
            };
        }

        private static String prettyPrintSqlClauses(String sqlStatementToPrettyPrint,
                                                    List<String> sqlKeywords)
        {
            String prettyPrintedSqlCommand = "";

            if (!String.IsNullOrWhiteSpace(sqlStatementToPrettyPrint))
            {
                prettyPrintedSqlCommand = sqlStatementToPrettyPrint;

                if (sqlKeywords != null)
                {
                    List<String> sqlClauseKeywords = getSqlClauseKeywords();

                    foreach (String currentSqlkeyword in sqlKeywords)
                    {
                        StringBuilder prettyPrintedKeyWordStringBuilder = new StringBuilder();

                        String currentCapitalizedKeyword = currentSqlkeyword.ToUpper();

                        //add a line before to break the clause up
                        bool isClauseKeyword =
                            sqlClauseKeywords.Contains(currentCapitalizedKeyword);

                        bool isFieldSeparator = currentCapitalizedKeyword == ",";

                        if (isFieldSeparator || isClauseKeyword)
                        {
                            if (isClauseKeyword)
                            {
                                prettyPrintedKeyWordStringBuilder.Append(Environment.NewLine);
                            }

                            prettyPrintedKeyWordStringBuilder.Append(currentCapitalizedKeyword);
                            prettyPrintedKeyWordStringBuilder.Append(Environment.NewLine);

                            prettyPrintedSqlCommand
                                = Regex.Replace(prettyPrintedSqlCommand,
                                                currentSqlkeyword,
                                                prettyPrintedKeyWordStringBuilder.ToString(),
                                                RegexOptions.IgnoreCase);
                        }
                        else
                        {
                            //just put the word back in, like for example: 
                            //if you have a word that starts with a keyword like Order
                            prettyPrintedKeyWordStringBuilder.Append(currentSqlkeyword);
                        }

                        //the regex replaceme statement was here
                    }

                    //Replace the double lines, we will need to handle a few different cases 
                    //b /c we don't know exactly where they are coming from
                    prettyPrintedSqlCommand
                        = prettyPrintedSqlCommand
                            .Replace(Environment.NewLine + Environment.NewLine, Environment.NewLine)
                            .Replace("\n\n", Environment.NewLine)
                            .Replace("\r\n \r\n", Environment.NewLine)
                            .Replace("\r\nSELECT\r\n ", Environment.NewLine + "SELECT" + Environment.NewLine + "\t")
                            .Replace("\r\nFROM\r\n", Environment.NewLine + "FROM" + Environment.NewLine + "\t")
                            .Replace("\nFROM\n", Environment.NewLine + "FROM" + Environment.NewLine + "\t")
                            .Replace(",\r\n", "," + Environment.NewLine + "\t");
                }
            }

            return prettyPrintedSqlCommand.Trim();
        }

        public static bool IsValidSqlStatement(String sqlStatementToValidate)
        {
            return
                !String.IsNullOrWhiteSpace(sqlStatementToValidate)
                    && (sqlStatementToValidate.StartsWith("SELECT", StringComparison.OrdinalIgnoreCase)
                            || sqlStatementToValidate.StartsWith("UPDATE", StringComparison.OrdinalIgnoreCase)
                            || sqlStatementToValidate.StartsWith("INSERT", StringComparison.OrdinalIgnoreCase)
                            || sqlStatementToValidate.StartsWith("DELETE", StringComparison.OrdinalIgnoreCase)
                            || sqlStatementToValidate.StartsWith("DROP", StringComparison.OrdinalIgnoreCase))
                            || sqlStatementToValidate.StartsWith("TRUNC", StringComparison.OrdinalIgnoreCase);

        }
    }
}
