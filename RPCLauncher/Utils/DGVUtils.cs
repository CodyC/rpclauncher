﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace RPCLauncher.Utils
{
    public class DGVUtils
    {
        /// <summary>
        /// Used to add columns to grids and set the necessary properties.  Used where the grid
        /// is expected to be bound to a List object
        /// <para/>
        /// MAKE SURE The name is the same as the property that is to be bound from the list object
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="name"></param>
        /// <param name="headerText"></param>
        /// <param name="readOnly"></param>
        /// <param name="width"></param>
        public static void AddColToDataGrid(DataGridView dgv,
                                            string name,
                                            string headerText,
                                            bool readOnly)
        {
            //Add new column to data grid
            DataGridViewTextBoxColumn newColumn = new DataGridViewTextBoxColumn();
            newColumn.Name = name;
            newColumn.DataPropertyName = name;  //MAKE SURE The name is the same as the property that is to be bound
            newColumn.ReadOnly = readOnly;

            //Allow lineWraps
            newColumn.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            if (String.IsNullOrWhiteSpace(headerText))
            {
                newColumn.HeaderText = name;
            }
            else
            {
                newColumn.HeaderText = headerText;
            }

            dgv.Columns.Add(newColumn);

            //Allow Autosizing for wrapped rows.
            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        public static void SetupGrid<T>(DataGridView dgv, BindingList<T> dataSource)
        {
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = dataSource;

            dgv.Refresh();
        }
    }
}
