﻿using System;
using System.Linq;

namespace RPCLauncher.Utils
{
    public class PhoneNumberUtils
    {
        /// <summary>
        /// Verifies that a given string is an unformatted US Phone Number
        /// </summary>
        /// <param name="phoneNumberToValidate"></param>
        /// <returns></returns>
        public static bool IsUnformattedPhoneNumber(String phoneNumberToValidate)
        {
            string formattedPhoneNumberToValidate = phoneNumberToValidate;
            bool isValidPhoneNumber = false;

            if (!String.IsNullOrWhiteSpace(phoneNumberToValidate))
            {
                formattedPhoneNumberToValidate = phoneNumberToValidate.Replace(".", "").Trim();

                int dotCount = phoneNumberToValidate.Count(x => x == '.');


                bool are4DigitsAfterLastDot =
                    phoneNumberToValidate.LastIndexOf('.') + 1 == phoneNumberToValidate.Length - 4;

                bool isDotCountValid = dotCount >= 0 && dotCount <= 3;

                if (dotCount == 3 || dotCount == 4)
                {
                    isDotCountValid = isDotCountValid && are4DigitsAfterLastDot;
                }


                if (isDotCountValid)
                {

                    bool is10DigitNumber = (!String.IsNullOrWhiteSpace(formattedPhoneNumberToValidate)
                                            && formattedPhoneNumberToValidate.Length == 10
                                            && formattedPhoneNumberToValidate.All(Char.IsDigit));

                    bool is11DigitNumber =
                    (!String.IsNullOrWhiteSpace(formattedPhoneNumberToValidate)
                     && formattedPhoneNumberToValidate.Length == 11
                     && formattedPhoneNumberToValidate.Substring(0, 1) == "1"
                     && formattedPhoneNumberToValidate.All(Char.IsDigit));

                    bool is12DigitNumber =
                    (!String.IsNullOrWhiteSpace(formattedPhoneNumberToValidate)
                     && formattedPhoneNumberToValidate.Length == 12
                     && formattedPhoneNumberToValidate.Substring(0, 1) == "+"
                     && formattedPhoneNumberToValidate.Substring(1, 1) == "1"
                     && formattedPhoneNumberToValidate.Substring(1).All(Char.IsDigit));

                    isValidPhoneNumber = is10DigitNumber || is11DigitNumber || is12DigitNumber;
                }
            }

            return isValidPhoneNumber;
        }

        /// <summary>
        /// Formats a given string as a 10 digit US Phone Number
        /// For example 2222222222
        /// would become: (222) 222-2222
        /// 
        /// For example 12222222222
        /// would become: 1 (222) 222-2222
        /// 
        /// For example +12222222222
        /// would become: +1 (222) 222-2222
        /// 
        /// . separated numbers are also supported
        /// 
        /// </summary>
        /// <param name="phoneNumberToFormat"></param>
        /// <returns></returns>
        public static String FormatPhoneNumber(String phoneNumberToFormat)
        {
            String formattedPhoneNumber = phoneNumberToFormat;

            if (!String.IsNullOrWhiteSpace(phoneNumberToFormat)
                    && IsUnformattedPhoneNumber(phoneNumberToFormat))
            {

                String stippedPhoneNumber =
                    phoneNumberToFormat
                        .Replace(".", "")
                        .Trim();

                int areaCodeStartIndex = stippedPhoneNumber.Length - 10;

                String countryCode = "";
                if (stippedPhoneNumber.StartsWith("+1"))
                {
                    countryCode = "+1 ";
                }
                else if (stippedPhoneNumber.StartsWith("1"))
                {
                    countryCode = "1 ";
                }

                String areaCode = "(" + stippedPhoneNumber.Substring(areaCodeStartIndex, 3) + ")";
                String exchange = stippedPhoneNumber.Substring(areaCodeStartIndex + 3, 3);
                String suffix = stippedPhoneNumber.Substring(areaCodeStartIndex + 6);

                formattedPhoneNumber = countryCode + areaCode + " " + exchange + "-" + suffix;
                formattedPhoneNumber = formattedPhoneNumber.Trim();
            }

            return formattedPhoneNumber;
        }
    }
}
