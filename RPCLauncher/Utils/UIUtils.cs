﻿using RPCLauncher.Constants;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace RPCLauncher.Utils
{
    public class UIUtils
    {
        public static void CloseFormOnEscapeKeyPressed(KeyEventArgs e, Form formToClose)
        {
            bool escapeWasPressed = KeyHandlerUtils.EscapeWasPressed(e);

            if (escapeWasPressed)
            {
                formToClose.Close();
            }
        }

        public static Screen GetScreenContainingMousePointer()
        {
            Screen mouseContainingScreen = Screen.AllScreens[0];

            bool isMultiMonitorEnvironment = Screen.AllScreens.Length > 1;

            if (isMultiMonitorEnvironment)
            {
                int screenCounter = 0;
                bool hasMonitorContaingPointerBeenFound = false;

                foreach (Screen currentScreen in Screen.AllScreens)
                {
                    hasMonitorContaingPointerBeenFound = currentScreen.Bounds.Contains(Cursor.Position);

                    if (hasMonitorContaingPointerBeenFound)
                    {
                        mouseContainingScreen = Screen.AllScreens[screenCounter];
                        break;
                    }
                    else
                    {
                        screenCounter++;
                    }
                }
            }

            return mouseContainingScreen;
        }

        public static Point GetScreenContainingMousePointerCenter()
        {
            Screen screenContainingMousePointer = GetScreenContainingMousePointer();
            Point centerOfScreen = GetCenterOfScreen(screenContainingMousePointer);

            return centerOfScreen;
        }


        /// <summary>
        /// Returns the center point of the given screen
        /// </summary>
        /// <param name="screen"></param>
        /// <returns></returns>
        public static Point GetCenterOfScreen(Screen screen)
        {
            var centerPointOfScreen =
                new Point(screen.WorkingArea.Location.X / 2,
                          screen.WorkingArea.Location.Y / 2);

            return centerPointOfScreen;
        }

        /// <summary>
        /// Display the given form above all other windows with focus requested.
        /// </summary>
        /// <param name="formToDisplay"></param>
        public static void ShowTopMostFocusedDialog(Form formToDisplay)
        {
            formToDisplay.TopMost = true;
            formToDisplay.ShowDialog();
            formToDisplay.Focus();
        }

        /// <summary>
        /// Checks the given textBox for the default blacklist
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns>returns status of blacklist text</returns>
        public static bool ContainsBlackListCharacter(TextBoxBase textBox)
        {
            return ContainsBlackListCharacter(textBox, GetDefaultTextBoxBlacklist());
        }

        /// <summary>
        /// Checks the given text for the default blacklist
        /// </summary>
        /// <param name="textToSearch"></param>
        /// <returns>returns status of blacklist text</returns>
        public static bool ContainsBlackListCharacter(String textToSearch)
        {
            return ContainsBlackListCharacter(textToSearch, GetDefaultTextBoxBlacklist());
        }

        /// <summary>
        /// Checks the given textBox for the given blacklist
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="blacklist"></param>
        /// <returns>returns status of blacklist text</returns>
        public static bool ContainsBlackListCharacter(String textToSearch, List<String> blacklist)
        {
            bool containsBlacklistString = false;

            if (!String.IsNullOrEmpty(textToSearch))
            {
                containsBlacklistString = blacklist.Contains(textToSearch);
            }

            return containsBlacklistString;
        }

        /// <summary>
        /// Checks the given textBox for the given blacklist
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="blacklist"></param>
        /// <returns>returns status of blacklist text</returns>
        public static bool ContainsBlackListCharacter(TextBoxBase textBox, List<String> blacklist)
        {
            return ContainsBlackListCharacter(textBox.Text, blacklist);
        }

        /// <summary>
        /// Get standard blacklist strings for textboxes
        /// </summary>
        /// <returns>List of default non-allowed strings</returns>
        public static List<String> GetDefaultTextBoxBlacklist()
        {
            return
                new List<String>()
                {
                    "\b",
                    KeyCodes.CONTROL_KEY_CODE,
                    KeyCodes.ESCAPE_KEY_CODE
                };
        }
    }
}
