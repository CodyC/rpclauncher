﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RPCLauncher.Utils
{
    public class IpAddressUtils
    {
        /// <summary>
        /// Verifies that a given string is a valid Ip address
        /// </summary>
        /// <param name="ipToValidate"></param>
        /// <returns></returns>
        public static bool IsValidIpV4Address(String ipToValidate)
        {
            bool isValidIpV4Address = false;

            if (!String.IsNullOrWhiteSpace(ipToValidate))
            {
                List<String> octets =
                    ipToValidate
                        .Trim()
                        .Split('.')
                        .ToList();

                if (octets.Count == 4)
                {

                    bool isOctetValid = true;

                    foreach (var currentOctet in octets)
                    {
                        int parsedOctet = -1;
                        bool isParseSuccessful = Int32.TryParse(currentOctet, out parsedOctet);

                        if (isParseSuccessful)
                        {
                            isOctetValid = isOctetValid && (parsedOctet > 0 && parsedOctet < 256);

                            if (!isOctetValid)
                            {
                                break;
                            }
                        }
                    }

                    if (isOctetValid)
                    {

                        isValidIpV4Address = true;
                    }

                }
            }

            return isValidIpV4Address;
        }

    }
}
