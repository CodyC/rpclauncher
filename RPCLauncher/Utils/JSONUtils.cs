﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace RPCLauncher.Utils
{
    public static class JSONUtils
    {
        public static T Deserialize<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public static string Serialize(Object objToSerialize)
        {
            return JsonConvert.SerializeObject(objToSerialize);
        }

        public static String EscapeJSONString(String jsonStringToEscape)
        {
            //Backspace is replaced with \b
            //Form feed is replaced with \f
            //Newline is replaced with \n
            //Carriage return is replaced with \r
            //Tab is replaced with \t
            //Double quote is replaced with \"
            //Backslash is replaced with \\

            String escapedJSONString = jsonStringToEscape;

            if (!String.IsNullOrWhiteSpace(jsonStringToEscape))
            {
                escapedJSONString =
                    jsonStringToEscape
                        .Replace(@"\", @"\\")
                        .Replace('"'.ToString(),
                                 @"\".ToString() + '"'.ToString());
            }

            return escapedJSONString;
        }

        //Stolen from http://stackoverflow.com/a/24782322
        public static string PrettyPrintJSON(string json)
        {
            String normalizedjson = "";

            if (!String.IsNullOrWhiteSpace(json))
            {
                normalizedjson = Serialize(Deserialize<Object>(json));
            }

            const string INDENT_STRING = "    ";

            int indentation = 0;
            int quoteCount = 0;

            var result =
                from ch in normalizedjson

                let quotes = ch == '"' ? quoteCount++ : quoteCount

                let lineBreak =
                    ch == ',' && quotes % 2 == 0
                        ? ch + Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, indentation))
                        : null

                let openChar = ch == '{' || ch == '[' ? ch + Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, ++indentation)) : ch.ToString()

                let closeChar = ch == '}' || ch == ']' ? Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, --indentation)) + ch : ch.ToString()

                select lineBreak == null
                        ? openChar.Length > 1
                            ? openChar
                            : closeChar
                        : lineBreak;

            return String.Concat(result);
        }

        public static bool isJSONString(String stringToTest)
        {
            bool isJSONString = false;

            if (!String.IsNullOrWhiteSpace(stringToTest))
            {
                string trimmedStringToTest = stringToTest.Trim();

                isJSONString =
                    trimmedStringToTest.StartsWith("{")
                        || trimmedStringToTest.StartsWith("[");
            }

            return isJSONString;
        }
    }
}
