﻿using RPCLauncher.Domain;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace RPCLauncher.Utils
{
    public class YouTubeUtils
    {
        /// <summary>
        /// Verifies that a given url is a valid YouTube address
        /// </summary>
        /// <param name="urlToValidate"></param>
        /// <returns></returns>
        public static bool IsValidYouTubeAddress(String urlToValidate)
        {
            return !String.IsNullOrWhiteSpace(urlToValidate)
                    && (urlToValidate.ToUpper().Contains("YOUTUBE.COM/WATCH?V=")
                        || urlToValidate.ToUpper().Contains("YOUTU.BE/"));
        }

        public static String TranscribeYouTubeVideo(String urlToTranscribe)
        {
            String transcription = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://tactiq-apps-prod.tactiq.io/");
            client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));//implicit ACCEPT header

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "transcript");
            string requestBody = "{\"videoUrl\":\"" + urlToTranscribe + "\", \"langCode\":\"en\"}";
            request.Content = new StringContent(requestBody, Encoding.UTF8, "application/json");//implicit CONTENT-TYPE header

            var content = client.SendAsync(request).Result.Content.ReadAsStringAsync().Result;

            if (!String.IsNullOrEmpty(content))
            {
                try
                {
                    YouTubeTranscript ytt = JSONUtils.Deserialize<YouTubeTranscript>(content);

                    StringBuilder transcriptStringBuilder = new StringBuilder();

                    transcriptStringBuilder.Append("TITLE:");
                    transcriptStringBuilder.AppendLine(ytt.title);

                    foreach (var currentCaption in ytt.captions)
                    {
                        transcriptStringBuilder.Append(currentCaption.start);
                        transcriptStringBuilder.Append(": ");
                        transcriptStringBuilder.AppendLine(currentCaption.text);
                    }

                    transcription = transcriptStringBuilder.ToString();
                }
                catch (Exception)
                {

                }
            }
            else
            {
                transcription = "ERROR - Can't process YouTube transcription.";
            }

            return transcription;
        }
    }
}
