﻿using RPCLauncher.GlobalMontiors;
using System;
using System.Windows.Forms;

namespace RPCLauncher.Utils
{
    public class ListBoxUtils
    {
        /// <summary>
        /// Copy the current selected item in the listbox into the clipboard.  Will not reocrd history of copied text.
        /// </summary>
        /// <param name="listBoxForSelection"></param>
        public static void textSelectionToClipboardWithoutHistory(ListBox listBoxForSelection)
        {
            String selectedText = listBoxForSelection.SelectedItem as String;

            //Stop collecting history b/c we don't want to add the item we have selected back into history.  May cause a paradox...
            ClipboardMonitor.Stop();

            Clipboard.SetText(selectedText);

            //Start collecting history again.
            ClipboardMonitor.Start();
        }
    }
}
