﻿using RPCLauncher.Constants;
using System.Windows.Forms;

namespace RPCLauncher.Utils
{
    public class KeyHandlerUtils
    {
        public static bool EnterWasPressed(KeyEventArgs e)
        {
            return compareKeyCode(e, Keys.Return);
        }

        public static bool DeleteWasPressed(KeyEventArgs e)
        {
            return compareKeyCode(e, Keys.Delete);
        }

        public static bool EscapeWasPressed(KeyEventArgs e)
        {
            return compareKeyCode(e, Keys.Escape);
        }

        public static bool ContainsEscapeKey(string text)
        {
            bool containsEscapeKey = false;

            if (!string.IsNullOrWhiteSpace(text))
            {
                containsEscapeKey = text.Contains(KeyCodes.ESCAPE_KEY_CODE);
            }

            return containsEscapeKey;
        }

        public static bool UpArrowWasPressed(KeyEventArgs e)
        {
            return compareKeyCode(e, Keys.Up);
        }

        public static bool DownArrowWasPressed(KeyEventArgs e)
        {
            return compareKeyCode(e, Keys.Down);
        }

        private static bool compareKeyCode(KeyEventArgs e, Keys keyToFind)
        {
            return e.KeyCode == keyToFind;
        }
    }
}
