﻿using System;

namespace RPCLauncher.Utils
{
    public class TimeUtils
    {
        public static long getCurrentUnixTimestamp()
        {
            DateTimeOffset dateTimeOffset = new DateTimeOffset(DateTime.Now);

            return dateTimeOffset.ToUnixTimeSeconds();
        }

        public static long getCurrentUnixTimestampMilliseconds()
        {
            DateTimeOffset dateTimeOffset = new DateTimeOffset(DateTime.Now);

            return dateTimeOffset.ToUnixTimeMilliseconds();
        }

        public static String getCurrentUnixTimestampAsString()
        {
            return getCurrentUnixTimestamp().ToString();
        }

    }
}
