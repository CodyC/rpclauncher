﻿using RPCLauncher.Domain;
using RPCLauncher.Services;
using RPCLauncher.Utils;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace RPCLauncher.Forms
{
    public partial class CommandExecutionForm : StatementToSendSelectionForm
    {
        public CommandExecutionForm()
        {
            InitializeComponent();

            //Pops the form up in the center of the screen containing the mouse cursor
            //See this link for more details: 
            //https://msdn.microsoft.com/en-us/library/system.windows.forms.form.centertoscreen(v=vs.110).aspx
            this.StartPosition = FormStartPosition.CenterScreen;

            StatementsToDisplayListBox.DataSource = ConfigUtils.GetCommandEntries();
            StatementsToDisplayListBox.DisplayMember = "Key";
            StatementsToDisplayListBox.ValueMember = "Value";

            setupUI();
        }

        private void setupUI()
        {
            this.Text = "Command Execution";
            preserveClipboardTextCheckBox.Visible = false;

            SubmitBtn.Text = "&Execute";
        }

        /// <summary>
        /// Overrides default selection handler
        /// </summary>
        public override void handleSelectionFromStatementsListBox()
        {
            if (StatementsToDisplayListBox.SelectedItem != null)
            {
                StatementToSendDisplayKeyValuePairs selectedCommand = StatementsToDisplayListBox.SelectedItem as StatementToSendDisplayKeyValuePairs;
                TokenReplacementService tokenReplacementService = new TokenReplacementService();
                
                //Clear out the selection so it doesn't propagate.
                StatementsToDisplayListBox.SelectedItem = null;

                //Escape the command,
                //Replace the tokens in it
                String escapedCommand = '"' + tokenReplacementService.GetTokenFilledString(selectedCommand.Value) + '"';

                //Call the command on the command line
                Process.Start("cmd", " /c" + escapedCommand);

                this.Close();
            }
        }
    }
}
