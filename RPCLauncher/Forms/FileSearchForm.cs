﻿using RPCLauncher.Services;
using RPCLauncher.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPCLauncher.Forms
{
    public partial class FileSearchForm : StatementToSendSelectionForm
    {
        private CancellationTokenSource source = new CancellationTokenSource();
        private CancellationToken token;

        public FileSearchForm()
        {
            InitializeComponent();

            //Pops the form up in the center of the screen containing the mouse cursor
            //See this link for more details: 
            //https://msdn.microsoft.com/en-us/library/system.windows.forms.form.centertoscreen(v=vs.110).aspx
            this.StartPosition = FormStartPosition.CenterScreen;

            setupUI();

            setupEventHandlers();
        }

        private void setupUI()
        {
            preserveClipboardTextCheckBox.Visible = false;

            SearchLbl.Text = "File :";
            SubmitBtn.Text = "&Open";
        }

        /// <summary>
        /// Assign the new event handlers that override the existing handlers from the inherited form.
        /// </summary>
        private void setupEventHandlers()
        {
            //remove the parent class' handlers and assign our new ones
            //SearchTxt.TextChanged -= searchTxt_TextChanged;
            //SearchTxt.TextChanged += fileSearchTxt_TextChanged;

            SearchTxt.KeyDown -= searchTxt_KeyDown;
            SearchTxt.KeyDown += fileSearchTxt_KeyDown;
        }

        /// <summary>
        /// Overrides default selection handler
        /// </summary>
        public override async void handleSelectionFromStatementsListBox()
        {
            if (SearchTxt.Focused)
            {
                String searchText = String.Copy(SearchTxt.Text);

                StatementsToDisplayListBox.BeginUpdate();
                // Set cursor as hourglass
                this.UseWaitCursor = true;

                token = source.Token;

                //Update the title bar text
                this.Text = "File Search - Searching file and directory names for " + searchText + " ...";

                var start = TimeUtils.getCurrentUnixTimestampMilliseconds();

                await Task.Run(async () =>
                {
                    String driveIndexFilePath = FileSystemService.GetDriveIndexFilePath();
                    bool driveIndexFileExists = File.Exists(driveIndexFilePath);

                    if (!driveIndexFileExists)
                    {
                        await FileSystemService.IndexFileSystem();
                    }

                    var filesToSearch = File.ReadLines(driveIndexFilePath);

                    StatementsToDisplayListBox.DataSource =
                                filesToSearch
                                .AsParallel()
                                .Where(x => x.ToLower().IndexOf(searchText.ToLower()) > -1)
                                .ToList();
                }, token);

                var end = TimeUtils.getCurrentUnixTimestampMilliseconds();
                List<String> foundResults = StatementsToDisplayListBox.DataSource as List<String>;
                String formattedFoundResultsCount = String.Format("{0:n0}", foundResults.Count);

                double searchDelta = Math.Round((end - start) / 1000.0, 3);
                String statsString = searchDelta + " seconds";

                //Update the title bar text
                this.Text = "File Search - Found " + formattedFoundResultsCount + " Results.  Took " + statsString;

                StatementsToDisplayListBox.EndUpdate();
                //StatementsToDisplayListBox.Refresh();
                this.UseWaitCursor = false;
            }
            else if (StatementsToDisplayListBox.Focused)
            {
                if (StatementsToDisplayListBox.SelectedItem != null)
                {
                    //open the file or directory with the default application.
                    String fileOrDirectoryToOpen = StatementsToDisplayListBox.SelectedItem as String;

                    if (!String.IsNullOrWhiteSpace(fileOrDirectoryToOpen))
                    {
                        using Process fileopener = new Process();

                        fileopener.StartInfo.FileName = "explorer.exe";
                        fileopener.StartInfo.Arguments = "\"" + fileOrDirectoryToOpen + "\"";
                        fileopener.Start();
                    }

                    //close the form to get out of the way
                    //this.Close();
                    closeWithoutSendingText();
                }
            }
        }

        protected void fileSearchTxt_KeyDown(object sender, KeyEventArgs e)
        {
            searchTxtKeyDownHandler(e, handleSelectionFromStatementsListBox);
        }

        protected void FilesAndDirectoriesDisplayListBox_KeyDown(object sender, KeyEventArgs e)
        {
            Action enterKeyDownAction = handleSelectionFromStatementsListBox;

            displayListBoxKeyDownHandler(e, null, enterKeyDownAction);
        }

        private void FileSearchForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (source != null)
            {
                this.Text = "File Search - Canceling";
                source.Cancel();
            }
        }
    }
}
