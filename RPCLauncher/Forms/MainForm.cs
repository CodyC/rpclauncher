﻿using IWshRuntimeLibrary;
using RPCLauncher.Domain;
using RPCLauncher.GlobalMontiors;
using RPCLauncher.Services;
using RPCLauncher.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPCLauncher.Forms
{
    public partial class MainForm : Form
    {
        readonly ClipboardManagementService clipboardManagementService = new ClipboardManagementService();
        readonly ShortcutKeyService shortcutKeyService = new ShortcutKeyService();
        readonly LanguageStylePreferencesService languageStylePreferencesService = new LanguageStylePreferencesService();
        readonly MessageGenerator messagesGenerator = new MessageGenerator();
        readonly CaseConversionService caseConversionService = new CaseConversionService();
        readonly TemplateGenerator templateGenerator = new TemplateGenerator();

        List<String> clipboardHistory = new List<string>();
        List<LanguageStylePreference> languageStylePreferences = new List<LanguageStylePreference>();

        BindingList<DataGridEntry> quickPasteEntries;
        BindingList<StatementToSendDisplayKeyValuePairs> commandsEntries;

        private bool closeFormMenuItemClicked = false;

        public MainForm()
        {
            InitializeComponent();

            //    //Pops the form up in the center of the screen containing the mouse cursor
            //    //See this link for more details: 
            //    //https://msdn.microsoft.com/en-us/library/system.windows.forms.form.centertoscreen(v=vs.110).aspx
            this.StartPosition = FormStartPosition.CenterScreen;

            this.WindowState = FormWindowState.Minimized;

            indexFileSystem();
            startIndexFileSystemSchedule();

            getClipboardHistory();

            ClipboardMonitor.OnClipboardChange += ClipboardMonitor_OnClipboardChange;
            ClipboardMonitor.Start();

            wireShortcutKeys();

            setupDGVs();
        }

        private async void startIndexFileSystemSchedule()
        {
            await Task.Run(() => FileSystemService.IndexFileSystemOnSchedule());
        }

        private async void indexFileSystem()
        {
            await Task.Run(() =>
            (
                FileSystemService.IndexFileSystem()
            ));
        }

        private async void getClipboardHistory()
        {
            await Task.Run(() =>
            (
                clipboardHistory = clipboardManagementService.GetClipboardHistoryFromConfig()
            ));
        }

        private void setupUI()
        {
            BraceStyleCB.Items.AddRange(BraceStyles.GetAllBraceStyleDisplayValues().ToArray());

            SpacingStyleCB.Items.AddRange(SpacingStyles.GetAllBraceStyleDisplayValues().ToArray());

            languageStylePreferences = ConfigUtils.GetLanguageStylePreferences();

            loadLastSelectedLanguage();

            fillUIWithLanguageStylePreferences();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            setupUI();
            this.Hide();
        }

        private void loadLastSelectedLanguage()
        {
            var appConfig = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
            String lastSelectedLanguage = ConfigUtils.GetConfigValue(ConfigUtils.LAST_SELECTED_LANGUAGE_KEY);

            if (String.IsNullOrWhiteSpace(lastSelectedLanguage))
            {
                //Use this as a default if we don't have one set via config.
                //If there is no selected language the statment generation will make blank statements
                lastSelectedLanguage = Languages.C_SHARP;
            }

            setSelectedLanguageRadioButton(lastSelectedLanguage);
        }

        public void ClipboardMonitor_OnClipboardChange(ClipboardFormat format, object data)
        {
            clipboardManagementService.HandleClipboardChange(clipboardHistory);
        }

        private void wireShortcutKeys()
        {
            wireGeneratedStatementsHotKey();

            wireAppendToClipboardExecutionHotKey();

            wireClipboardHistoryHotKey();

            wireCommonlyPastedStringsHotKey();

            wireFormattingToolsHotKey();

            wireHighlightsHotKey();

            wireFindFileHotKey();

            wireCommandExecutionHotKey();
        }

        private void wireFindFileHotKey()
        {
            //show Find Files dialog shortcut Ctrl + Shift + f
            if (ShortcutKeyService.FileSearchShortcutKey.Registered)
            {
                ShortcutKeyService.FileSearchShortcutKey.Unregister();
            }
            ShortcutKeyService.FileSearchShortcutKey.Register(this);
            ShortcutKeyService.FileSearchShortcutKey.Pressed +=
                delegate
                {
                    UIUtils.ShowTopMostFocusedDialog(new FileSearchForm());
                };
        }

        private void wireClipboardHistoryHotKey()
        {
            //Show clipboard history shortcut = Ctrl + Shift + v on Windows 10 Win + v wasn't recognized
            if (ShortcutKeyService.ClipboardHistoryShortcutKey.Registered)
            {
                ShortcutKeyService.ClipboardHistoryShortcutKey.Unregister();
            }
            ShortcutKeyService.ClipboardHistoryShortcutKey.Register(this);
            ShortcutKeyService.ClipboardHistoryShortcutKey.Pressed +=
                delegate
                {
                    showStatementsWrapper(showClipboardHistoryDisplay);
                };
        }

        private void wireAppendToClipboardExecutionHotKey()
        {
            //Append To Clipboard shortcut = Ctrl + Shift + c
            if (ShortcutKeyService.AppendToClipboardExecutionShortcutKey.Registered)
            {
                ShortcutKeyService.AppendToClipboardExecutionShortcutKey.Unregister();
            }

            ShortcutKeyService.AppendToClipboardExecutionShortcutKey.Register(this);
            ShortcutKeyService.AppendToClipboardExecutionShortcutKey.Pressed +=
                delegate
                {
                    String clipboardItemBeforeCopy = ClipboardUtils.GetTrimmedClipboardText();

                    //Trigger the copy and paste
                    SendKeysUtils.SendCopyCommand();

                    String appendedClipboardText = "";
                    if (clipboardHistory != null
                            && clipboardHistory.Count >= 1
                            && ClipboardMonitor.IsClipboardMonitored)
                    {
                        //Add the last clipboard history item together and send them to the clipboard
                        appendedClipboardText = clipboardHistory[1] + clipboardHistory[0];
                    }

                    if (!String.IsNullOrEmpty(appendedClipboardText))
                    {
                        Clipboard.SetText(appendedClipboardText);
                    }
                };
        }

        private void wireGeneratedStatementsHotKey()
        {
            //Log message creation shortcut = Ctrl + Shift + d
            if (ShortcutKeyService.GeneratedStatementsShortcutKey.Registered)
            {
                ShortcutKeyService.GeneratedStatementsShortcutKey.Unregister();
            }
            ShortcutKeyService.GeneratedStatementsShortcutKey.Register(this);
            ShortcutKeyService.GeneratedStatementsShortcutKey.Pressed +=
                delegate
                {
                    showStatementsWrapper(showGeneratedStatements);
                };
        }

        private void wireHighlightsHotKey()
        {
            //Show clipboard history shortcut = Ctrl + Shift + h on Windows 10 Win + v wasn't recognized
            if (ShortcutKeyService.HighlightsShortcutKey.Registered)
            {
                ShortcutKeyService.HighlightsShortcutKey.Unregister();
            }
            ShortcutKeyService.HighlightsShortcutKey.Register(this);
            ShortcutKeyService.HighlightsShortcutKey.Pressed +=
                delegate
                {
                    //Insert the oath of the wall here 
                    //Edit: I wrote this comment before they ruined Game of Thrones
                    ProcessWatcher.SetLastActive();
                    ProcessWatcher.StartWatch();
                    UIUtils.ShowTopMostFocusedDialog(new HighlightsForm());

                    ProcessWatcher.StopWatch();
                };
        }

        private void wireCommonlyPastedStringsHotKey()
        {
            //Quick Paste Items shortcut = Ctrl + Shift + e
            if (ShortcutKeyService.QuickPasteItemsShortcutKey.Registered)
            {
                ShortcutKeyService.QuickPasteItemsShortcutKey.Unregister();
            }
            ShortcutKeyService.QuickPasteItemsShortcutKey.Register(this);
            ShortcutKeyService.QuickPasteItemsShortcutKey.Pressed +=
                delegate
                {
                    showStatementsWrapper(showCommonlyPastedStrings);
                };
        }

        private void wireFormattingToolsHotKey()
        {
            //Log message creation shortcut = Ctrl + Shift + q
            if (ShortcutKeyService.FormattingToolsShortcutKey.Registered)
            {
                ShortcutKeyService.FormattingToolsShortcutKey.Unregister();
            }
            ShortcutKeyService.FormattingToolsShortcutKey.Register(this);
            ShortcutKeyService.FormattingToolsShortcutKey.Pressed +=
                delegate
                {
                    showStatementsWrapper(showFormattingToolStrings);
                };
        }

        private void wireCommandExecutionHotKey()
        {
            //Log message creation shortcut = Ctrl + Shift + x
            if (ShortcutKeyService.CommandExecutionShortcutKey.Registered)
            {
                ShortcutKeyService.CommandExecutionShortcutKey.Unregister();
            }
            ShortcutKeyService.CommandExecutionShortcutKey.Register(this);
            ShortcutKeyService.CommandExecutionShortcutKey.Pressed +=
                delegate
                {
                    showStatementsWrapper(showCommandExecutionDisplay);
                };
        }

        /// <summary>
        /// Wraps the given Action to show statements with the needed logic to start and stop watchers and processes, 
        /// that the underlying program needs when showing the form and sending keystrokes to other programs.
        /// 
        /// </summary>
        /// <param name="showStatementsAction"></param>
        private void showStatementsWrapper(Action showStatementsAction)
        {
            //Insert the Nights Watch Oath here 
            //http://store.hbo.com/game-of-thrones-nights-watch-oath-poster-11x17/detail.php?p=447555

            ProcessWatcher.SetLastActive();
            ProcessWatcher.StartWatch();
            showStatementsAction();

            ProcessWatcher.StopWatch();
        }

        private void showClipboardHistoryDisplay()
        {
            StatementToSendSelectionForm clipboardHistoryDisplay =
                new StatementToSendSelectionForm(clipboardHistory, "Clipboard History");
            UIUtils.ShowTopMostFocusedDialog(clipboardHistoryDisplay);
        }

        private void showCommandExecutionDisplay()
        {
            CommandExecutionForm commandExecutionForm = new CommandExecutionForm();
            UIUtils.ShowTopMostFocusedDialog(commandExecutionForm);
        }

        private void showCommonlyPastedStrings()
        {
            TokenReplacementService tokenReplacementService = new TokenReplacementService();

            List<String> tokenFilledQUickPasteItems =
                tokenReplacementService
                    .GetTokenFilledStrings(ConfigUtils.GetQuickPasteItems());

            StatementToSendSelectionForm generatedStatementsDisplay =
                new StatementToSendSelectionForm(tokenFilledQUickPasteItems, "Commonly Pasted Strings");
            UIUtils.ShowTopMostFocusedDialog(generatedStatementsDisplay);
        }

        private void showGeneratedStatements()
        {
            string textToProcess = ClipboardUtils.GetTrimmedClipboardText();

            string selectedLanguage = getSelectedLanguage();

            LanguageStylePreference currentLanguageStylePreference =
                languageStylePreferencesService
                    .GetLanguageStylePreferenceByLanguageName(selectedLanguage, languageStylePreferences);

            PossibleGeneratedStatements possibleGeneratedStatements =
                messagesGenerator
                    .GetPossibleGeneratedStatements(textToProcess, selectedLanguage, currentLanguageStylePreference);

            PossibleGeneratedTemplates possibleGeneratedTemplates =
                templateGenerator
                    .GetPossibleGeneratedTemplates(textToProcess, selectedLanguage);

            List<String> possibleStatementsAndTemplatesList =
                new List<string>()
                {
                    possibleGeneratedStatements.DebugStatement,
                    possibleGeneratedTemplates.StringNotNullCheck,
                    possibleGeneratedTemplates.NotNullCheck,
                    possibleGeneratedStatements.ObjectDeclaration,
                    possibleGeneratedStatements.CygwinConvertedPath,
                    possibleGeneratedTemplates.IfStatement,
                    possibleGeneratedTemplates.IfElseStatement,
                    possibleGeneratedTemplates.IfElseIfStatement,
                    possibleGeneratedTemplates.SwitchStatement,
                    possibleGeneratedStatements.SqlSelectStatement,
                    possibleGeneratedStatements.SqlUpdateStatement,
                    possibleGeneratedStatements.SqlInsertStatement,
                    possibleGeneratedStatements.CurrentUnixTimestamp,
                    possibleGeneratedStatements.RandomGUID,
                };

            //remove the empties
            possibleStatementsAndTemplatesList.RemoveAll(String.IsNullOrWhiteSpace);

            //remove the duplicates
            HashSet<String> duplicateRemoverSet = new HashSet<string>(possibleStatementsAndTemplatesList);
            List<String> uniqueGeneratedStatementsAndTemplates = new List<String>(duplicateRemoverSet);

            StatementToSendSelectionForm generatedStatementsDisplay =
                new StatementToSendSelectionForm(uniqueGeneratedStatementsAndTemplates, "Generated Statements");

            UIUtils.ShowTopMostFocusedDialog(generatedStatementsDisplay);
        }

        private async void showFormattingToolStrings()
        {
            String trimmedClipBoardText = ClipboardUtils.GetTrimmedClipboardText();
            var formattingToolStrings = new List<string>();

            String popUpTitle = "Formatting Tools";

            bool clipboardIsEmpty = String.IsNullOrWhiteSpace(trimmedClipBoardText);

            if (clipboardIsEmpty)
            {
                formattingToolStrings.Add("Please have JSON, XML, SQL, IPv4 Address, Phone Number or Text in your clipboard.");
            }
            else
            {
                if (JSONUtils.isJSONString(trimmedClipBoardText))
                {
                    await Task.Run(() =>
                    {
                        String prettyPrintedXML = JSONUtils.PrettyPrintJSON(trimmedClipBoardText);
                        formattingToolStrings.Add(prettyPrintedXML);

                        String escapedXML = JSONUtils.EscapeJSONString(trimmedClipBoardText);
                        formattingToolStrings.Add(escapedXML);
                    });
                    popUpTitle = "JSON Tools";

                }
                else if (XMLUtils.isXMLString(trimmedClipBoardText))
                {
                    await Task.Run(() =>
                    {
                        String prettyPrintedXML = XMLUtils.PrettyPrintXML(trimmedClipBoardText);
                        formattingToolStrings.Add(prettyPrintedXML);

                        String escapedXML = XMLUtils.EscapeXML(trimmedClipBoardText);
                        formattingToolStrings.Add(escapedXML);

                        String unescapedXML = XMLUtils.UnescapeXML(trimmedClipBoardText);
                        formattingToolStrings.Add(unescapedXML);
                    });
                    popUpTitle = "XML Tools";
                }
                else if (SqlUtils.IsValidSqlStatement(trimmedClipBoardText))
                {

                    await Task.Run(() =>
                    {
                        String prettyPrintedSql = SqlUtils.PrettyPrintSql(trimmedClipBoardText);
                        formattingToolStrings.Add(prettyPrintedSql);
                    });
                    popUpTitle = "SQL Tools";
                }
                else if (PhoneNumberUtils.IsUnformattedPhoneNumber(trimmedClipBoardText))
                {
                    await Task.Run(() =>
                    {
                        String prettyPrintedPhoneNumber = PhoneNumberUtils.FormatPhoneNumber(trimmedClipBoardText);
                        formattingToolStrings.Add(prettyPrintedPhoneNumber);
                    });
                    popUpTitle = "Phone Number Tools";
                }
                else if (IpAddressUtils.IsValidIpV4Address(trimmedClipBoardText))
                {
                    popUpTitle = "IP Address Tools";
                }
                else if (YouTubeUtils.IsValidYouTubeAddress(trimmedClipBoardText))
                {
                    await Task.Run(() =>
                    {
                        formattingToolStrings.Add(YouTubeUtils.TranscribeYouTubeVideo(trimmedClipBoardText));
                    });
                    popUpTitle = "YouTube Tools";
                }
                else
                {
                    await Task.Run(() =>
                    {
                        formattingToolStrings = caseConversionService.buildCaseConvertedStringsList(trimmedClipBoardText);
                    });

                    popUpTitle = "Text Tools";
                }
            }

            StatementToSendSelectionForm generatedStatementsDisplay = new StatementToSendSelectionForm(formattingToolStrings, popUpTitle);

            UIUtils.ShowTopMostFocusedDialog(generatedStatementsDisplay);
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            setNotifyIconVisibility();
        }

        private void setNotifyIconVisibility()
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                taskBarNotifyIcon.Visible = true;
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                taskBarNotifyIcon.Visible = false;
            }
        }

        private void taskBarNotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            ShowInTaskbar = true;
            taskBarNotifyIcon.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void languageRB_CheckedChanged(object sender, EventArgs e)
        {
            fillUIWithLanguageStylePreferences();
        }

        private void fillUIWithLanguageStylePreferences()
        {
            languageStylePreferences = ConfigUtils.GetLanguageStylePreferences();

            var selectedLanguageStylePreference =
                languageStylePreferencesService
                    .GetLanguageStylePreferenceByLanguageName(getSelectedLanguage(),
                                                              languageStylePreferences);

            BraceStyleCB.Enabled = true;

            if (selectedLanguageStylePreference.LanguageName != Languages.PYTHON)
            {
                //fill the brace style combo box
                string savedBraceStyle
                    = BraceStyles
                        .GetAllBraceStylesValueAndDisplayValuePairs()
                        .Find(x => x.Key.Equals(selectedLanguageStylePreference.BraceStyleKey))
                        .Value;

                BraceStyleCB.SelectedItem = savedBraceStyle;
            }
            else
            {
                BraceStyleCB.SelectedItem = "";
                BraceStyleCB.Enabled = false;
            }

            //fill the space style combo box
            string savedSpacingStyle
                = SpacingStyles
                    .GetAllSpacingStylesValueAndDisplayValuePairs()
                    .Find(x => x.Key.Equals(selectedLanguageStylePreference.SpacingStyleKey))
                    .Value;

            SpacingStyleCB.SelectedItem = savedSpacingStyle;

            AvailableLibrariesCheckBoxList.Items.Clear();

            //unassign the event handler so it won't change the config for the selected libriaries and stop us from setting them correctly
            this.AvailableLibrariesCheckBoxList.ItemCheck -= AvailableLibrariesCheckBoxList_ItemCheck;

            setSelectedLibraries(selectedLanguageStylePreference);

            //re-assign the event handler so it will track the changes in the config from the UI
            this.AvailableLibrariesCheckBoxList.ItemCheck += AvailableLibrariesCheckBoxList_ItemCheck;
        }

        private void setSelectedLibraries(LanguageStylePreference selectedLanguageStylePreference)
        {
            if (getSelectedLanguage().Equals(Languages.JAVA))
            {
                AvailableLibrariesCheckBoxList.Items.Add(JavaLibraryChoices.LOG_4_J);
                AvailableLibrariesCheckBoxList.Items.Add(JavaLibraryChoices.APACHE_STRING_UTILS);

                if (selectedLanguageStylePreference.AvailableLibraries != null)
                {
                    if (selectedLanguageStylePreference.AvailableLibraries.Contains(JavaLibraryChoices.LOG_4_J))
                    {
                        int log4JIndex = AvailableLibrariesCheckBoxList.Items.IndexOf(JavaLibraryChoices.LOG_4_J);

                        AvailableLibrariesCheckBoxList.SetItemChecked(log4JIndex, true);
                    }

                    if (selectedLanguageStylePreference.AvailableLibraries.Contains(JavaLibraryChoices.APACHE_STRING_UTILS))
                    {
                        int apacheStringUtilsIndex = AvailableLibrariesCheckBoxList.Items.IndexOf(JavaLibraryChoices.APACHE_STRING_UTILS);

                        AvailableLibrariesCheckBoxList.SetItemChecked(apacheStringUtilsIndex, true);
                    }
                }
            }
        }

        /// <summary>
        /// Get the language selected by the radio buttons.
        /// </summary>
        /// <returns></returns>
        private String getSelectedLanguage()
        {
            String selectedLanguage = null;

            if (CSharpRB.Checked)
            {
                selectedLanguage = Languages.C_SHARP;
            }
            else if (JavaRB.Checked)
            {
                selectedLanguage = Languages.JAVA;
            }
            else if (JavaScriptRB.Checked)
            {
                selectedLanguage = Languages.JAVASCRIPT;
            }
            else if (PythonRB.Checked)
            {
                selectedLanguage = Languages.PYTHON;
            }

            return selectedLanguage;
        }

        private String setSelectedLanguageRadioButton(String selectedLanguage)
        {
            AvailableLibrariesCheckBoxList.Items.Clear();

            if (selectedLanguage.Equals(Languages.C_SHARP))
            {
                CSharpRB.Checked = true;
            }
            else if (selectedLanguage.Equals(Languages.JAVA))
            {
                JavaRB.Checked = true;
            }
            else if (selectedLanguage.Equals(Languages.JAVASCRIPT))
            {
                JavaScriptRB.Checked = true;
            }
            else if (selectedLanguage.Equals(Languages.PYTHON))
            {
                PythonRB.Checked = true;
            }

            return selectedLanguage;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Cleanup before we exit, otherwise we get a dangling process and it
            //has to be killed before you can run the IDE again.  
            //I'm sure it would also be a memory leak in production.

            bool isExternalShutdown =
                e.CloseReason == CloseReason.WindowsShutDown
                    || e.CloseReason == CloseReason.TaskManagerClosing;

            if (closeFormMenuItemClicked || isExternalShutdown)
            {
                try
                {
                    clipboardManagementService.SaveClipboardHistoryToConfig(clipboardHistory);

                    saveQuickPasteItems();
                    saveCommandEntries();

                    //Save last selected language
                    ConfigUtils.UpdateSetting(ConfigUtils.LAST_SELECTED_LANGUAGE_KEY,
                                              getSelectedLanguage());

                    ProcessWatcher.StopWatch();
                    ClipboardMonitor.Stop();
                }
                catch (Exception)
                {
                    //Ignore this if something happens, we are already closing down.  There really isn't much we can do
                }
            }
            else
            {
                //Minimize instead of closing
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
            }
        }

        private void taskbarContextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            bool closeItemWasClicked = e.ClickedItem.Name.Equals(getCloseMenuItemName());
            bool enableShortcutKeysMenuItemWasClicked = e.ClickedItem.Name.Equals(getEnableShortcutKeysMenuItemName());
            bool disableShortcutKeysMenuItemWasClicked = e.ClickedItem.Name.Equals(getDisableShortcutKeysMenuItemName());
            bool enableClipboardMonitorMenuItemWasClicked = e.ClickedItem.Name.Equals(getEnableClipboardMonitoringMenuItemName());
            bool disableClipboardMonitorMenuItemWasClicked = e.ClickedItem.Name.Equals(getDisableClipboardMonitoringMenuItemName());

            if (closeItemWasClicked)
            {
                closeFormMenuItemClicked = true;
                this.Close();
            }
            else if (enableShortcutKeysMenuItemWasClicked)
            {
                shortcutKeyService.RegisterAllShortcutKeys(this);

                EnableShortcutKeysMenuItem.Visible = false;
                DisableShortcutKeysMenuItem.Visible = true;

                TaskbarContextMenuStrip.Refresh();
            }
            else if (disableShortcutKeysMenuItemWasClicked)
            {
                shortcutKeyService.UnregisterAllShortcutKeys();

                EnableShortcutKeysMenuItem.Visible = true;
                DisableShortcutKeysMenuItem.Visible = false;

                TaskbarContextMenuStrip.Refresh();
            }
            else if (enableClipboardMonitorMenuItemWasClicked)
            {
                ClipboardMonitor.Start();

                EnableClipboardMonitoringMenuItem.Visible = false;
                DisableClipboardMonitoringMenuItem.Visible = true;

                TaskbarContextMenuStrip.Refresh();
            }
            else if (disableClipboardMonitorMenuItemWasClicked)
            {
                ClipboardMonitor.Stop();

                EnableClipboardMonitoringMenuItem.Visible = true;
                DisableClipboardMonitoringMenuItem.Visible = false;

                TaskbarContextMenuStrip.Refresh();
            }
        }
        private static string getCloseMenuItemName()
        {
            return "CloseMenuItem";
        }

        private static string getEnableShortcutKeysMenuItemName()
        {
            return "EnableShortcutKeysMenuItem";
        }

        private static string getDisableShortcutKeysMenuItemName()
        {
            return "DisableShortcutKeysMenuItem";
        }

        private static string getEnableClipboardMonitoringMenuItemName()
        {
            return "EnableClipboardMonitoringMenuItem";
        }

        private static string getDisableClipboardMonitoringMenuItemName()
        {
            return "DisableClipboardMonitoringMenuItem";
        }

        private void BraceStyleCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            languageStylePreferencesService.
                SaveBraceStyleForSelectedLanguage(getSelectedLanguage(),
                                                  BraceStyleCB.SelectedItem as String,
                                                  languageStylePreferences);
        }

        private void SpacingStyleCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            languageStylePreferencesService.
                SaveSpacingStyleForSelectedLanguage(getSelectedLanguage(),
                                                    SpacingStyleCB.SelectedItem as String,
                                                    languageStylePreferences);
        }

        private void AvailableLibrariesCheckBoxList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox clb = (CheckedListBox)sender;
            // Switch off event handler
            clb.ItemCheck -= AvailableLibrariesCheckBoxList_ItemCheck;
            clb.SetItemCheckState(e.Index, e.NewValue);
            // Switch on event handler
            clb.ItemCheck += AvailableLibrariesCheckBoxList_ItemCheck;

            var checkedItems = new List<string>();

            foreach (String checkedItem in AvailableLibrariesCheckBoxList.CheckedItems)
            {
                checkedItems.Add(checkedItem);
            }

            languageStylePreferencesService.
                SaveLibrariesUsedForSelectedLanguage(getSelectedLanguage(),
                                                     checkedItems,
                                                     languageStylePreferences);
        }

        private void MainFormHelpButton_Click(object sender, EventArgs e)
        {
            try
            {
                String currentExecutablePath = Environment.GetCommandLineArgs()[0];
                String currentDirectory = Path.GetDirectoryName(currentExecutablePath);
                String readmePath = Path.Combine(currentDirectory, "Docs\\Readme.md");

                if (System.IO.File.Exists(readmePath))
                {
                    Process.Start(readmePath);
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Setup all the DataGridViews from the Settings page, including
        /// the binding data and columns
        /// </summary>
        private void setupDGVs()
        {
            DataGridEntryService dataGridEntryService = new DataGridEntryService();

            quickPasteEntries =
                new BindingList<DataGridEntry>(dataGridEntryService.GetEntries(ConfigUtils.GetQuickPasteItems()));
            setupDGV(quickPasteItemsDGV, "Quick Paste Items", quickPasteEntries);

            commandsEntries =
                new BindingList<StatementToSendDisplayKeyValuePairs>(ConfigUtils.GetCommandEntries());
            setupDGVForCommandExecutionForm(CommandEntriesSettingsDGV, commandsEntries);
        }

        private void setupDGV(DataGridView dgvToSetup,
                              String columnTitle,
                              BindingList<DataGridEntry> backingList)
        {
            quickPasteItemsDGV.AllowUserToAddRows = true;
            DGVUtils.AddColToDataGrid(dgvToSetup, "DataGridItem", columnTitle, false);
            DGVUtils.SetupGrid<DataGridEntry>(dgvToSetup, backingList);
        }

        private void setupDGVForCommandExecutionForm(DataGridView dgvToSetup,
                                                     BindingList<StatementToSendDisplayKeyValuePairs> backingList)
        {
            quickPasteItemsDGV.AllowUserToAddRows = true;

            DGVUtils.AddColToDataGrid(dgvToSetup, "Key", "Name", false);
            DGVUtils.AddColToDataGrid(dgvToSetup, "Value", "Command to Execute", false);

            DGVUtils.SetupGrid<StatementToSendDisplayKeyValuePairs>(dgvToSetup, backingList);
        }

        #region SaveCalls
        private void saveQuickPasteItems()
        {
            defaultSaveFromGrid(quickPasteEntries,
                                ConfigUtils.QUICK_PASTE_ITEMS_JSON_KEY);
        }

        private void saveCommandEntries()
        {
            defaultSaveFromGrid(commandsEntries,
                                ConfigUtils.COMMAND_ENTRIES_JSON_KEY);
        }

        /// <summary>
        /// Provides a default implementation to save data from the given backing store
        /// to the given config key value
        /// </summary>
        /// <param name="listToSave"></param>
        /// <param name="configKey"></param>
        private void defaultSaveFromGrid(BindingList<StatementToSendDisplayKeyValuePairs> listToSave, String configKey)
        {
            //remove the dupes by converting to a HashSet
            var setToSave = new HashSet<StatementToSendDisplayKeyValuePairs>(listToSave);

            List<StatementToSendDisplayKeyValuePairs> uniqueItemsToSave = new List<StatementToSendDisplayKeyValuePairs>(setToSave);

            //remove all empties
            uniqueItemsToSave.RemoveAll(x => String.IsNullOrWhiteSpace(x.Value));

            String quickPasteJson = JSONUtils.Serialize(uniqueItemsToSave);

            ConfigUtils.UpdateSetting(configKey, quickPasteJson);
        }

        /// <summary>
        /// Provides a default implementation to save data from the given backing store
        /// to the given config key value
        /// </summary>
        /// <param name="listToSave"></param>
        /// <param name="configKey"></param>
        private void defaultSaveFromGrid(BindingList<DataGridEntry> listToSave, String configKey)
        {
            DataGridEntryService dataGridEntryService = new DataGridEntryService();
            List<String> itemsToSave = dataGridEntryService.GetItems(listToSave);

            //remove the dupes by converting to a HashSet
            var setToSave = new HashSet<String>(itemsToSave);

            List<String> uniqueItemsToSave = new List<String>(setToSave);

            //remove all empties
            uniqueItemsToSave.RemoveAll(String.IsNullOrWhiteSpace);

            String quickPasteJson = JSONUtils.Serialize(uniqueItemsToSave);

            ConfigUtils.UpdateSetting(configKey, quickPasteJson);
        }

        #endregion

        #region KeyDownHandlers

        private void quickPasteItemsDGV_KeyDown(object sender, KeyEventArgs e)
        {
            dataGridViewDefaultKeyDownHandler(e, quickPasteEntries, quickPasteItemsDGV);
        }

        private void CommandSettingsDGV_KeyDown(object sender, KeyEventArgs e)
        {
            dataGridViewDefaultKeyDownHandler(e, commandsEntries, CommandEntriesSettingsDGV);
        }

        private void dataGridViewDefaultKeyDownHandler<T>(KeyEventArgs e,
                                                          BindingList<T> backingCollection,
                                                          DataGridView dgv)
        {
            bool enterWasPressed = e.KeyCode == Keys.Return;
            bool deleteWasPressed = e.KeyCode == Keys.Delete;

            if (enterWasPressed)
            {
                //add a new record and refresh
                backingCollection.Add(default(T));
                dgv.Refresh();
            }
            else if (deleteWasPressed)
            {
                backingCollection.RemoveAt(dgv.CurrentCell.RowIndex);
            }
        }

        #endregion

        #region HelpButtonHandlers

        private void quickPasteItemsHelpBtn_Click(object sender, EventArgs e)
        {
            String quickPasteItemsHelpText
                = "The grid above shows your quick paste items.  The quick paste items can be accessed by pressing:" +
                    Environment.NewLine +
                    "   Control + Shift + e" +
                    Environment.NewLine +
                    Environment.NewLine +
                    "In addition to standard text, you can use the tokens to replaced at run-time with a certain value.  " +

                    Environment.NewLine +
                    Environment.NewLine +

                    "For example, if 'Mars' is in your clipboard and 'Hello {clipboard}!' is a quick paste item," +
                    " when the text was sent from RPC Launcher the output would be: " +
                    Environment.NewLine +
                    "   'Hello Mars!'" +
                    Environment.NewLine +
                    Environment.NewLine +

                    getTemplateHelpText();

            MessageBox.Show(quickPasteItemsHelpText);
        }

        private String getTemplateHelpText()
        {
            return
            "Templates:" +
                Environment.NewLine +
                "   {clipboard} will insert the text in your clipboard" +
                Environment.NewLine +
                "   {guid} will insert a new GUID and " +
                Environment.NewLine +
                "   {unixTimeStamp} will insert the current Unix TimeStamp.";
        }

        private void FileSearchHelpBtn_Click(object sender, EventArgs e)
        {
            String fileSearchHelpText =
                "This grid will allow you to manage the directories that are searched when using the file search dialog.  " +
                Environment.NewLine +
                "The file search dialog  can be accessed by pressing:" +
                Environment.NewLine +
                "   Control + Shift + f" +

                Environment.NewLine +
                Environment.NewLine +

                @"Your home directory (usually C:\Users\{UserName} is included by default." +
                Environment.NewLine +
                Environment.NewLine +
                "If a file is chosen, it will be opened with the deafult program." +
                Environment.NewLine +
                "If a folder is chosen, it will be opened in explorer.";

            MessageBox.Show(fileSearchHelpText);
        }

        private void CommandSettingsHelpButton_Click(object sender, EventArgs e)
        {
            String commandEntriesHelpText =
                "This grid will allow you to manage the commands that are presented when using the command excecution dialog.  " +
                Environment.NewLine +
                "The command excecution dialog can be accessed by pressing:" +
                Environment.NewLine +
                "   Control + Shift + x." +
                Environment.NewLine +
                "This lists commands/scripts that can be run.  The Key column is what you will see in the pop-up window, and the Value column is what will be executed." +
                Environment.NewLine +
                "To copy the results of a script or command to your clipboard use | clip.exe ." +
                Environment.NewLine +
                "These commands also support the global templates. " +
                Environment.NewLine +
                getTemplateHelpText() +
                Environment.NewLine +
                getTemplateHelpText();

            MessageBox.Show(commandEntriesHelpText);
        }

        #endregion

        private void ShortcutKeysTabPage_Enter(object sender, EventArgs e)
        {
            // Valid options are 
            //A-Z
            //F1-F12 to be added in the future

            //With big hands you can do all of the following with your left hand
            //Q
            //A
            //Z
            //W
            //S
            //X
            //E
            //D
            //R
            //F
            //V
            //T
            //G
            //B
            //H

            //A, M, Z, Y are all very common global windows shortucts.  So we will skip those.

            //We need to make sure we only populate the combo with:
            //all the unselected options
            //and the value that is currently selected for that action

            Dictionary<String, Keys> shortcutKeysDictionary = ConfigUtils.GetShortcutKeysDictionary();

            Keys clipboardHistoryClipboardShortcutKey;
            shortcutKeysDictionary.TryGetValue(ConfigUtils.CLIPBOARD_HISTORY_COMMAND_JSON_KEY, out clipboardHistoryClipboardShortcutKey);
            List<Keys> clipboardHistoryAvailableShortcutKeys = getAvailableShortcutKeyOptions(ConfigUtils.CLIPBOARD_HISTORY_COMMAND_JSON_KEY);

            clipboardHistoryShortcutComboBox.DataSource = clipboardHistoryAvailableShortcutKeys;
            clipboardHistoryShortcutComboBox.SelectedIndex = clipboardHistoryAvailableShortcutKeys.IndexOf(clipboardHistoryClipboardShortcutKey);

            Keys quickPasteItemsShortcutKey;
            shortcutKeysDictionary.TryGetValue(ConfigUtils.QUICK_PASTE_ITEMS_COMMAND_JSON_KEY, out quickPasteItemsShortcutKey);
            List<Keys> quickPasteItemsAvailableShortcutKeys = getAvailableShortcutKeyOptions(ConfigUtils.QUICK_PASTE_ITEMS_COMMAND_JSON_KEY);

            QuickPasteShortcutComboBox.DataSource = quickPasteItemsAvailableShortcutKeys;
            QuickPasteShortcutComboBox.SelectedIndex = quickPasteItemsAvailableShortcutKeys.IndexOf(quickPasteItemsShortcutKey);

            Keys generatedStatementsShortcutKey;
            shortcutKeysDictionary.TryGetValue(ConfigUtils.GENERATED_STATEMENTS_COMMAND_JSON_KEY, out generatedStatementsShortcutKey);
            List<Keys> generatedStatementsAvailableShortcutKeys = getAvailableShortcutKeyOptions(ConfigUtils.GENERATED_STATEMENTS_COMMAND_JSON_KEY);

            GeneratedStatementsShortcutComboBox.DataSource = generatedStatementsAvailableShortcutKeys;
            GeneratedStatementsShortcutComboBox.SelectedIndex = generatedStatementsAvailableShortcutKeys.IndexOf(generatedStatementsShortcutKey);

            Keys highlightsShortcutKey;
            shortcutKeysDictionary.TryGetValue(ConfigUtils.HIGHLIGHT_COMMAND_JSON_KEY, out highlightsShortcutKey);
            List<Keys> highlightsAvailableShortcutKeys = getAvailableShortcutKeyOptions(ConfigUtils.HIGHLIGHT_COMMAND_JSON_KEY);

            HighlightsShortcutComboBox.DataSource = highlightsAvailableShortcutKeys;
            HighlightsShortcutComboBox.SelectedIndex = highlightsAvailableShortcutKeys.IndexOf(highlightsShortcutKey);

            Keys fileSearchShortcutKey;
            shortcutKeysDictionary.TryGetValue(ConfigUtils.FILE_SEARCH_COMMAND_JSON_KEY, out fileSearchShortcutKey);
            List<Keys> fileSearchAvailableShortcutKeys = getAvailableShortcutKeyOptions(ConfigUtils.FILE_SEARCH_COMMAND_JSON_KEY);

            FileSearchShortcutComoboBox.DataSource = fileSearchAvailableShortcutKeys;
            FileSearchShortcutComoboBox.SelectedIndex = fileSearchAvailableShortcutKeys.IndexOf(fileSearchShortcutKey);

            Keys formattingToolsShortcutKey;
            shortcutKeysDictionary.TryGetValue(ConfigUtils.FORMATTING_TOOLS_COMMAND_JSON_KEY, out formattingToolsShortcutKey);
            List<Keys> formattingToolsAvailableShortcutKeys = getAvailableShortcutKeyOptions(ConfigUtils.FORMATTING_TOOLS_COMMAND_JSON_KEY);

            FormattingToolsShortcutComboBox.DataSource = formattingToolsAvailableShortcutKeys;
            FormattingToolsShortcutComboBox.SelectedIndex = formattingToolsAvailableShortcutKeys.IndexOf(formattingToolsShortcutKey);

            Keys commandExecutionShortcutKey;
            shortcutKeysDictionary.TryGetValue(ConfigUtils.COMMAND_EXECUTION_JSON_KEY, out commandExecutionShortcutKey);
            List<Keys> commandExecutionAvailableShortcutKeys = getAvailableShortcutKeyOptions(ConfigUtils.COMMAND_EXECUTION_JSON_KEY);

            CommandExecutionShortcutComboBox.DataSource = commandExecutionAvailableShortcutKeys;
            CommandExecutionShortcutComboBox.SelectedIndex = commandExecutionAvailableShortcutKeys.IndexOf(commandExecutionShortcutKey);

            Keys appendToClipboardShortcutKey;
            shortcutKeysDictionary.TryGetValue(ConfigUtils.APPEND_TO_CLIPBOARD_COMMAND_JSON_KEY, out appendToClipboardShortcutKey);
            List<Keys> appendClipboardAvailableShortcutKeys = getAvailableShortcutKeyOptions(ConfigUtils.APPEND_TO_CLIPBOARD_COMMAND_JSON_KEY);

            AppendToClipboardShortcutComboBox.DataSource = appendClipboardAvailableShortcutKeys;
            AppendToClipboardShortcutComboBox.SelectedIndex = appendClipboardAvailableShortcutKeys.IndexOf(appendToClipboardShortcutKey);
        }

        private List<Keys> getAvailableShortcutKeyOptions(String selectedCommand)
        {
            List<Keys> availableShortcutKeys =
                new List<Keys>() { Keys.B, Keys.C,
                                   Keys.D, Keys.E, Keys.F,
                                   Keys.G, Keys.H, Keys.I,
                                   Keys.J, Keys.K, Keys.L,
                                   Keys.N, Keys.O,
                                   Keys.P, Keys.Q, Keys.R,
                                   Keys.T, Keys.U,
                                   Keys.V, Keys.W, Keys.X, };

            Dictionary<String, Keys> shortcutKeysDictionary = ConfigUtils.GetShortcutKeysDictionary();

            foreach (String currentCommandName in shortcutKeysDictionary.Keys)
            {
                availableShortcutKeys.RemoveAll(x => shortcutKeysDictionary.Values.ToList().Contains(x));
            }

            availableShortcutKeys.Add(shortcutKeysDictionary[selectedCommand]);

            availableShortcutKeys.Sort();

            return availableShortcutKeys;
        }

        private void clipboardHistoryShortcutComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateShortcutKeys();
        }
        private void GeneratedStatementsShortcutComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateShortcutKeys();
        }
        private void QuickPasteShortcutComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateShortcutKeys();
        }
        private void FileSearchShortcutComoboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateShortcutKeys();
        }
        private void HighlightsShortcutComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateShortcutKeys();
        }
        private void FormattingToolsShortcutComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateShortcutKeys();
        }
        private void CommandExecutionShortcutComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateShortcutKeys();
        }

        private void AppendToClipboardShortcutComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateShortcutKeys();
        }

        private void updateShortcutKeys()
        {
            var commandShortcutKeyDictionary = ConfigUtils.GetShortcutKeysDictionary();
            //shortcutKeyService.UnregisterAllShortcutKeys(commandShortcutKeyDictionary);

            if (clipboardHistoryShortcutComboBox.SelectedValue != null)
            {
                commandShortcutKeyDictionary[ConfigUtils.CLIPBOARD_HISTORY_COMMAND_JSON_KEY] = (Keys)clipboardHistoryShortcutComboBox.SelectedValue;
            }

            if (QuickPasteShortcutComboBox.SelectedValue != null)
            {
                commandShortcutKeyDictionary[ConfigUtils.QUICK_PASTE_ITEMS_COMMAND_JSON_KEY] = (Keys)QuickPasteShortcutComboBox.SelectedValue;
            }

            if (GeneratedStatementsShortcutComboBox.SelectedValue != null)
            {
                commandShortcutKeyDictionary[ConfigUtils.GENERATED_STATEMENTS_COMMAND_JSON_KEY] = (Keys)GeneratedStatementsShortcutComboBox.SelectedValue;
            }

            if (HighlightsShortcutComboBox.SelectedValue != null)
            {
                commandShortcutKeyDictionary[ConfigUtils.HIGHLIGHT_COMMAND_JSON_KEY] = (Keys)HighlightsShortcutComboBox.SelectedValue;
            }

            if (FileSearchShortcutComoboBox.SelectedValue != null)
            {
                commandShortcutKeyDictionary[ConfigUtils.FILE_SEARCH_COMMAND_JSON_KEY] = (Keys)FileSearchShortcutComoboBox.SelectedValue;
            }

            if (FormattingToolsShortcutComboBox.SelectedValue != null)
            {
                commandShortcutKeyDictionary[ConfigUtils.FORMATTING_TOOLS_COMMAND_JSON_KEY] = (Keys)FormattingToolsShortcutComboBox.SelectedValue;
            }

            if (CommandExecutionShortcutComboBox.SelectedValue != null)
            {
                commandShortcutKeyDictionary[ConfigUtils.COMMAND_EXECUTION_JSON_KEY] = (Keys)CommandExecutionShortcutComboBox.SelectedValue;
            }

            if (AppendToClipboardShortcutComboBox.SelectedValue != null)
            {
                commandShortcutKeyDictionary[ConfigUtils.APPEND_TO_CLIPBOARD_COMMAND_JSON_KEY] = (Keys)AppendToClipboardShortcutComboBox.SelectedValue;
            }

            ConfigUtils.UpdateSetting(ConfigUtils.COMMAND_SHORTCUT_KEYS_JSON_KEY, JSONUtils.Serialize(commandShortcutKeyDictionary));

            shortcutKeyService.UnregisterAllShortcutKeys();
            shortcutKeyService.RegisterAllShortcutKeys(this);

            wireShortcutKeys();
        }

        private void quickPasteItemsDGV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            saveQuickPasteItems();
        }

        private void CommandEntriesSettingsDGV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            saveCommandEntries();
        }

        private void LaunchOnStartupCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox launchOnStartupCheckbox = sender as CheckBox;

            if (launchOnStartupCheckbox != null)
            {
                string startUpFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
                string shortcutPath = startUpFolderPath + "\\" + Application.ProductName + ".lnk";

                if (launchOnStartupCheckbox.Checked)
                {
                    WshShell wshShell = new WshShell();

                    // Create the shortcut
                    if (!System.IO.File.Exists(shortcutPath))
                    {
                        IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)wshShell.CreateShortcut(shortcutPath);

                        shortcut.TargetPath = Application.ExecutablePath;
                        shortcut.WorkingDirectory = Application.StartupPath;
                        shortcut.Description = "Launch My Application";
                        // shortcut.IconLocation = Application.StartupPath + @"\App.ico";
                        shortcut.Save();
                    }
                }
                else
                {
                    try
                    {
                        System.IO.File.Delete(shortcutPath);
                    }
                    catch { }
                }

                ConfigUtils.UpdateSetting(ConfigUtils.LAUNCH_ON_STARTUP_JSON_KEY, JSONUtils.Serialize(launchOnStartupCheckbox.Checked));
            }
        }

        private void startupTabPage_Enter(object sender, EventArgs e)
        {
            LaunchOnStartupCheckBox.Checked = ConfigUtils.GetLaunchOnStartupConfigValue();
        }
    }
}
