﻿using RPCLauncher.Domain;
using RPCLauncher.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace RPCLauncher.Forms
{
    public partial class StatementToSendSelectionForm : Form
    {
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent,
                                                 IntPtr hwndChildAfter,
                                                 string lpszClass,
                                                 string lpszWindow);

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                                             int uMsg,
                                             int wParam,
                                             string lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        private List<String> statementsToDisplay { get; }

        ToolTip toolTip = new ToolTip();

        private String statementToSend = "";
        protected StatementToSendSelectionForm()
        {
            InitializeComponent();
            SetForegroundWindow(this.Handle);
        }

        public StatementToSendSelectionForm(List<String> statementsToDisplay, String windowTitle)
        {
            InitializeComponent();

            this.statementsToDisplay = statementsToDisplay;

            //Pops the form up in the center of the screen containing the mouse cursor
            //See this link for more details: 
            //https://msdn.microsoft.com/en-us/library/system.windows.forms.form.centertoscreen(v=vs.110).aspx
            this.StartPosition = FormStartPosition.CenterScreen;

            setPreserveClipboardTextCheckboxFromConfig();

            //Load the listbox with the clipboard history
            StatementsToDisplayListBox.DataSource = statementsToDisplay;
            if (StatementsToDisplayListBox.Items.Count > 0)
            {
                StatementsToDisplayListBox.SelectedIndex = 0;
            }

            SetForegroundWindow(this.Handle);

            this.Text = windowTitle;
        }

        private void setPreserveClipboardTextCheckboxFromConfig()
        {
            bool preserveClipboardText = true;

            bool preserveClipboardTextTryParseSucceeded =
                Boolean.TryParse(ConfigUtils.GetConfigValue(ConfigUtils.PRESERVE_CLIPBOARD_TEXT_KEY),
                                 out preserveClipboardText);

            if (preserveClipboardTextTryParseSucceeded)
            {
                preserveClipboardTextCheckBox.Checked = preserveClipboardText;
            }
        }

        private void deleteKeyDownAction()
        {
            deleteSelectedItemFromListBox(StatementsToDisplayListBox);
        }

        private void deleteSelectedItemFromListBox(ListBox listBox)
        {
            String selectedValue = listBox.SelectedItem as String;

            Object existingDataSource = StatementsToDisplayListBox.DataSource;
            listBox.DataSource = null;
            listBox.Items.Remove(selectedValue);

            List<String> existingDataSourceList = existingDataSource as List<String>;

            existingDataSourceList?.Remove(selectedValue);

            listBox.DataSource = existingDataSourceList;
        }

        protected void StatementsToDisplayListBox_KeyDown(object sender, KeyEventArgs e)
        {
            Action enterKeyDownAction = handleSelectionFromStatementsListBox;

            displayListBoxKeyDownHandler(e, deleteKeyDownAction, enterKeyDownAction);
        }

        /// <summary>
        /// Provides a default key down handler for the display list box.  Allows hooks into delete and enter key presses.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="deleteKeyDownAction"></param>
        /// <param name="enterKeyDownAction"></param>
        protected void displayListBoxKeyDownHandler(KeyEventArgs e, Action deleteKeyDownAction, Action enterKeyDownAction)
        {
            bool enterWasPressed = KeyHandlerUtils.EnterWasPressed(e);
            bool deleteWasPressed = KeyHandlerUtils.DeleteWasPressed(e);
            bool escapeWasPressed = KeyHandlerUtils.EscapeWasPressed(e);
            bool upArrowWasPressed = KeyHandlerUtils.UpArrowWasPressed(e);
            bool downArrowWasPressed = KeyHandlerUtils.DownArrowWasPressed(e);

            if (enterWasPressed)
            {
                if (enterKeyDownAction != null)
                {
                    enterKeyDownAction.Invoke();
                }
            }
            else if (deleteWasPressed)
            {
                if (deleteKeyDownAction != null)
                {
                    deleteKeyDownAction.Invoke();
                }
            }
            else if (upArrowWasPressed)
            {
                //if the topmost element is selected, roll down to the bottom
                if (StatementsToDisplayListBox.SelectedIndex == 0)
                {
                    StatementsToDisplayListBox.SelectedIndex = StatementsToDisplayListBox.Items.Count - 1;

                    //Don't worry, we have it handled!  
                    //Don't let the key event propagate, or it will choose the next to last and not the next.
                    e.Handled = true;
                }
            }
            else if (downArrowWasPressed)
            {
                //Make sure there is an index selected
                if (StatementsToDisplayListBox.SelectedIndex > -1)
                {
                    //if the last element is selected, roll down to the bottom
                    int lastIndex = StatementsToDisplayListBox.Items.Count - 1;

                    if (StatementsToDisplayListBox.SelectedIndex == lastIndex)
                    {
                        StatementsToDisplayListBox.SelectedIndex = 0;

                        //Don't worry, we have it handled!  
                        //Don't let the key event propagate, or it will choose the next to last and not the next.
                        e.Handled = true;
                    }
                }
                else if (escapeWasPressed)
                {
                    // close the form if they hit escape at any point
                    closeWithoutSendingText();
                }
            }
        }

        private void StatementsToDisplayListBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //All the special cases such as arrow keys, enter and escape are handled in the 
            //keyDown handler

            //Here if we have a printable key, we want to focus on the search box and 
            //send the text to it.
            setFocusOnSearchTxt();
            SearchTxt.AppendText(e.KeyChar.ToString());
        }

        private void setFocusOnSearchTxt()
        {
            //This check stops the control from relenqusihing focus
            //when the control already has focus.
            if (!SearchTxt.Focus())
            {
                SearchTxt.Focus();
            }
        }

        private void StatementsToDisplayListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            handleSelectionFromStatementsListBox();
        }

        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            handleSelectionFromStatementsListBox();
        }

        private void StatementsToDisplayListBox_DoubleClick(object sender, EventArgs e)
        {
            handleSelectionFromStatementsListBox();
        }

        /// <summary>
        /// Handle the action of a user selecting an item from the listbox.
        /// This should be overidden in subclasses to provide a new usage for a selection
        /// instead of rewiring all the event handlers
        /// </summary>
        public virtual void handleSelectionFromStatementsListBox()
        {
            statementToSend = StatementsToDisplayListBox.SelectedItem as String;
            this.Close();
        }

        private void savePreserveClipboardTextToConfig()
        {
            bool preserveClipboardText = preserveClipboardTextCheckBox.Checked;
            ConfigUtils.UpdateSetting(ConfigUtils.PRESERVE_CLIPBOARD_TEXT_KEY, preserveClipboardText.ToString());
        }

        protected void searchTxt_KeyDown(object sender, KeyEventArgs e)
        {
            searchTxtKeyDownHandler(e, handleSelectionFromStatementsListBox);
        }

        /// <summary>
        /// Provides a default key handler for the searchTxt field with a hook to catch the enter key being pressed.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="enterKeyDownAction"></param>
        protected void searchTxtKeyDownHandler(KeyEventArgs e, Action enterKeyDownAction)
        {
            if (KeyHandlerUtils.EnterWasPressed(e))
            {
                if (enterKeyDownAction != null)
                {
                    enterKeyDownAction.Invoke();
                }
            }
            else if (KeyHandlerUtils.UpArrowWasPressed(e))
            {
                StatementsToDisplayListBox.Focus();

                //if the topmost element is selected, roll down to the bottom
                if (StatementsToDisplayListBox.SelectedIndex == 0)
                {
                    StatementsToDisplayListBox.SelectedIndex = StatementsToDisplayListBox.Items.Count - 1;

                    //Don't worry, we have it handled!  
                    //Don't let the key event propagate, or it will choose the next to last and not the next.
                    e.Handled = true;
                }
            }
            else if (KeyHandlerUtils.DownArrowWasPressed(e))
            {
                StatementsToDisplayListBox.Focus();
                if (StatementsToDisplayListBox.SelectedIndex > -1)
                {
                    // Move down one index, this will select the next item.
                    //We want this bc enter will select the highlighted item

                    if (StatementsToDisplayListBox.SelectedIndex + 1 < StatementsToDisplayListBox.Items.Count)
                    {
                        StatementsToDisplayListBox.SelectedIndex += 1;
                    }
                }
            }
            else if (KeyHandlerUtils.EscapeWasPressed(e))
            {
                closeWithoutSendingText();
            }
        }

        protected void closeWithoutSendingText()
        {
            StatementsToDisplayListBox.SelectedItem = null;
            this.Close();
        }

        protected void searchTxt_TextChanged(object sender, EventArgs e)
        {
            //This blacklist will handle any characters that will not display in the text box,
            //but still need handled as keystrokes.  
            //Backspace is one example.  We don't to search for '\b' but we do want to be able to backspace.

            if (KeyHandlerUtils.ContainsEscapeKey(SearchTxt.Text))
            {
                // close the form if they hit escape at any point
                closeWithoutSendingText();
            }
            else
            {
                if (!String.IsNullOrEmpty(SearchTxt.Text))
                {
                    if (UIUtils.ContainsBlackListCharacter(SearchTxt))
                    {
                        SearchTxt.Text = "";
                    }
                    else
                    {
                        searchStatementsToSend(SearchTxt.Text);
                    }
                }
                else
                {
                    StatementsToDisplayListBox.BeginUpdate();
                    StatementsToDisplayListBox.DataSource = statementsToDisplay;
                    StatementsToDisplayListBox.EndUpdate();
                }
            }
        }

        private void searchStatementsToSend(String textToFind)
        {
            if (!String.IsNullOrEmpty(textToFind))
            {
                if (statementsToDisplay != null)
                {
                    var matchingSearchItems = new List<String>();

                    foreach (String currentListOption in statementsToDisplay)
                    {
                        bool isMatch = StringUtils.Contains(currentListOption, textToFind);

                        if (isMatch)
                        {
                            matchingSearchItems.Add(currentListOption);
                        }
                    }

                    StatementsToDisplayListBox.BeginUpdate();
                    StatementsToDisplayListBox.DataSource = matchingSearchItems;
                    StatementsToDisplayListBox.EndUpdate();
                }
            }
        }

        private void StatementsToDisplayListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var listBox = (sender as ListBox);

            string selectedItemText = listBox.SelectedItem as String;

            toolTip.Hide(listBox);

            if (!String.IsNullOrEmpty(selectedItemText)
                    && (selectedItemText.Length > 100
                            || selectedItemText.Contains(Environment.NewLine)))
            {
                // Set up the delays for the ToolTip.
                toolTip.AutoPopDelay = 5000;
                toolTip.InitialDelay = 3000;
                toolTip.ReshowDelay = 3000;
                toolTip.BackColor = Color.Black;
                toolTip.ForeColor = Color.LightSkyBlue;
                // Force the ToolTip text to be displayed whether or not the form is active.
                //toolTip.ShowAlways = true;

                // Set up the ToolTip text for the Button and Checkbox.                
                //toolTip.SetToolTip(listBox, selectedItemText);
                toolTip.Show(selectedItemText, listBox);
            }
        }

        private void StatementToSendSelectionForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //The Dispose call is critical.  Wihtout this I can't guarantee the form isn't closed.  
            //This seems to make a huge difference and fixes the issue of the form grabbing focus
            //and swallowing the pasted text.
            this.Dispose();

            if (!String.IsNullOrEmpty(statementToSend))
            {
                SendKeysUtils.SendTextToWindow(ProcessWatcher.LastHandle, statementToSend, preserveClipboardTextCheckBox.Checked);
            }
      
            savePreserveClipboardTextToConfig();
        }      
    }
}
