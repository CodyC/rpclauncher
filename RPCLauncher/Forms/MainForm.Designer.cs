﻿namespace RPCLauncher.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            taskBarNotifyIcon = new System.Windows.Forms.NotifyIcon(components);
            TaskbarContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(components);
            EnableShortcutKeysMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            DisableShortcutKeysMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            EnableClipboardMonitoringMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            DisableClipboardMonitoringMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            CloseMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            QuickPasteTabPage = new System.Windows.Forms.TabPage();
            QuickPasteItemsHelpBtn = new System.Windows.Forms.Button();
            quickPasteItemsDGV = new System.Windows.Forms.DataGridView();
            ConfigTP = new System.Windows.Forms.TabPage();
            MainFormHelpButton = new System.Windows.Forms.Button();
            LibrariesUsedLbl = new System.Windows.Forms.Label();
            AvailableLibrariesCheckBoxList = new System.Windows.Forms.CheckedListBox();
            LanguageLbl = new System.Windows.Forms.Label();
            CSharpRB = new System.Windows.Forms.RadioButton();
            SpacingStyleLbl = new System.Windows.Forms.Label();
            JavaRB = new System.Windows.Forms.RadioButton();
            SpacingStyleCB = new System.Windows.Forms.ComboBox();
            JavaScriptRB = new System.Windows.Forms.RadioButton();
            BraceStyleLbl = new System.Windows.Forms.Label();
            PythonRB = new System.Windows.Forms.RadioButton();
            BraceStyleCB = new System.Windows.Forms.ComboBox();
            MainFormTabControl = new System.Windows.Forms.TabControl();
            CommandsTabPage = new System.Windows.Forms.TabPage();
            CommandEntriesSettingsDGV = new System.Windows.Forms.DataGridView();
            CommandSettingsHelpButton = new System.Windows.Forms.Button();
            ShortcutKeysTabPage = new System.Windows.Forms.TabPage();
            CommandExecutionShortcutControlShiftLabel = new System.Windows.Forms.Label();
            CommandExecutionShortcutComboBox = new System.Windows.Forms.ComboBox();
            CommandExecutionShortcutLabel = new System.Windows.Forms.Label();
            FormattingToolsShortcutControlShiftLabel = new System.Windows.Forms.Label();
            FormattingToolsShortcutComboBox = new System.Windows.Forms.ComboBox();
            FormattingToolsShortcutLabel = new System.Windows.Forms.Label();
            FileSearchShortcutControlShiftLabel = new System.Windows.Forms.Label();
            HighlightsShortcutComboBox = new System.Windows.Forms.ComboBox();
            HighlightsShortcutLabel = new System.Windows.Forms.Label();
            GeneratedStatementsControlShiftLabel = new System.Windows.Forms.Label();
            GeneratedStatementsShortcutComboBox = new System.Windows.Forms.ComboBox();
            GeneratedStatementsLabel = new System.Windows.Forms.Label();
            QuickPasteShortcutControlShiftLabel = new System.Windows.Forms.Label();
            QuickPasteShortcutComboBox = new System.Windows.Forms.ComboBox();
            QuickPasteShortcutLabel = new System.Windows.Forms.Label();
            clipboardHistoryControlShiftLabel = new System.Windows.Forms.Label();
            fileSearchControlShiftLabel = new System.Windows.Forms.Label();
            FileSearchShortcutComoboBox = new System.Windows.Forms.ComboBox();
            FileSearchShortcutLbl = new System.Windows.Forms.Label();
            clipboardHistoryShortcutComboBox = new System.Windows.Forms.ComboBox();
            ClipboardHistoryShortcutLbl = new System.Windows.Forms.Label();
            uiSettingsPage = new System.Windows.Forms.TabPage();
            startupTabPage = new System.Windows.Forms.TabPage();
            LaunchOnStartupCheckBox = new System.Windows.Forms.CheckBox();
            AppendToClipboardShortcutControlShiftLabel = new System.Windows.Forms.Label();
            AppendToClipboardShortcutComboBox = new System.Windows.Forms.ComboBox();
            AppendToClipboardLabel = new System.Windows.Forms.Label();
            TaskbarContextMenuStrip.SuspendLayout();
            QuickPasteTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)quickPasteItemsDGV).BeginInit();
            ConfigTP.SuspendLayout();
            MainFormTabControl.SuspendLayout();
            CommandsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)CommandEntriesSettingsDGV).BeginInit();
            ShortcutKeysTabPage.SuspendLayout();
            startupTabPage.SuspendLayout();
            SuspendLayout();
            // 
            // taskBarNotifyIcon
            // 
            taskBarNotifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            taskBarNotifyIcon.ContextMenuStrip = TaskbarContextMenuStrip;
            taskBarNotifyIcon.Icon = (System.Drawing.Icon)resources.GetObject("taskBarNotifyIcon.Icon");
            taskBarNotifyIcon.Text = "RPC Launcher";
            taskBarNotifyIcon.Visible = true;
            taskBarNotifyIcon.MouseDoubleClick += taskBarNotifyIcon_MouseDoubleClick;
            // 
            // TaskbarContextMenuStrip
            // 
            TaskbarContextMenuStrip.ImageScalingSize = new System.Drawing.Size(28, 28);
            TaskbarContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { EnableShortcutKeysMenuItem, DisableShortcutKeysMenuItem, EnableClipboardMonitoringMenuItem, DisableClipboardMonitoringMenuItem, CloseMenuItem });
            TaskbarContextMenuStrip.Name = "taskbarContextMenuStrip";
            TaskbarContextMenuStrip.Size = new System.Drawing.Size(277, 124);
            TaskbarContextMenuStrip.ItemClicked += taskbarContextMenuStrip_ItemClicked;
            // 
            // EnableShortcutKeysMenuItem
            // 
            EnableShortcutKeysMenuItem.BackColor = System.Drawing.Color.Black;
            EnableShortcutKeysMenuItem.ForeColor = System.Drawing.Color.LightSkyBlue;
            EnableShortcutKeysMenuItem.Name = "EnableShortcutKeysMenuItem";
            EnableShortcutKeysMenuItem.Size = new System.Drawing.Size(276, 24);
            EnableShortcutKeysMenuItem.Text = "Enable Hot Keys";
            EnableShortcutKeysMenuItem.Visible = false;
            // 
            // DisableShortcutKeysMenuItem
            // 
            DisableShortcutKeysMenuItem.BackColor = System.Drawing.Color.Black;
            DisableShortcutKeysMenuItem.ForeColor = System.Drawing.Color.LightSkyBlue;
            DisableShortcutKeysMenuItem.Name = "DisableShortcutKeysMenuItem";
            DisableShortcutKeysMenuItem.Size = new System.Drawing.Size(276, 24);
            DisableShortcutKeysMenuItem.Text = "Disable Hot Keys";
            // 
            // EnableClipboardMonitoringMenuItem
            // 
            EnableClipboardMonitoringMenuItem.BackColor = System.Drawing.Color.Black;
            EnableClipboardMonitoringMenuItem.ForeColor = System.Drawing.Color.LightSkyBlue;
            EnableClipboardMonitoringMenuItem.Name = "EnableClipboardMonitoringMenuItem";
            EnableClipboardMonitoringMenuItem.Size = new System.Drawing.Size(276, 24);
            EnableClipboardMonitoringMenuItem.Text = "Enable Clipboard Monitoring";
            EnableClipboardMonitoringMenuItem.Visible = false;
            // 
            // DisableClipboardMonitoringMenuItem
            // 
            DisableClipboardMonitoringMenuItem.BackColor = System.Drawing.Color.Black;
            DisableClipboardMonitoringMenuItem.ForeColor = System.Drawing.Color.LightSkyBlue;
            DisableClipboardMonitoringMenuItem.Name = "DisableClipboardMonitoringMenuItem";
            DisableClipboardMonitoringMenuItem.Size = new System.Drawing.Size(276, 24);
            DisableClipboardMonitoringMenuItem.Text = "Disable Clipboard Monitoring";
            // 
            // CloseMenuItem
            // 
            CloseMenuItem.BackColor = System.Drawing.Color.Black;
            CloseMenuItem.ForeColor = System.Drawing.Color.LightSkyBlue;
            CloseMenuItem.Name = "CloseMenuItem";
            CloseMenuItem.Size = new System.Drawing.Size(276, 24);
            CloseMenuItem.Text = "Close";
            // 
            // QuickPasteTabPage
            // 
            QuickPasteTabPage.BackColor = System.Drawing.Color.Black;
            QuickPasteTabPage.Controls.Add(QuickPasteItemsHelpBtn);
            QuickPasteTabPage.Controls.Add(quickPasteItemsDGV);
            QuickPasteTabPage.Location = new System.Drawing.Point(4, 29);
            QuickPasteTabPage.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            QuickPasteTabPage.Name = "QuickPasteTabPage";
            QuickPasteTabPage.Padding = new System.Windows.Forms.Padding(2, 4, 2, 4);
            QuickPasteTabPage.Size = new System.Drawing.Size(518, 562);
            QuickPasteTabPage.TabIndex = 2;
            QuickPasteTabPage.Text = "Quick Paste Items";
            // 
            // QuickPasteItemsHelpBtn
            // 
            QuickPasteItemsHelpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            QuickPasteItemsHelpBtn.Location = new System.Drawing.Point(6, 6);
            QuickPasteItemsHelpBtn.Margin = new System.Windows.Forms.Padding(2);
            QuickPasteItemsHelpBtn.Name = "QuickPasteItemsHelpBtn";
            QuickPasteItemsHelpBtn.Size = new System.Drawing.Size(73, 39);
            QuickPasteItemsHelpBtn.TabIndex = 2;
            QuickPasteItemsHelpBtn.Text = "&Help";
            QuickPasteItemsHelpBtn.UseVisualStyleBackColor = true;
            // 
            // quickPasteItemsDGV
            // 
            quickPasteItemsDGV.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            quickPasteItemsDGV.BackgroundColor = System.Drawing.Color.Black;
            quickPasteItemsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.142858F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            quickPasteItemsDGV.DefaultCellStyle = dataGridViewCellStyle3;
            quickPasteItemsDGV.GridColor = System.Drawing.Color.Black;
            quickPasteItemsDGV.Location = new System.Drawing.Point(6, 50);
            quickPasteItemsDGV.Margin = new System.Windows.Forms.Padding(2);
            quickPasteItemsDGV.Name = "quickPasteItemsDGV";
            quickPasteItemsDGV.RowHeadersWidth = 62;
            quickPasteItemsDGV.RowTemplate.Height = 31;
            quickPasteItemsDGV.Size = new System.Drawing.Size(479, 488);
            quickPasteItemsDGV.TabIndex = 1;
            quickPasteItemsDGV.CellEndEdit += quickPasteItemsDGV_CellEndEdit;
            // 
            // ConfigTP
            // 
            ConfigTP.BackColor = System.Drawing.Color.Black;
            ConfigTP.Controls.Add(MainFormHelpButton);
            ConfigTP.Controls.Add(LibrariesUsedLbl);
            ConfigTP.Controls.Add(AvailableLibrariesCheckBoxList);
            ConfigTP.Controls.Add(LanguageLbl);
            ConfigTP.Controls.Add(CSharpRB);
            ConfigTP.Controls.Add(SpacingStyleLbl);
            ConfigTP.Controls.Add(JavaRB);
            ConfigTP.Controls.Add(SpacingStyleCB);
            ConfigTP.Controls.Add(JavaScriptRB);
            ConfigTP.Controls.Add(BraceStyleLbl);
            ConfigTP.Controls.Add(PythonRB);
            ConfigTP.Controls.Add(BraceStyleCB);
            ConfigTP.Location = new System.Drawing.Point(4, 29);
            ConfigTP.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            ConfigTP.Name = "ConfigTP";
            ConfigTP.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            ConfigTP.Size = new System.Drawing.Size(518, 562);
            ConfigTP.TabIndex = 0;
            ConfigTP.Text = "Code";
            // 
            // MainFormHelpButton
            // 
            MainFormHelpButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            MainFormHelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            MainFormHelpButton.Location = new System.Drawing.Point(6, 504);
            MainFormHelpButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            MainFormHelpButton.Name = "MainFormHelpButton";
            MainFormHelpButton.Size = new System.Drawing.Size(78, 37);
            MainFormHelpButton.TabIndex = 15;
            MainFormHelpButton.Text = "&Help";
            MainFormHelpButton.UseVisualStyleBackColor = true;
            MainFormHelpButton.Click += MainFormHelpButton_Click;
            // 
            // LibrariesUsedLbl
            // 
            LibrariesUsedLbl.AutoSize = true;
            LibrariesUsedLbl.Location = new System.Drawing.Point(2, 260);
            LibrariesUsedLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            LibrariesUsedLbl.Name = "LibrariesUsedLbl";
            LibrariesUsedLbl.Size = new System.Drawing.Size(105, 20);
            LibrariesUsedLbl.TabIndex = 14;
            LibrariesUsedLbl.Text = "Libraries Used:";
            // 
            // AvailableLibrariesCheckBoxList
            // 
            AvailableLibrariesCheckBoxList.BackColor = System.Drawing.Color.Black;
            AvailableLibrariesCheckBoxList.ForeColor = System.Drawing.Color.LightSkyBlue;
            AvailableLibrariesCheckBoxList.FormattingEnabled = true;
            AvailableLibrariesCheckBoxList.Location = new System.Drawing.Point(6, 294);
            AvailableLibrariesCheckBoxList.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            AvailableLibrariesCheckBoxList.Name = "AvailableLibrariesCheckBoxList";
            AvailableLibrariesCheckBoxList.Size = new System.Drawing.Size(333, 70);
            AvailableLibrariesCheckBoxList.TabIndex = 13;
            AvailableLibrariesCheckBoxList.ItemCheck += AvailableLibrariesCheckBoxList_ItemCheck;
            // 
            // LanguageLbl
            // 
            LanguageLbl.AutoSize = true;
            LanguageLbl.Location = new System.Drawing.Point(2, 3);
            LanguageLbl.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            LanguageLbl.Name = "LanguageLbl";
            LanguageLbl.Size = new System.Drawing.Size(77, 20);
            LanguageLbl.TabIndex = 4;
            LanguageLbl.Text = "Language:";
            // 
            // CSharpRB
            // 
            CSharpRB.AutoSize = true;
            CSharpRB.Location = new System.Drawing.Point(6, 29);
            CSharpRB.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            CSharpRB.Name = "CSharpRB";
            CSharpRB.Size = new System.Drawing.Size(48, 24);
            CSharpRB.TabIndex = 0;
            CSharpRB.Text = "C#";
            CSharpRB.UseVisualStyleBackColor = true;
            CSharpRB.CheckedChanged += languageRB_CheckedChanged;
            // 
            // SpacingStyleLbl
            // 
            SpacingStyleLbl.AutoSize = true;
            SpacingStyleLbl.Location = new System.Drawing.Point(2, 217);
            SpacingStyleLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            SpacingStyleLbl.Name = "SpacingStyleLbl";
            SpacingStyleLbl.Size = new System.Drawing.Size(88, 20);
            SpacingStyleLbl.TabIndex = 11;
            SpacingStyleLbl.Text = "Space Style:";
            // 
            // JavaRB
            // 
            JavaRB.AutoSize = true;
            JavaRB.Checked = true;
            JavaRB.Location = new System.Drawing.Point(6, 63);
            JavaRB.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            JavaRB.Name = "JavaRB";
            JavaRB.Size = new System.Drawing.Size(58, 24);
            JavaRB.TabIndex = 1;
            JavaRB.TabStop = true;
            JavaRB.Text = "Java";
            JavaRB.UseVisualStyleBackColor = true;
            JavaRB.CheckedChanged += languageRB_CheckedChanged;
            // 
            // SpacingStyleCB
            // 
            SpacingStyleCB.BackColor = System.Drawing.Color.Black;
            SpacingStyleCB.ForeColor = System.Drawing.Color.LightSkyBlue;
            SpacingStyleCB.FormattingEnabled = true;
            SpacingStyleCB.Location = new System.Drawing.Point(94, 212);
            SpacingStyleCB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            SpacingStyleCB.Name = "SpacingStyleCB";
            SpacingStyleCB.Size = new System.Drawing.Size(126, 28);
            SpacingStyleCB.TabIndex = 10;
            SpacingStyleCB.SelectedIndexChanged += SpacingStyleCB_SelectedIndexChanged;
            // 
            // JavaScriptRB
            // 
            JavaScriptRB.AutoSize = true;
            JavaScriptRB.Location = new System.Drawing.Point(6, 97);
            JavaScriptRB.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            JavaScriptRB.Name = "JavaScriptRB";
            JavaScriptRB.Size = new System.Drawing.Size(96, 24);
            JavaScriptRB.TabIndex = 2;
            JavaScriptRB.Text = "JavaScript";
            JavaScriptRB.UseVisualStyleBackColor = true;
            JavaScriptRB.CheckedChanged += languageRB_CheckedChanged;
            // 
            // BraceStyleLbl
            // 
            BraceStyleLbl.AutoSize = true;
            BraceStyleLbl.Location = new System.Drawing.Point(2, 171);
            BraceStyleLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            BraceStyleLbl.Name = "BraceStyleLbl";
            BraceStyleLbl.Size = new System.Drawing.Size(85, 20);
            BraceStyleLbl.TabIndex = 9;
            BraceStyleLbl.Text = "Brace Style:";
            // 
            // PythonRB
            // 
            PythonRB.AutoSize = true;
            PythonRB.Location = new System.Drawing.Point(6, 129);
            PythonRB.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            PythonRB.Name = "PythonRB";
            PythonRB.Size = new System.Drawing.Size(75, 24);
            PythonRB.TabIndex = 3;
            PythonRB.Text = "Python";
            PythonRB.UseVisualStyleBackColor = true;
            PythonRB.CheckedChanged += languageRB_CheckedChanged;
            // 
            // BraceStyleCB
            // 
            BraceStyleCB.BackColor = System.Drawing.Color.Black;
            BraceStyleCB.ForeColor = System.Drawing.Color.LightSkyBlue;
            BraceStyleCB.FormattingEnabled = true;
            BraceStyleCB.Location = new System.Drawing.Point(94, 171);
            BraceStyleCB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            BraceStyleCB.Name = "BraceStyleCB";
            BraceStyleCB.Size = new System.Drawing.Size(126, 28);
            BraceStyleCB.TabIndex = 8;
            BraceStyleCB.SelectedIndexChanged += BraceStyleCB_SelectedIndexChanged;
            // 
            // MainFormTabControl
            // 
            MainFormTabControl.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            MainFormTabControl.Controls.Add(ConfigTP);
            MainFormTabControl.Controls.Add(QuickPasteTabPage);
            MainFormTabControl.Controls.Add(CommandsTabPage);
            MainFormTabControl.Controls.Add(ShortcutKeysTabPage);
            MainFormTabControl.Controls.Add(uiSettingsPage);
            MainFormTabControl.Controls.Add(startupTabPage);
            MainFormTabControl.Location = new System.Drawing.Point(10, 17);
            MainFormTabControl.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            MainFormTabControl.Name = "MainFormTabControl";
            MainFormTabControl.SelectedIndex = 0;
            MainFormTabControl.Size = new System.Drawing.Size(526, 595);
            MainFormTabControl.TabIndex = 12;
            // 
            // CommandsTabPage
            // 
            CommandsTabPage.BackColor = System.Drawing.Color.Black;
            CommandsTabPage.Controls.Add(CommandEntriesSettingsDGV);
            CommandsTabPage.Controls.Add(CommandSettingsHelpButton);
            CommandsTabPage.Location = new System.Drawing.Point(4, 29);
            CommandsTabPage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            CommandsTabPage.Name = "CommandsTabPage";
            CommandsTabPage.Size = new System.Drawing.Size(518, 562);
            CommandsTabPage.TabIndex = 3;
            CommandsTabPage.Text = "Commands";
            // 
            // CommandEntriesSettingsDGV
            // 
            CommandEntriesSettingsDGV.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            CommandEntriesSettingsDGV.BackgroundColor = System.Drawing.Color.Black;
            CommandEntriesSettingsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.142858F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            CommandEntriesSettingsDGV.DefaultCellStyle = dataGridViewCellStyle4;
            CommandEntriesSettingsDGV.GridColor = System.Drawing.Color.Black;
            CommandEntriesSettingsDGV.Location = new System.Drawing.Point(2, 57);
            CommandEntriesSettingsDGV.Margin = new System.Windows.Forms.Padding(2);
            CommandEntriesSettingsDGV.Name = "CommandEntriesSettingsDGV";
            CommandEntriesSettingsDGV.RowHeadersWidth = 62;
            CommandEntriesSettingsDGV.RowTemplate.Height = 31;
            CommandEntriesSettingsDGV.Size = new System.Drawing.Size(487, 488);
            CommandEntriesSettingsDGV.TabIndex = 6;
            CommandEntriesSettingsDGV.CellEndEdit += CommandEntriesSettingsDGV_CellEndEdit;
            // 
            // CommandSettingsHelpButton
            // 
            CommandSettingsHelpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            CommandSettingsHelpButton.Location = new System.Drawing.Point(6, 6);
            CommandSettingsHelpButton.Margin = new System.Windows.Forms.Padding(2);
            CommandSettingsHelpButton.Name = "CommandSettingsHelpButton";
            CommandSettingsHelpButton.Size = new System.Drawing.Size(73, 39);
            CommandSettingsHelpButton.TabIndex = 5;
            CommandSettingsHelpButton.Text = "&Help";
            CommandSettingsHelpButton.UseVisualStyleBackColor = true;
            // 
            // ShortcutKeysTabPage
            // 
            ShortcutKeysTabPage.BackColor = System.Drawing.Color.Black;
            ShortcutKeysTabPage.Controls.Add(AppendToClipboardShortcutControlShiftLabel);
            ShortcutKeysTabPage.Controls.Add(AppendToClipboardShortcutComboBox);
            ShortcutKeysTabPage.Controls.Add(AppendToClipboardLabel);
            ShortcutKeysTabPage.Controls.Add(CommandExecutionShortcutControlShiftLabel);
            ShortcutKeysTabPage.Controls.Add(CommandExecutionShortcutComboBox);
            ShortcutKeysTabPage.Controls.Add(CommandExecutionShortcutLabel);
            ShortcutKeysTabPage.Controls.Add(FormattingToolsShortcutControlShiftLabel);
            ShortcutKeysTabPage.Controls.Add(FormattingToolsShortcutComboBox);
            ShortcutKeysTabPage.Controls.Add(FormattingToolsShortcutLabel);
            ShortcutKeysTabPage.Controls.Add(FileSearchShortcutControlShiftLabel);
            ShortcutKeysTabPage.Controls.Add(HighlightsShortcutComboBox);
            ShortcutKeysTabPage.Controls.Add(HighlightsShortcutLabel);
            ShortcutKeysTabPage.Controls.Add(GeneratedStatementsControlShiftLabel);
            ShortcutKeysTabPage.Controls.Add(GeneratedStatementsShortcutComboBox);
            ShortcutKeysTabPage.Controls.Add(GeneratedStatementsLabel);
            ShortcutKeysTabPage.Controls.Add(QuickPasteShortcutControlShiftLabel);
            ShortcutKeysTabPage.Controls.Add(QuickPasteShortcutComboBox);
            ShortcutKeysTabPage.Controls.Add(QuickPasteShortcutLabel);
            ShortcutKeysTabPage.Controls.Add(clipboardHistoryControlShiftLabel);
            ShortcutKeysTabPage.Controls.Add(fileSearchControlShiftLabel);
            ShortcutKeysTabPage.Controls.Add(FileSearchShortcutComoboBox);
            ShortcutKeysTabPage.Controls.Add(FileSearchShortcutLbl);
            ShortcutKeysTabPage.Controls.Add(clipboardHistoryShortcutComboBox);
            ShortcutKeysTabPage.Controls.Add(ClipboardHistoryShortcutLbl);
            ShortcutKeysTabPage.Location = new System.Drawing.Point(4, 29);
            ShortcutKeysTabPage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            ShortcutKeysTabPage.Name = "ShortcutKeysTabPage";
            ShortcutKeysTabPage.Size = new System.Drawing.Size(518, 562);
            ShortcutKeysTabPage.TabIndex = 4;
            ShortcutKeysTabPage.Text = "Shortcut Keys";
            ShortcutKeysTabPage.Enter += ShortcutKeysTabPage_Enter;
            // 
            // CommandExecutionShortcutControlShiftLabel
            // 
            CommandExecutionShortcutControlShiftLabel.AutoSize = true;
            CommandExecutionShortcutControlShiftLabel.Location = new System.Drawing.Point(192, 366);
            CommandExecutionShortcutControlShiftLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            CommandExecutionShortcutControlShiftLabel.Name = "CommandExecutionShortcutControlShiftLabel";
            CommandExecutionShortcutControlShiftLabel.Size = new System.Drawing.Size(80, 20);
            CommandExecutionShortcutControlShiftLabel.TabIndex = 20;
            CommandExecutionShortcutControlShiftLabel.Text = "Ctrl + Shift";
            // 
            // CommandExecutionShortcutComboBox
            // 
            CommandExecutionShortcutComboBox.BackColor = System.Drawing.Color.Black;
            CommandExecutionShortcutComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            CommandExecutionShortcutComboBox.FormattingEnabled = true;
            CommandExecutionShortcutComboBox.Location = new System.Drawing.Point(274, 363);
            CommandExecutionShortcutComboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            CommandExecutionShortcutComboBox.Name = "CommandExecutionShortcutComboBox";
            CommandExecutionShortcutComboBox.Size = new System.Drawing.Size(86, 28);
            CommandExecutionShortcutComboBox.TabIndex = 19;
            CommandExecutionShortcutComboBox.SelectedIndexChanged += CommandExecutionShortcutComboBox_SelectedIndexChanged;
            // 
            // CommandExecutionShortcutLabel
            // 
            CommandExecutionShortcutLabel.AutoSize = true;
            CommandExecutionShortcutLabel.Location = new System.Drawing.Point(2, 366);
            CommandExecutionShortcutLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            CommandExecutionShortcutLabel.Name = "CommandExecutionShortcutLabel";
            CommandExecutionShortcutLabel.Size = new System.Drawing.Size(146, 20);
            CommandExecutionShortcutLabel.TabIndex = 18;
            CommandExecutionShortcutLabel.Text = "Command Execution";
            // 
            // FormattingToolsShortcutControlShiftLabel
            // 
            FormattingToolsShortcutControlShiftLabel.AutoSize = true;
            FormattingToolsShortcutControlShiftLabel.Location = new System.Drawing.Point(192, 308);
            FormattingToolsShortcutControlShiftLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            FormattingToolsShortcutControlShiftLabel.Name = "FormattingToolsShortcutControlShiftLabel";
            FormattingToolsShortcutControlShiftLabel.Size = new System.Drawing.Size(80, 20);
            FormattingToolsShortcutControlShiftLabel.TabIndex = 17;
            FormattingToolsShortcutControlShiftLabel.Text = "Ctrl + Shift";
            // 
            // FormattingToolsShortcutComboBox
            // 
            FormattingToolsShortcutComboBox.BackColor = System.Drawing.Color.Black;
            FormattingToolsShortcutComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            FormattingToolsShortcutComboBox.FormattingEnabled = true;
            FormattingToolsShortcutComboBox.Location = new System.Drawing.Point(274, 305);
            FormattingToolsShortcutComboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            FormattingToolsShortcutComboBox.Name = "FormattingToolsShortcutComboBox";
            FormattingToolsShortcutComboBox.Size = new System.Drawing.Size(86, 28);
            FormattingToolsShortcutComboBox.TabIndex = 16;
            FormattingToolsShortcutComboBox.SelectedIndexChanged += FormattingToolsShortcutComboBox_SelectedIndexChanged;
            // 
            // FormattingToolsShortcutLabel
            // 
            FormattingToolsShortcutLabel.AutoSize = true;
            FormattingToolsShortcutLabel.Location = new System.Drawing.Point(2, 308);
            FormattingToolsShortcutLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            FormattingToolsShortcutLabel.Name = "FormattingToolsShortcutLabel";
            FormattingToolsShortcutLabel.Size = new System.Drawing.Size(121, 20);
            FormattingToolsShortcutLabel.TabIndex = 15;
            FormattingToolsShortcutLabel.Text = "Formatting Tools";
            // 
            // FileSearchShortcutControlShiftLabel
            // 
            FileSearchShortcutControlShiftLabel.AutoSize = true;
            FileSearchShortcutControlShiftLabel.Location = new System.Drawing.Point(192, 250);
            FileSearchShortcutControlShiftLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            FileSearchShortcutControlShiftLabel.Name = "FileSearchShortcutControlShiftLabel";
            FileSearchShortcutControlShiftLabel.Size = new System.Drawing.Size(80, 20);
            FileSearchShortcutControlShiftLabel.TabIndex = 14;
            FileSearchShortcutControlShiftLabel.Text = "Ctrl + Shift";
            // 
            // HighlightsShortcutComboBox
            // 
            HighlightsShortcutComboBox.BackColor = System.Drawing.Color.Black;
            HighlightsShortcutComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            HighlightsShortcutComboBox.FormattingEnabled = true;
            HighlightsShortcutComboBox.Location = new System.Drawing.Point(274, 246);
            HighlightsShortcutComboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            HighlightsShortcutComboBox.Name = "HighlightsShortcutComboBox";
            HighlightsShortcutComboBox.Size = new System.Drawing.Size(86, 28);
            HighlightsShortcutComboBox.TabIndex = 13;
            HighlightsShortcutComboBox.SelectedIndexChanged += HighlightsShortcutComboBox_SelectedIndexChanged;
            // 
            // HighlightsShortcutLabel
            // 
            HighlightsShortcutLabel.AutoSize = true;
            HighlightsShortcutLabel.Location = new System.Drawing.Point(2, 250);
            HighlightsShortcutLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            HighlightsShortcutLabel.Name = "HighlightsShortcutLabel";
            HighlightsShortcutLabel.Size = new System.Drawing.Size(77, 20);
            HighlightsShortcutLabel.TabIndex = 12;
            HighlightsShortcutLabel.Text = "Highlights";
            // 
            // GeneratedStatementsControlShiftLabel
            // 
            GeneratedStatementsControlShiftLabel.AutoSize = true;
            GeneratedStatementsControlShiftLabel.Location = new System.Drawing.Point(196, 76);
            GeneratedStatementsControlShiftLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            GeneratedStatementsControlShiftLabel.Name = "GeneratedStatementsControlShiftLabel";
            GeneratedStatementsControlShiftLabel.Size = new System.Drawing.Size(80, 20);
            GeneratedStatementsControlShiftLabel.TabIndex = 11;
            GeneratedStatementsControlShiftLabel.Text = "Ctrl + Shift";
            // 
            // GeneratedStatementsShortcutComboBox
            // 
            GeneratedStatementsShortcutComboBox.BackColor = System.Drawing.Color.Black;
            GeneratedStatementsShortcutComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            GeneratedStatementsShortcutComboBox.FormattingEnabled = true;
            GeneratedStatementsShortcutComboBox.Location = new System.Drawing.Point(274, 73);
            GeneratedStatementsShortcutComboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            GeneratedStatementsShortcutComboBox.Name = "GeneratedStatementsShortcutComboBox";
            GeneratedStatementsShortcutComboBox.Size = new System.Drawing.Size(86, 28);
            GeneratedStatementsShortcutComboBox.TabIndex = 10;
            GeneratedStatementsShortcutComboBox.SelectedIndexChanged += GeneratedStatementsShortcutComboBox_SelectedIndexChanged;
            // 
            // GeneratedStatementsLabel
            // 
            GeneratedStatementsLabel.AutoSize = true;
            GeneratedStatementsLabel.Location = new System.Drawing.Point(2, 76);
            GeneratedStatementsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            GeneratedStatementsLabel.Name = "GeneratedStatementsLabel";
            GeneratedStatementsLabel.Size = new System.Drawing.Size(156, 20);
            GeneratedStatementsLabel.TabIndex = 9;
            GeneratedStatementsLabel.Text = "Generated Statements";
            // 
            // QuickPasteShortcutControlShiftLabel
            // 
            QuickPasteShortcutControlShiftLabel.AutoSize = true;
            QuickPasteShortcutControlShiftLabel.Location = new System.Drawing.Point(192, 134);
            QuickPasteShortcutControlShiftLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            QuickPasteShortcutControlShiftLabel.Name = "QuickPasteShortcutControlShiftLabel";
            QuickPasteShortcutControlShiftLabel.Size = new System.Drawing.Size(80, 20);
            QuickPasteShortcutControlShiftLabel.TabIndex = 8;
            QuickPasteShortcutControlShiftLabel.Text = "Ctrl + Shift";
            // 
            // QuickPasteShortcutComboBox
            // 
            QuickPasteShortcutComboBox.BackColor = System.Drawing.Color.Black;
            QuickPasteShortcutComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            QuickPasteShortcutComboBox.FormattingEnabled = true;
            QuickPasteShortcutComboBox.Location = new System.Drawing.Point(274, 131);
            QuickPasteShortcutComboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            QuickPasteShortcutComboBox.Name = "QuickPasteShortcutComboBox";
            QuickPasteShortcutComboBox.Size = new System.Drawing.Size(86, 28);
            QuickPasteShortcutComboBox.TabIndex = 7;
            QuickPasteShortcutComboBox.SelectedIndexChanged += QuickPasteShortcutComboBox_SelectedIndexChanged;
            // 
            // QuickPasteShortcutLabel
            // 
            QuickPasteShortcutLabel.AutoSize = true;
            QuickPasteShortcutLabel.Location = new System.Drawing.Point(2, 134);
            QuickPasteShortcutLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            QuickPasteShortcutLabel.Name = "QuickPasteShortcutLabel";
            QuickPasteShortcutLabel.Size = new System.Drawing.Size(84, 20);
            QuickPasteShortcutLabel.TabIndex = 6;
            QuickPasteShortcutLabel.Text = "Quick Paste";
            // 
            // clipboardHistoryControlShiftLabel
            // 
            clipboardHistoryControlShiftLabel.AutoSize = true;
            clipboardHistoryControlShiftLabel.Location = new System.Drawing.Point(192, 14);
            clipboardHistoryControlShiftLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            clipboardHistoryControlShiftLabel.Name = "clipboardHistoryControlShiftLabel";
            clipboardHistoryControlShiftLabel.Size = new System.Drawing.Size(80, 20);
            clipboardHistoryControlShiftLabel.TabIndex = 5;
            clipboardHistoryControlShiftLabel.Text = "Ctrl + Shift";
            // 
            // fileSearchControlShiftLabel
            // 
            fileSearchControlShiftLabel.AutoSize = true;
            fileSearchControlShiftLabel.Location = new System.Drawing.Point(196, 192);
            fileSearchControlShiftLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            fileSearchControlShiftLabel.Name = "fileSearchControlShiftLabel";
            fileSearchControlShiftLabel.Size = new System.Drawing.Size(80, 20);
            fileSearchControlShiftLabel.TabIndex = 4;
            fileSearchControlShiftLabel.Text = "Ctrl + Shift";
            // 
            // FileSearchShortcutComoboBox
            // 
            FileSearchShortcutComoboBox.BackColor = System.Drawing.Color.Black;
            FileSearchShortcutComoboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            FileSearchShortcutComoboBox.FormattingEnabled = true;
            FileSearchShortcutComoboBox.Location = new System.Drawing.Point(274, 189);
            FileSearchShortcutComoboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            FileSearchShortcutComoboBox.Name = "FileSearchShortcutComoboBox";
            FileSearchShortcutComoboBox.Size = new System.Drawing.Size(86, 28);
            FileSearchShortcutComoboBox.TabIndex = 3;
            FileSearchShortcutComoboBox.SelectedIndexChanged += FileSearchShortcutComoboBox_SelectedIndexChanged;
            // 
            // FileSearchShortcutLbl
            // 
            FileSearchShortcutLbl.AutoSize = true;
            FileSearchShortcutLbl.Location = new System.Drawing.Point(2, 192);
            FileSearchShortcutLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            FileSearchShortcutLbl.Name = "FileSearchShortcutLbl";
            FileSearchShortcutLbl.Size = new System.Drawing.Size(80, 20);
            FileSearchShortcutLbl.TabIndex = 2;
            FileSearchShortcutLbl.Text = "File Search";
            // 
            // clipboardHistoryShortcutComboBox
            // 
            clipboardHistoryShortcutComboBox.BackColor = System.Drawing.Color.Black;
            clipboardHistoryShortcutComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            clipboardHistoryShortcutComboBox.FormattingEnabled = true;
            clipboardHistoryShortcutComboBox.Location = new System.Drawing.Point(274, 11);
            clipboardHistoryShortcutComboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            clipboardHistoryShortcutComboBox.Name = "clipboardHistoryShortcutComboBox";
            clipboardHistoryShortcutComboBox.Size = new System.Drawing.Size(86, 28);
            clipboardHistoryShortcutComboBox.TabIndex = 1;
            clipboardHistoryShortcutComboBox.SelectedIndexChanged += clipboardHistoryShortcutComboBox_SelectedIndexChanged;
            // 
            // ClipboardHistoryShortcutLbl
            // 
            ClipboardHistoryShortcutLbl.AutoSize = true;
            ClipboardHistoryShortcutLbl.Location = new System.Drawing.Point(2, 18);
            ClipboardHistoryShortcutLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            ClipboardHistoryShortcutLbl.Name = "ClipboardHistoryShortcutLbl";
            ClipboardHistoryShortcutLbl.Size = new System.Drawing.Size(126, 20);
            ClipboardHistoryShortcutLbl.TabIndex = 0;
            ClipboardHistoryShortcutLbl.Text = "Clipboard History";
            // 
            // uiSettingsPage
            // 
            uiSettingsPage.BackColor = System.Drawing.Color.Black;
            uiSettingsPage.Location = new System.Drawing.Point(4, 29);
            uiSettingsPage.Margin = new System.Windows.Forms.Padding(2);
            uiSettingsPage.Name = "uiSettingsPage";
            uiSettingsPage.Size = new System.Drawing.Size(518, 562);
            uiSettingsPage.TabIndex = 5;
            uiSettingsPage.Text = "UI Settings";
            // 
            // startupTabPage
            // 
            startupTabPage.BackColor = System.Drawing.Color.Black;
            startupTabPage.Controls.Add(LaunchOnStartupCheckBox);
            startupTabPage.Location = new System.Drawing.Point(4, 29);
            startupTabPage.Name = "startupTabPage";
            startupTabPage.Size = new System.Drawing.Size(518, 562);
            startupTabPage.TabIndex = 6;
            startupTabPage.Text = "Startup";
            startupTabPage.Enter += startupTabPage_Enter;
            // 
            // LaunchOnStartupCheckBox
            // 
            LaunchOnStartupCheckBox.AutoSize = true;
            LaunchOnStartupCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            LaunchOnStartupCheckBox.Location = new System.Drawing.Point(0, 0);
            LaunchOnStartupCheckBox.Name = "LaunchOnStartupCheckBox";
            LaunchOnStartupCheckBox.Size = new System.Drawing.Size(518, 24);
            LaunchOnStartupCheckBox.TabIndex = 0;
            LaunchOnStartupCheckBox.Text = "Launch on Startup?";
            LaunchOnStartupCheckBox.UseVisualStyleBackColor = true;
            LaunchOnStartupCheckBox.CheckedChanged += LaunchOnStartupCheckBox_CheckedChanged;
            // 
            // AppendToClipboardShortcutControlShiftLabel
            // 
            AppendToClipboardShortcutControlShiftLabel.AutoSize = true;
            AppendToClipboardShortcutControlShiftLabel.Location = new System.Drawing.Point(192, 426);
            AppendToClipboardShortcutControlShiftLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            AppendToClipboardShortcutControlShiftLabel.Name = "AppendToClipboardShortcutControlShiftLabel";
            AppendToClipboardShortcutControlShiftLabel.Size = new System.Drawing.Size(80, 20);
            AppendToClipboardShortcutControlShiftLabel.TabIndex = 23;
            AppendToClipboardShortcutControlShiftLabel.Text = "Ctrl + Shift";
            // 
            // AppendToClipboardShortcutComboBox
            // 
            AppendToClipboardShortcutComboBox.BackColor = System.Drawing.Color.Black;
            AppendToClipboardShortcutComboBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            AppendToClipboardShortcutComboBox.FormattingEnabled = true;
            AppendToClipboardShortcutComboBox.Location = new System.Drawing.Point(274, 423);
            AppendToClipboardShortcutComboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            AppendToClipboardShortcutComboBox.Name = "AppendToClipboardShortcutComboBox";
            AppendToClipboardShortcutComboBox.Size = new System.Drawing.Size(86, 28);
            AppendToClipboardShortcutComboBox.TabIndex = 22;
            AppendToClipboardShortcutComboBox.SelectedIndexChanged += AppendToClipboardShortcutComboBox_SelectedIndexChanged;
            // 
            // AppendToClipboardLabel
            // 
            AppendToClipboardLabel.AutoSize = true;
            AppendToClipboardLabel.Location = new System.Drawing.Point(2, 426);
            AppendToClipboardLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            AppendToClipboardLabel.Name = "AppendToClipboardLabel";
            AppendToClipboardLabel.Size = new System.Drawing.Size(152, 20);
            AppendToClipboardLabel.TabIndex = 21;
            AppendToClipboardLabel.Text = "Append To Clipboard";
            // 
            // MainForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.Color.Black;
            ClientSize = new System.Drawing.Size(542, 624);
            Controls.Add(MainFormTabControl);
            ForeColor = System.Drawing.Color.LightSkyBlue;
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            Name = "MainForm";
            Text = "RPC Launcher";
            FormClosing += MainForm_FormClosing;
            Shown += MainForm_Shown;
            Resize += MainForm_Resize;
            TaskbarContextMenuStrip.ResumeLayout(false);
            QuickPasteTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)quickPasteItemsDGV).EndInit();
            ConfigTP.ResumeLayout(false);
            ConfigTP.PerformLayout();
            MainFormTabControl.ResumeLayout(false);
            CommandsTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)CommandEntriesSettingsDGV).EndInit();
            ShortcutKeysTabPage.ResumeLayout(false);
            ShortcutKeysTabPage.PerformLayout();
            startupTabPage.ResumeLayout(false);
            startupTabPage.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.NotifyIcon taskBarNotifyIcon;
        private System.Windows.Forms.ContextMenuStrip TaskbarContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CloseMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EnableShortcutKeysMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DisableShortcutKeysMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EnableClipboardMonitoringMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DisableClipboardMonitoringMenuItem;
        private System.Windows.Forms.TabPage QuickPasteTabPage;
        private System.Windows.Forms.Button QuickPasteItemsHelpBtn;
        private System.Windows.Forms.DataGridView quickPasteItemsDGV;
        private System.Windows.Forms.TabPage ConfigTP;
        private System.Windows.Forms.Button MainFormHelpButton;
        private System.Windows.Forms.Label LibrariesUsedLbl;
        private System.Windows.Forms.CheckedListBox AvailableLibrariesCheckBoxList;
        private System.Windows.Forms.Label LanguageLbl;
        private System.Windows.Forms.RadioButton CSharpRB;
        private System.Windows.Forms.Label SpacingStyleLbl;
        private System.Windows.Forms.RadioButton JavaRB;
        private System.Windows.Forms.ComboBox SpacingStyleCB;
        private System.Windows.Forms.RadioButton JavaScriptRB;
        private System.Windows.Forms.Label BraceStyleLbl;
        private System.Windows.Forms.RadioButton PythonRB;
        private System.Windows.Forms.ComboBox BraceStyleCB;
        private System.Windows.Forms.TabControl MainFormTabControl;
        private System.Windows.Forms.TabPage CommandsTabPage;
        private System.Windows.Forms.DataGridView CommandEntriesSettingsDGV;
        private System.Windows.Forms.Button CommandSettingsHelpButton;
        private System.Windows.Forms.TabPage ShortcutKeysTabPage;
        private System.Windows.Forms.ComboBox FileSearchShortcutComoboBox;
        private System.Windows.Forms.Label FileSearchShortcutLbl;
        private System.Windows.Forms.ComboBox clipboardHistoryShortcutComboBox;
        private System.Windows.Forms.Label ClipboardHistoryShortcutLbl;
        private System.Windows.Forms.Label clipboardHistoryControlShiftLabel;
        private System.Windows.Forms.Label fileSearchControlShiftLabel;
        private System.Windows.Forms.Label CommandExecutionShortcutControlShiftLabel;
        private System.Windows.Forms.ComboBox CommandExecutionShortcutComboBox;
        private System.Windows.Forms.Label CommandExecutionShortcutLabel;
        private System.Windows.Forms.Label FormattingToolsShortcutControlShiftLabel;
        private System.Windows.Forms.ComboBox FormattingToolsShortcutComboBox;
        private System.Windows.Forms.Label FormattingToolsShortcutLabel;
        private System.Windows.Forms.Label FileSearchShortcutControlShiftLabel;
        private System.Windows.Forms.ComboBox HighlightsShortcutComboBox;
        private System.Windows.Forms.Label HighlightsShortcutLabel;
        private System.Windows.Forms.Label GeneratedStatementsControlShiftLabel;
        private System.Windows.Forms.ComboBox GeneratedStatementsShortcutComboBox;
        private System.Windows.Forms.Label GeneratedStatementsLabel;
        private System.Windows.Forms.Label QuickPasteShortcutControlShiftLabel;
        private System.Windows.Forms.ComboBox QuickPasteShortcutComboBox;
        private System.Windows.Forms.Label QuickPasteShortcutLabel;
        private System.Windows.Forms.TabPage uiSettingsPage;
        private System.Windows.Forms.TabPage startupTabPage;
        private System.Windows.Forms.CheckBox LaunchOnStartupCheckBox;
        private System.Windows.Forms.Label AppendToClipboardShortcutControlShiftLabel;
        private System.Windows.Forms.ComboBox AppendToClipboardShortcutComboBox;
        private System.Windows.Forms.Label AppendToClipboardLabel;
    }
}

