﻿namespace RPCLauncher.Forms
{
    partial class StatementToSendSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatementToSendSelectionForm));
            StatementsToDisplayListBox = new System.Windows.Forms.ListBox();
            SubmitBtn = new System.Windows.Forms.Button();
            preserveClipboardTextCheckBox = new System.Windows.Forms.CheckBox();
            SearchLbl = new System.Windows.Forms.Label();
            SearchTxt = new System.Windows.Forms.RichTextBox();
            SuspendLayout();
            // 
            // StatementsToDisplayListBox
            // 
            StatementsToDisplayListBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            StatementsToDisplayListBox.BackColor = System.Drawing.Color.Black;
            StatementsToDisplayListBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            StatementsToDisplayListBox.FormattingEnabled = true;
            StatementsToDisplayListBox.ItemHeight = 25;
            StatementsToDisplayListBox.Location = new System.Drawing.Point(20, 50);
            StatementsToDisplayListBox.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            StatementsToDisplayListBox.Name = "StatementsToDisplayListBox";
            StatementsToDisplayListBox.Size = new System.Drawing.Size(789, 629);
            StatementsToDisplayListBox.TabIndex = 1;
            StatementsToDisplayListBox.SelectedIndexChanged += StatementsToDisplayListBox_SelectedIndexChanged;
            StatementsToDisplayListBox.DoubleClick += StatementsToDisplayListBox_DoubleClick;
            StatementsToDisplayListBox.KeyDown += StatementsToDisplayListBox_KeyDown;
            StatementsToDisplayListBox.KeyPress += StatementsToDisplayListBox_KeyPress;
            StatementsToDisplayListBox.MouseDoubleClick += StatementsToDisplayListBox_MouseDoubleClick;
            // 
            // SubmitBtn
            // 
            SubmitBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            SubmitBtn.BackColor = System.Drawing.Color.Black;
            SubmitBtn.Location = new System.Drawing.Point(20, 694);
            SubmitBtn.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            SubmitBtn.Name = "SubmitBtn";
            SubmitBtn.Size = new System.Drawing.Size(144, 66);
            SubmitBtn.TabIndex = 2;
            SubmitBtn.Text = "&Paste";
            SubmitBtn.UseVisualStyleBackColor = false;
            SubmitBtn.Click += SubmitBtn_Click;
            // 
            // preserveClipboardTextCheckBox
            // 
            preserveClipboardTextCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            preserveClipboardTextCheckBox.AutoSize = true;
            preserveClipboardTextCheckBox.CheckAlign = System.Drawing.ContentAlignment.BottomRight;
            preserveClipboardTextCheckBox.Location = new System.Drawing.Point(172, 731);
            preserveClipboardTextCheckBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            preserveClipboardTextCheckBox.Name = "preserveClipboardTextCheckBox";
            preserveClipboardTextCheckBox.Size = new System.Drawing.Size(230, 29);
            preserveClipboardTextCheckBox.TabIndex = 3;
            preserveClipboardTextCheckBox.Text = "Preserve Clipboard Text?";
            preserveClipboardTextCheckBox.UseVisualStyleBackColor = true;
            // 
            // SearchLbl
            // 
            SearchLbl.AutoSize = true;
            SearchLbl.Location = new System.Drawing.Point(15, 11);
            SearchLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            SearchLbl.Name = "SearchLbl";
            SearchLbl.Size = new System.Drawing.Size(68, 25);
            SearchLbl.TabIndex = 5;
            SearchLbl.Text = "Search:";
            // 
            // SearchTxt
            // 
            SearchTxt.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            SearchTxt.BackColor = System.Drawing.Color.Black;
            SearchTxt.ForeColor = System.Drawing.Color.LightSkyBlue;
            SearchTxt.Location = new System.Drawing.Point(94, 6);
            SearchTxt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            SearchTxt.Multiline = false;
            SearchTxt.Name = "SearchTxt";
            SearchTxt.Size = new System.Drawing.Size(715, 32);
            SearchTxt.TabIndex = 0;
            SearchTxt.Text = "";
            SearchTxt.TextChanged += searchTxt_TextChanged;
            SearchTxt.KeyDown += searchTxt_KeyDown;
            // 
            // StatementToSendSelectionForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.Color.Black;
            ClientSize = new System.Drawing.Size(831, 781);
            Controls.Add(SearchTxt);
            Controls.Add(SearchLbl);
            Controls.Add(preserveClipboardTextCheckBox);
            Controls.Add(SubmitBtn);
            Controls.Add(StatementsToDisplayListBox);
            ForeColor = System.Drawing.Color.LightSkyBlue;
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            Name = "StatementToSendSelectionForm";
            Text = "Clipboard History";
            FormClosed += StatementToSendSelectionForm_FormClosed;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        protected System.Windows.Forms.CheckBox preserveClipboardTextCheckBox;
        protected System.Windows.Forms.ListBox StatementsToDisplayListBox;
        protected System.Windows.Forms.Label SearchLbl;
        protected System.Windows.Forms.Button SubmitBtn;
        protected System.Windows.Forms.RichTextBox SearchTxt;
    }
}