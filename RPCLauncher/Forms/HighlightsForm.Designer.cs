﻿namespace RPCLauncher.Forms
{
    partial class HighlightsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HighlightsForm));
            this.HighlightsTC = new System.Windows.Forms.TabControl();
            this.NewHighlightsTab = new System.Windows.Forms.TabPage();
            this.RecentTagsListBox = new System.Windows.Forms.ListBox();
            this.RecentTagsLbl = new System.Windows.Forms.Label();
            this.DeleteHighlightBtn = new System.Windows.Forms.Button();
            this.TagsCsvRTB = new System.Windows.Forms.RichTextBox();
            this.TagsCSVLbl = new System.Windows.Forms.Label();
            this.SourceRTB = new System.Windows.Forms.RichTextBox();
            this.SourceLbl = new System.Windows.Forms.Label();
            this.HighlightsSaveBtn = new System.Windows.Forms.Button();
            this.NotesTextRTB = new System.Windows.Forms.RichTextBox();
            this.NotesTextLbl = new System.Windows.Forms.Label();
            this.HighlightTextRTB = new System.Windows.Forms.RichTextBox();
            this.HighlightTextLbl = new System.Windows.Forms.Label();
            this.EditHighlightsTab = new System.Windows.Forms.TabPage();
            this.HighlightsDGV = new System.Windows.Forms.DataGridView();
            this.ExportHighlightsTab = new System.Windows.Forms.TabPage();
            this.HighlightExportFormattingHelpLbl = new System.Windows.Forms.Label();
            this.HighlightsExportBtn = new System.Windows.Forms.Button();
            this.HighlightExportFormattingRTB = new System.Windows.Forms.RichTextBox();
            this.HighlightExportFormattingLbl = new System.Windows.Forms.Label();
            this.HighlightsTC.SuspendLayout();
            this.NewHighlightsTab.SuspendLayout();
            this.EditHighlightsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HighlightsDGV)).BeginInit();
            this.ExportHighlightsTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // HighlightsTC
            // 
            this.HighlightsTC.Controls.Add(this.NewHighlightsTab);
            this.HighlightsTC.Controls.Add(this.EditHighlightsTab);
            this.HighlightsTC.Controls.Add(this.ExportHighlightsTab);
            this.HighlightsTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HighlightsTC.Location = new System.Drawing.Point(0, 0);
            this.HighlightsTC.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.HighlightsTC.Name = "HighlightsTC";
            this.HighlightsTC.SelectedIndex = 0;
            this.HighlightsTC.Size = new System.Drawing.Size(629, 894);
            this.HighlightsTC.TabIndex = 0;
            this.HighlightsTC.Selected += new System.Windows.Forms.TabControlEventHandler(this.HighlightsTC_Selected);
            // 
            // NewHighlightsTab
            // 
            this.NewHighlightsTab.BackColor = System.Drawing.Color.Black;
            this.NewHighlightsTab.Controls.Add(this.RecentTagsListBox);
            this.NewHighlightsTab.Controls.Add(this.RecentTagsLbl);
            this.NewHighlightsTab.Controls.Add(this.DeleteHighlightBtn);
            this.NewHighlightsTab.Controls.Add(this.TagsCsvRTB);
            this.NewHighlightsTab.Controls.Add(this.TagsCSVLbl);
            this.NewHighlightsTab.Controls.Add(this.SourceRTB);
            this.NewHighlightsTab.Controls.Add(this.SourceLbl);
            this.NewHighlightsTab.Controls.Add(this.HighlightsSaveBtn);
            this.NewHighlightsTab.Controls.Add(this.NotesTextRTB);
            this.NewHighlightsTab.Controls.Add(this.NotesTextLbl);
            this.NewHighlightsTab.Controls.Add(this.HighlightTextRTB);
            this.NewHighlightsTab.Controls.Add(this.HighlightTextLbl);
            this.NewHighlightsTab.Location = new System.Drawing.Point(4, 29);
            this.NewHighlightsTab.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NewHighlightsTab.Name = "NewHighlightsTab";
            this.NewHighlightsTab.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NewHighlightsTab.Size = new System.Drawing.Size(621, 861);
            this.NewHighlightsTab.TabIndex = 0;
            this.NewHighlightsTab.Text = "New Highlight";
            // 
            // RecentTagsListBox
            // 
            this.RecentTagsListBox.BackColor = System.Drawing.Color.Black;
            this.RecentTagsListBox.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.RecentTagsListBox.FormattingEnabled = true;
            this.RecentTagsListBox.ItemHeight = 20;
            this.RecentTagsListBox.Location = new System.Drawing.Point(10, 695);
            this.RecentTagsListBox.Name = "RecentTagsListBox";
            this.RecentTagsListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.RecentTagsListBox.Size = new System.Drawing.Size(331, 84);
            this.RecentTagsListBox.TabIndex = 27;
            this.RecentTagsListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RecentTagsListBox_KeyDown);
            // 
            // RecentTagsLbl
            // 
            this.RecentTagsLbl.AutoSize = true;
            this.RecentTagsLbl.Location = new System.Drawing.Point(6, 672);
            this.RecentTagsLbl.Name = "RecentTagsLbl";
            this.RecentTagsLbl.Size = new System.Drawing.Size(104, 20);
            this.RecentTagsLbl.TabIndex = 26;
            this.RecentTagsLbl.Text = "Recent Tags:";
            // 
            // DeleteHighlightBtn
            // 
            this.DeleteHighlightBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteHighlightBtn.BackColor = System.Drawing.Color.Gray;
            this.DeleteHighlightBtn.Location = new System.Drawing.Point(474, 787);
            this.DeleteHighlightBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DeleteHighlightBtn.Name = "DeleteHighlightBtn";
            this.DeleteHighlightBtn.Size = new System.Drawing.Size(129, 52);
            this.DeleteHighlightBtn.TabIndex = 24;
            this.DeleteHighlightBtn.Text = "&Delete";
            this.DeleteHighlightBtn.UseVisualStyleBackColor = false;
            this.DeleteHighlightBtn.Click += new System.EventHandler(this.DeleteHighlightBtn_Click);
            // 
            // TagsCsvRTB
            // 
            this.TagsCsvRTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TagsCsvRTB.BackColor = System.Drawing.Color.Black;
            this.TagsCsvRTB.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.TagsCsvRTB.Location = new System.Drawing.Point(4, 553);
            this.TagsCsvRTB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TagsCsvRTB.Name = "TagsCsvRTB";
            this.TagsCsvRTB.Size = new System.Drawing.Size(599, 94);
            this.TagsCsvRTB.TabIndex = 4;
            this.TagsCsvRTB.Text = "";
            // 
            // TagsCSVLbl
            // 
            this.TagsCSVLbl.AutoSize = true;
            this.TagsCSVLbl.Location = new System.Drawing.Point(6, 528);
            this.TagsCSVLbl.Name = "TagsCSVLbl";
            this.TagsCSVLbl.Size = new System.Drawing.Size(85, 20);
            this.TagsCSVLbl.TabIndex = 23;
            this.TagsCSVLbl.Text = "Tags CSV:";
            // 
            // SourceRTB
            // 
            this.SourceRTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SourceRTB.BackColor = System.Drawing.Color.Black;
            this.SourceRTB.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.SourceRTB.Location = new System.Drawing.Point(4, 423);
            this.SourceRTB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SourceRTB.Name = "SourceRTB";
            this.SourceRTB.Size = new System.Drawing.Size(599, 94);
            this.SourceRTB.TabIndex = 3;
            this.SourceRTB.Text = "";
            // 
            // SourceLbl
            // 
            this.SourceLbl.AutoSize = true;
            this.SourceLbl.Location = new System.Drawing.Point(6, 398);
            this.SourceLbl.Name = "SourceLbl";
            this.SourceLbl.Size = new System.Drawing.Size(98, 20);
            this.SourceLbl.TabIndex = 21;
            this.SourceLbl.Text = "Source Text:";
            // 
            // HighlightsSaveBtn
            // 
            this.HighlightsSaveBtn.AutoSize = true;
            this.HighlightsSaveBtn.BackColor = System.Drawing.Color.Gray;
            this.HighlightsSaveBtn.Location = new System.Drawing.Point(9, 787);
            this.HighlightsSaveBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.HighlightsSaveBtn.Name = "HighlightsSaveBtn";
            this.HighlightsSaveBtn.Size = new System.Drawing.Size(129, 52);
            this.HighlightsSaveBtn.TabIndex = 5;
            this.HighlightsSaveBtn.Text = "&Save";
            this.HighlightsSaveBtn.UseVisualStyleBackColor = false;
            this.HighlightsSaveBtn.Click += new System.EventHandler(this.HighlightsSaveBtn_Click);
            // 
            // NotesTextRTB
            // 
            this.NotesTextRTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NotesTextRTB.BackColor = System.Drawing.Color.Black;
            this.NotesTextRTB.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.NotesTextRTB.Location = new System.Drawing.Point(3, 233);
            this.NotesTextRTB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NotesTextRTB.Name = "NotesTextRTB";
            this.NotesTextRTB.Size = new System.Drawing.Size(599, 156);
            this.NotesTextRTB.TabIndex = 2;
            this.NotesTextRTB.Text = "";
            // 
            // NotesTextLbl
            // 
            this.NotesTextLbl.AutoSize = true;
            this.NotesTextLbl.Location = new System.Drawing.Point(5, 208);
            this.NotesTextLbl.Name = "NotesTextLbl";
            this.NotesTextLbl.Size = new System.Drawing.Size(89, 20);
            this.NotesTextLbl.TabIndex = 18;
            this.NotesTextLbl.Text = "Notes Text:";
            // 
            // HighlightTextRTB
            // 
            this.HighlightTextRTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HighlightTextRTB.BackColor = System.Drawing.Color.Black;
            this.HighlightTextRTB.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.HighlightTextRTB.Location = new System.Drawing.Point(3, 33);
            this.HighlightTextRTB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.HighlightTextRTB.Name = "HighlightTextRTB";
            this.HighlightTextRTB.Size = new System.Drawing.Size(599, 161);
            this.HighlightTextRTB.TabIndex = 1;
            this.HighlightTextRTB.Text = "";
            // 
            // HighlightTextLbl
            // 
            this.HighlightTextLbl.AutoSize = true;
            this.HighlightTextLbl.Location = new System.Drawing.Point(6, 4);
            this.HighlightTextLbl.Name = "HighlightTextLbl";
            this.HighlightTextLbl.Size = new System.Drawing.Size(109, 20);
            this.HighlightTextLbl.TabIndex = 16;
            this.HighlightTextLbl.Text = "Highlight Text:";
            // 
            // EditHighlightsTab
            // 
            this.EditHighlightsTab.BackColor = System.Drawing.Color.Black;
            this.EditHighlightsTab.Controls.Add(this.HighlightsDGV);
            this.EditHighlightsTab.Location = new System.Drawing.Point(4, 29);
            this.EditHighlightsTab.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.EditHighlightsTab.Name = "EditHighlightsTab";
            this.EditHighlightsTab.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.EditHighlightsTab.Size = new System.Drawing.Size(621, 861);
            this.EditHighlightsTab.TabIndex = 1;
            this.EditHighlightsTab.Text = "Edit Highlights";
            // 
            // HighlightsDGV
            // 
            this.HighlightsDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HighlightsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.HighlightsDGV.DefaultCellStyle = dataGridViewCellStyle1;
            this.HighlightsDGV.Location = new System.Drawing.Point(7, 8);
            this.HighlightsDGV.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.HighlightsDGV.Name = "HighlightsDGV";
            this.HighlightsDGV.RowTemplate.Height = 24;
            this.HighlightsDGV.Size = new System.Drawing.Size(606, 845);
            this.HighlightsDGV.TabIndex = 0;
            this.HighlightsDGV.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HighlightsDGV_KeyDown);
            // 
            // ExportHighlightsTab
            // 
            this.ExportHighlightsTab.BackColor = System.Drawing.Color.Black;
            this.ExportHighlightsTab.Controls.Add(this.HighlightExportFormattingHelpLbl);
            this.ExportHighlightsTab.Controls.Add(this.HighlightsExportBtn);
            this.ExportHighlightsTab.Controls.Add(this.HighlightExportFormattingRTB);
            this.ExportHighlightsTab.Controls.Add(this.HighlightExportFormattingLbl);
            this.ExportHighlightsTab.Location = new System.Drawing.Point(4, 29);
            this.ExportHighlightsTab.Name = "ExportHighlightsTab";
            this.ExportHighlightsTab.Size = new System.Drawing.Size(621, 861);
            this.ExportHighlightsTab.TabIndex = 2;
            this.ExportHighlightsTab.Text = "Export Highlights";
            // 
            // HighlightExportFormattingHelpLbl
            // 
            this.HighlightExportFormattingHelpLbl.AutoSize = true;
            this.HighlightExportFormattingHelpLbl.Location = new System.Drawing.Point(13, 381);
            this.HighlightExportFormattingHelpLbl.Name = "HighlightExportFormattingHelpLbl";
            this.HighlightExportFormattingHelpLbl.Size = new System.Drawing.Size(349, 200);
            this.HighlightExportFormattingHelpLbl.TabIndex = 20;
            this.HighlightExportFormattingHelpLbl.Text = resources.GetString("HighlightExportFormattingHelpLbl.Text");
            // 
            // HighlightsExportBtn
            // 
            this.HighlightsExportBtn.BackColor = System.Drawing.Color.Gray;
            this.HighlightsExportBtn.Location = new System.Drawing.Point(17, 759);
            this.HighlightsExportBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.HighlightsExportBtn.Name = "HighlightsExportBtn";
            this.HighlightsExportBtn.Size = new System.Drawing.Size(129, 52);
            this.HighlightsExportBtn.TabIndex = 18;
            this.HighlightsExportBtn.Text = "&Export";
            this.HighlightsExportBtn.UseVisualStyleBackColor = false;
            this.HighlightsExportBtn.Click += new System.EventHandler(this.HighlightsExportBtn_Click);
            // 
            // HighlightExportFormattingRTB
            // 
            this.HighlightExportFormattingRTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HighlightExportFormattingRTB.BackColor = System.Drawing.Color.Black;
            this.HighlightExportFormattingRTB.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.HighlightExportFormattingRTB.Location = new System.Drawing.Point(11, 38);
            this.HighlightExportFormattingRTB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.HighlightExportFormattingRTB.Name = "HighlightExportFormattingRTB";
            this.HighlightExportFormattingRTB.Size = new System.Drawing.Size(583, 297);
            this.HighlightExportFormattingRTB.TabIndex = 17;
            this.HighlightExportFormattingRTB.Text = "";
            // 
            // HighlightExportFormattingLbl
            // 
            this.HighlightExportFormattingLbl.AutoSize = true;
            this.HighlightExportFormattingLbl.Location = new System.Drawing.Point(13, 13);
            this.HighlightExportFormattingLbl.Name = "HighlightExportFormattingLbl";
            this.HighlightExportFormattingLbl.Size = new System.Drawing.Size(206, 20);
            this.HighlightExportFormattingLbl.TabIndex = 19;
            this.HighlightExportFormattingLbl.Text = "Highlight Export Formatting:";
            // 
            // HighlightsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(629, 894);
            this.Controls.Add(this.HighlightsTC);
            this.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(651, 845);
            this.Name = "HighlightsForm";
            this.Text = "HighlightsForm";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.HighlightsForm_Load);
            this.HighlightsTC.ResumeLayout(false);
            this.NewHighlightsTab.ResumeLayout(false);
            this.NewHighlightsTab.PerformLayout();
            this.EditHighlightsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HighlightsDGV)).EndInit();
            this.ExportHighlightsTab.ResumeLayout(false);
            this.ExportHighlightsTab.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl HighlightsTC;
        private System.Windows.Forms.TabPage NewHighlightsTab;
        private System.Windows.Forms.TabPage EditHighlightsTab;
        private System.Windows.Forms.RichTextBox TagsCsvRTB;
        private System.Windows.Forms.Label TagsCSVLbl;
        private System.Windows.Forms.RichTextBox SourceRTB;
        private System.Windows.Forms.Label SourceLbl;
        private System.Windows.Forms.Button HighlightsSaveBtn;
        private System.Windows.Forms.RichTextBox NotesTextRTB;
        private System.Windows.Forms.Label NotesTextLbl;
        private System.Windows.Forms.RichTextBox HighlightTextRTB;
        private System.Windows.Forms.Label HighlightTextLbl;
        private System.Windows.Forms.DataGridView HighlightsDGV;
        private System.Windows.Forms.TabPage ExportHighlightsTab;
        private System.Windows.Forms.Button HighlightsExportBtn;
        private System.Windows.Forms.RichTextBox HighlightExportFormattingRTB;
        private System.Windows.Forms.Label HighlightExportFormattingLbl;
        private System.Windows.Forms.Label HighlightExportFormattingHelpLbl;
        private System.Windows.Forms.Button DeleteHighlightBtn;
        private System.Windows.Forms.Label RecentTagsLbl;
        private System.Windows.Forms.ListBox RecentTagsListBox;
    }
}