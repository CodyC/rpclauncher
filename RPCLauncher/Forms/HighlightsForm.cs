﻿using RPCLauncher.Domain;
using RPCLauncher.Services;
using RPCLauncher.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace RPCLauncher.Forms
{
    public partial class HighlightsForm : Form
    {
        readonly HighlightService highlightService = new HighlightService();
        private String idToEdit = "";

        public HighlightsForm()
        {
            InitializeComponent();

            //Pops the form up in the center of the screen containing the mouse cursor
            //See this link for more details: 
            //https://msdn.microsoft.com/en-us/library/system.windows.forms.form.centertoscreen(v=vs.110).aspx
            this.StartPosition = FormStartPosition.CenterScreen;

            this.AutoScaleMode = AutoScaleMode.Font;
        }

        private void HighlightsSaveBtn_Click(object sender, EventArgs e)
        {
            Highlight highlightToSave = new Highlight();

            highlightToSave.HighlightText = HighlightTextRTB.Text;
            highlightToSave.Notes = NotesTextRTB.Text;
            highlightToSave.Source = SourceRTB.Text;
            highlightToSave.Tags = new List<string>();

            String tagsCSV = TagsCsvRTB.Text;
            if (!String.IsNullOrWhiteSpace(tagsCSV))
            {
                String[] tagsSplit = tagsCSV.Split(',');

                foreach (var currentTag in tagsSplit)
                {
                    highlightToSave.Tags.Add(currentTag.Trim());
                }
            }

            if (!String.IsNullOrWhiteSpace(idToEdit))
            {
                highlightToSave.Id = idToEdit;

                List<Highlight> savedHighlights = highlightService.GetHighlights();
                int indexToReplace =
                    savedHighlights
                        .FindIndex(x => x.Id == idToEdit);

                //Replace the index with the matching id
                savedHighlights[indexToReplace] = highlightToSave;

                //re-save the list
                highlightService.SaveHighlightsToFile(savedHighlights);

                //switch back to the edit tab if we are editing.
                switchToEditHighlightsTab();
            }
            else
            {
                highlightToSave.Id = Guid.NewGuid().ToString();
                highlightToSave.UnixTimestamp = TimeUtils.getCurrentUnixTimestamp();
                highlightService.AppendHighlightToSaveFile(highlightToSave);

                //close the form after saving, if we are only adding a new one.
                this.Close();
            }
        }

        private void switchToEditHighlightsTab()
        {
            HighlightsTC.SelectedTab = EditHighlightsTab;
        }

        private void presentPopulatedHighlightEditForm(object sender)
        {
            //get the element of that list that is selected
            List<DisplayableHighlight> displayableHighlights =
                getDisplayableHighlightsFromDataGridView();

            DataGridView dataGridView = sender as DataGridView;

            if (dataGridView != null)
            {
                var selectedRow = dataGridView.CurrentRow;

                String selectedId = selectedRow.Cells["Id"].Value as String;

                DisplayableHighlight selectedHighlight =
                    getHighlightFromDataGridview(displayableHighlights, selectedId);

                HighlightsTC.SelectedTab = NewHighlightsTab;
                fillHighlightFields(selectedHighlight);
                idToEdit = selectedId;

                DeleteHighlightBtn.Enabled = true;
            }
        }

        private static DisplayableHighlight getHighlightFromDataGridview(List<DisplayableHighlight> displayableHighlights,
                                                                         string selectedId)
        {
            return
                displayableHighlights
                    .Find(x => x.Id == selectedId);
        }

        private List<DisplayableHighlight> getDisplayableHighlightsFromDataGridView()
        {
            return HighlightsDGV.DataSource as List<DisplayableHighlight>;
        }

        private List<Highlight> getHighlightsFromDataGridView()
        {
            List<Highlight> highlights = new List<Highlight>();
            var displayableHighlights = getDisplayableHighlightsFromDataGridView();

            foreach (DisplayableHighlight currentDisplayableHighlight in displayableHighlights)
            {
                highlights.Add(currentDisplayableHighlight);
            }

            return highlights;
        }

        private void fillHighlightFields(DisplayableHighlight displayableHighlight)
        {
            //match the field contents to the UI element
            HighlightTextRTB.Text = displayableHighlight.HighlightText;
            NotesTextRTB.Text = displayableHighlight.Notes;
            SourceRTB.Text = displayableHighlight.Source;
            TagsCsvRTB.Text = displayableHighlight.DisplayableTags;
        }

        private void clearHighlightFields()
        {
            //match the field contents to the UI element
            HighlightTextRTB.Clear();
            NotesTextRTB.Clear();
            SourceRTB.Clear();
            TagsCsvRTB.Clear();
            idToEdit = "";
        }

        private void HighlightsTC_Selected(object sender, TabControlEventArgs e)
        {
            if (HighlightsTC.SelectedTab == EditHighlightsTab)
            {
                handleEditHighlightsTabSelection();
            }
            else if (HighlightsTC.SelectedTab == ExportHighlightsTab)
            {
                HighlightExportFormattingRTB.Text =
                    highlightService.GetExportFormatText();
            }
        }

        private void handleEditHighlightsTabSelection()
        {
            //load the data grid view up
            List<DisplayableHighlight> gridDisplayableHighlights =
                highlightService.GetDisplayableHighlights();

            HighlightsDGV.DataSource = gridDisplayableHighlights;

            HighlightsDGV.Columns["Id"].Visible = false;
            HighlightsDGV.Columns["UnixTimestamp"].Visible = false;

            HighlightsDGV.Columns["HighlightText"].DisplayIndex = 1;
            HighlightsDGV.Columns["Notes"].DisplayIndex = 2;
            HighlightsDGV.Columns["Source"].DisplayIndex = 3;
            HighlightsDGV.Columns["DisplayableTags"].DisplayIndex = 4;

            HighlightsDGV.Columns["DisplayableTags"].HeaderText = "Tags";

            HighlightsDGV.Focus();
        }

        private void HighlightsForm_Load(object sender, EventArgs e)
        {
            //Set the highlight text to the clipboard 
            //automatically when the user pulls up the form.
            HighlightTextRTB.Text = ClipboardUtils.GetTrimmedClipboardText();

            //Don't show the delete button, we don't have a record loaded.
            DeleteHighlightBtn.Enabled = false;

            loadRecentTags();
        }

        private void loadRecentTags()
        {
            //Load the recently used tags
            List<Highlight> highlights = highlightService.GetHighlights();
            if (highlights != null
                    && highlights.Count > 0)
            {
                int mostRecentHighlightsIndex =
                    highlights
                        .FindLastIndex(x => x.Tags != null
                                            && x.Tags.Count > 0);

                List<String> mostRecentHighlightTags =
                    highlights[mostRecentHighlightsIndex].Tags;

                RecentTagsListBox.DataSource = mostRecentHighlightTags;
            }
        }

        private void HighlightsDGV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2 || e.KeyCode == Keys.Enter)
            {
                presentPopulatedHighlightEditForm(sender);
            }
        }

        private void HighlightsExportBtn_Click(object sender, EventArgs e)
        {
            String formatText = HighlightExportFormattingRTB.Text;
            highlightService.SaveHighlightExportFormat(formatText);

            var displayableHighlights = highlightService.GetDisplayableHighlights();

            String exportedHighlights =
                highlightService.ExportHighlights(displayableHighlights, formatText);

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.ShowDialog();

            File.WriteAllText(saveFileDialog.FileName, exportedHighlights);

            //Close the form after we are done exporting
            this.Close();
        }

        private void DeleteHighlightBtn_Click(object sender, EventArgs e)
        {
            highlightService.DeleteHighlight(idToEdit);

            idToEdit = "";
            switchToEditHighlightsTab();
        }

        private void RecentTagsListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (KeyHandlerUtils.EnterWasPressed(e))
            {
                insertSelectedRecentTags();
            }
        }

        private void insertSelectedRecentTags()
        {
            List<string> tagsToInsert = new List<string>();

            string tagsToInsertCSV = "";

            if (RecentTagsListBox.SelectedItems != null
                    && RecentTagsListBox.SelectedItems.Count > 0)
            {
                foreach (var currentSelectedItem in RecentTagsListBox.SelectedItems)
                {
                    String currentItemString = currentSelectedItem as String;

                    if (!String.IsNullOrWhiteSpace(currentItemString))
                    {
                        tagsToInsert.Add(currentItemString.Trim());
                    }
                }

                tagsToInsertCSV = String.Join(",", tagsToInsert);
            }

            if (!String.IsNullOrWhiteSpace(TagsCsvRTB.Text))
            {
                TagsCsvRTB.AppendText(", ");
            }

            TagsCsvRTB.AppendText(tagsToInsertCSV);

        }
    }
}
