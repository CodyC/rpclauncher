﻿using RPCLauncher.Domain;
using RPCLauncher.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPCLauncher.Services
{
    public class TemplateGenerator
    {
        const String SPACES_TO_REPLACE_STANDARD_TAB = "    "; // 4 Spaces

        LanguageStylePreferencesService languagePreferencesService = new LanguageStylePreferencesService();

        public PossibleGeneratedTemplates GetPossibleGeneratedTemplates(String clipboardText, String currentLanguage)
        {
            string trimmedClipboardText = clipboardText.Trim();
            var languageStylePreferences = ConfigUtils.GetLanguageStylePreferences();

            var currentLanguageStylePreferences = languagePreferencesService.GetLanguageStylePreferenceByLanguageName(currentLanguage, languageStylePreferences);

            PossibleGeneratedTemplates possibleGeneratedTemplates = new PossibleGeneratedTemplates();

            possibleGeneratedTemplates.SwitchStatement = GenerateSwitchTemplate(trimmedClipboardText, currentLanguage, currentLanguageStylePreferences);

            List<String> statementTypes = new List<string>();
            statementTypes.Add(StatementTypes.IF_STATEMENT_TYPE);

            possibleGeneratedTemplates.IfStatement = GenerateIfTemplate(trimmedClipboardText, currentLanguage, statementTypes, currentLanguageStylePreferences);

            statementTypes = new List<string>();
            statementTypes.Add(StatementTypes.IF_STATEMENT_TYPE);
            statementTypes.Add(StatementTypes.ELSE_STATEMENT_TYPE);
            possibleGeneratedTemplates.IfElseStatement = GenerateIfTemplate(trimmedClipboardText, currentLanguage, statementTypes, currentLanguageStylePreferences);

            statementTypes = new List<string>();
            statementTypes.Add(StatementTypes.IF_STATEMENT_TYPE);
            statementTypes.Add(StatementTypes.ELSE_IF_STATEMENT_TYPE);
            possibleGeneratedTemplates.IfElseIfStatement = GenerateIfTemplate(trimmedClipboardText, currentLanguage, statementTypes, currentLanguageStylePreferences);

            possibleGeneratedTemplates.StringNotNullCheck = GenerateStringNotNullTemplate(clipboardText, currentLanguage, currentLanguageStylePreferences);
            possibleGeneratedTemplates.NotNullCheck = GenerateNotNullCheckTemplate(clipboardText, currentLanguage, currentLanguageStylePreferences);

            return possibleGeneratedTemplates;
        }

        /// <summary>
        /// Build a skeleton switch statement template for the given language.
        /// 
        /// </summary>
        /// <param name="switchValue"></param>
        /// <param name="languageName"></param>
        /// <returns></returns>
        public String GenerateSwitchTemplate(String switchValue,
                                             String languageName,
                                             LanguageStylePreference languageStylePreference)
        {
            String generatedSwitchStatement = "";

            switch (languageName)
            {
                //Purposeful fallthrough.  
                //This will likely change some but a switch gives us more flexibility.
                //Note, we are purposefully skipping python because it doesn't have switch statements.
                // 09/10/2024 Update: I have been working on this so long, that Python finally has switch statements.
                // Well, case matching statements.
                case Languages.C_SHARP:
                case Languages.JAVA:
                case Languages.JAVASCRIPT:
                default:
                    string switchValueToPrint = "";
                    StringBuilder switchValueToPrintStringBuilder = new StringBuilder();

                    if (!String.IsNullOrWhiteSpace(switchValue))
                    {
                        switchValueToPrint = switchValue;
                    }

                    string standardCaseBlock = buildStandardCaseBlock(languageStylePreference, languageName);

                    if (languageName != Languages.PYTHON)
                    {
                        switchValueToPrintStringBuilder.Append("switch(");
                        switchValueToPrintStringBuilder.Append(switchValueToPrint);
                        switchValueToPrintStringBuilder.Append(")");
                    }
                    else
                    {
                        switchValueToPrintStringBuilder.Append("case ");
                        switchValueToPrintStringBuilder.Append(switchValueToPrint);
                        switchValueToPrintStringBuilder.Append(":");
                    }

                    bool braceOnNextLine = isBraceOnNextLine(languageStylePreference);

                    bool useTabsForIdentation = useTabsForIndentation(languageStylePreference);

                    if (braceOnNextLine)
                    {
                        switchValueToPrintStringBuilder.Append(Environment.NewLine);
                    }
                    else
                    {
                        switchValueToPrintStringBuilder.Append(" ");
                    }

                    if (languageName != Languages.PYTHON)
                    {
                        switchValueToPrintStringBuilder.Append("{");
                    }

                    switchValueToPrintStringBuilder.Append(Environment.NewLine);
                    switchValueToPrintStringBuilder.Append(standardCaseBlock);
                    switchValueToPrintStringBuilder.Append(standardCaseBlock);

                    string defaultCaseBlock = buildStandardDefaultCaseBlock(languageStylePreference, languageName);

                    switchValueToPrintStringBuilder.Append(defaultCaseBlock);

                    if (languageName != Languages.PYTHON)
                    {
                        switchValueToPrintStringBuilder.Append("}");
                    }

                    generatedSwitchStatement = switchValueToPrintStringBuilder.ToString();
                    break;
            }

            return generatedSwitchStatement;
        }

        private static bool isBraceOnNextLine(LanguageStylePreference languageStylePreference)
        {
            return String.IsNullOrWhiteSpace(languageStylePreference.BraceStyleKey)
                || languageStylePreference.BraceStyleKey.Equals(BraceStyles.NEXT_LINE);
        }

        private static string buildStandardDefaultCaseBlock(LanguageStylePreference languageStylePreference, String selectedLanguage)
        {
            string standardCaseBlock = buildStandardCaseBlock(languageStylePreference, selectedLanguage);
            string standardDefaultCaseBlock = standardCaseBlock.Replace("case", "default");

            return standardDefaultCaseBlock;
        }

        private static string buildStandardCaseBlock(LanguageStylePreference languageStylePreference, String selectedLanguage)
        {
            StringBuilder defaultCaseBlockStringBuilder = new StringBuilder();

            bool useTabsForIdentation = useTabsForIndentation(languageStylePreference);

            if (useTabsForIdentation)
            {
                defaultCaseBlockStringBuilder.Append("\t");
            }
            else
            {
                defaultCaseBlockStringBuilder.Append(SPACES_TO_REPLACE_STANDARD_TAB);
            }

            defaultCaseBlockStringBuilder.Append("case: ");
            defaultCaseBlockStringBuilder.Append(Environment.NewLine);

            if (useTabsForIdentation)
            {
                defaultCaseBlockStringBuilder.Append("\t\t");
            }
            else
            {
                defaultCaseBlockStringBuilder.Append(SPACES_TO_REPLACE_STANDARD_TAB);
                defaultCaseBlockStringBuilder.Append(SPACES_TO_REPLACE_STANDARD_TAB);
            }

            defaultCaseBlockStringBuilder.Append("break");

            if (selectedLanguage != Languages.PYTHON)
            {
                defaultCaseBlockStringBuilder.Append(";");
            }

            defaultCaseBlockStringBuilder.Append(Environment.NewLine);

            return defaultCaseBlockStringBuilder.ToString();
        }

        private static bool useTabsForIndentation(LanguageStylePreference languageStylePreference)
        {
            return
                String.IsNullOrWhiteSpace(languageStylePreference.BraceStyleKey)
                    || languageStylePreference.BraceStyleKey.Equals(BraceStyles.NEXT_LINE);
        }

        public String GenerateNotNullCheckTemplate(String valueToCheck,
                                                     String languageName,
                                                     LanguageStylePreference languageStylePreference)
        {
            string statementToCheckIfAgainst = "";
            switch (languageName)
            {
                //Purposeful fallthrough
                case Languages.JAVA:
                case Languages.C_SHARP:
                case Languages.JAVASCRIPT:
                    statementToCheckIfAgainst = valueToCheck + " != null";
                    break;

                case Languages.PYTHON:
                    statementToCheckIfAgainst = valueToCheck + " is not None";
                    break;
            }

            return GenerateIfTemplate(statementToCheckIfAgainst,
                                      languageName,
                                      new List<string>() { StatementTypes.IF_STATEMENT_TYPE },
                                      languageStylePreference);
        }

        public String GenerateStringNotNullTemplate(String valueToCheck,
                                                     String languageName,
                                                     LanguageStylePreference languageStylePreference)
        {
            string statementToCheckIfAgainst = "";
            switch (languageName)
            {
                case Languages.JAVA:

                    bool isApacheStringUtilsAvailable =
                        languagePreferencesService.IsLibraryUsed(languageStylePreference, JavaLibraryChoices.APACHE_STRING_UTILS);

                    if (isApacheStringUtilsAvailable)
                    {
                        statementToCheckIfAgainst = "!StringUtils.isEmpty(" + valueToCheck + ")";
                    }
                    else
                    {
                        statementToCheckIfAgainst = valueToCheck + " != null";
                    }

                    break;

                case Languages.C_SHARP:
                    statementToCheckIfAgainst = "!String.IsNullOrWhiteSpace(" + valueToCheck + ")";
                    break;

                //Purposeful fallthrough, we are using one of the only good features of JavaScript
                case Languages.JAVASCRIPT:
                case Languages.PYTHON:
                    statementToCheckIfAgainst = valueToCheck;
                    break;
            }

            return GenerateIfTemplate(statementToCheckIfAgainst,
                                      languageName,
                                      new List<string>() { StatementTypes.IF_STATEMENT_TYPE },
                                      languageStylePreference);
        }

        /// <summary>
        /// Build a skeleton switch statement for the given language.
        /// 
        /// </summary>
        /// <param name="switchValue"></param>
        /// <param name="languageName"></param>
        /// <returns></returns>
        public String GenerateIfTemplate(String ifValue,
                                         String languageName,
                                         List<String> statementTypes,
                                         LanguageStylePreference languageStylePreference)
        {
            string generatedIfStatement;
            switch (languageName)
            {
                //Purposeful fallthrough.  
                //This will likely change some but a switch gives us more flexibility.
                case Languages.C_SHARP:
                case Languages.JAVA:
                case Languages.JAVASCRIPT:
                default:

                    StringBuilder ifStringBuilder = new StringBuilder();

                    foreach (String currentStatementType in statementTypes)
                    {
                        bool isElseStatement =
                            currentStatementType.Equals(StatementTypes.ELSE_STATEMENT_TYPE, StringComparison.OrdinalIgnoreCase);

                        ifStringBuilder.Append(currentStatementType);

                        if (!isElseStatement)
                        {
                            ifStringBuilder.Append("(");

                            if (!String.IsNullOrWhiteSpace(ifValue))
                            {
                                ifStringBuilder.Append(ifValue);
                            }

                            ifStringBuilder.Append(")");
                        }

                        bool braceOnNextLine = isBraceOnNextLine(languageStylePreference);

                        if (braceOnNextLine)
                        {
                            ifStringBuilder.Append(Environment.NewLine);
                        }

                        ifStringBuilder.Append("{");
                        ifStringBuilder.Append(Environment.NewLine);
                        ifStringBuilder.Append(Environment.NewLine);
                        ifStringBuilder.Append("}");

                        if (braceOnNextLine)
                        {
                            ifStringBuilder.Append(Environment.NewLine);
                        }
                    }

                    generatedIfStatement = ifStringBuilder.ToString();

                    break;

                case Languages.PYTHON:

                    StringBuilder pythonIfStringBuilder = new StringBuilder();

                    foreach (String currentStatementType in statementTypes)
                    {
                        bool isElseStatement =
                            currentStatementType.Equals(StatementTypes.ELSE_STATEMENT_TYPE, StringComparison.OrdinalIgnoreCase);

                        pythonIfStringBuilder.Append(currentStatementType);
                        pythonIfStringBuilder.Append(" ");

                        if (!isElseStatement)
                        {
                            if (!String.IsNullOrWhiteSpace(ifValue))
                            {
                                pythonIfStringBuilder.Append(ifValue);
                            }

                        }

                        pythonIfStringBuilder.Append(" :");
                        pythonIfStringBuilder.Append(Environment.NewLine);
                        pythonIfStringBuilder.Append(Environment.NewLine);

                    }

                    generatedIfStatement = pythonIfStringBuilder.ToString();

                    break;
            }

            return generatedIfStatement;
        }
    }

}
