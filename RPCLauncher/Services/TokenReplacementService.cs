﻿using RPCLauncher.Utils;
using System;
using System.Collections.Generic;


namespace RPCLauncher.Services
{
    public class TokenReplacementService
    {
        public const String CLIPBOARD_TOKEN_KEY = "{clipboard}";
        public const String UNIX_TIMESTAMP_TOKEN_KEY = "{unixTimestamp}";
        public const String GUID_TOKEN_KEY = "{guid}";

        public Dictionary<String, String> GetStandardTokenReplacementDictionary()
        {
            return
                   new Dictionary<string, string>
                   {
                    {CLIPBOARD_TOKEN_KEY, ClipboardUtils.GetTrimmedClipboardText()},
                    {UNIX_TIMESTAMP_TOKEN_KEY, TimeUtils.getCurrentUnixTimestampAsString()},
                    {GUID_TOKEN_KEY, Guid.NewGuid().ToString()}
                   };
        }

        /// <summary>
        /// Replace all tokens specifed by the key of the dictionary with the corresponding value
        /// 
        /// </summary>
        /// <param name="stringToProcess"></param>
        /// <returns>String with token values replaced</returns>
        public String GetTokenFilledString(String stringToProcess)
        {
            return GetTokenFilledString(stringToProcess, GetStandardTokenReplacementDictionary());
        }

        /// <summary>
        /// Replace all tokens specifed by the key of the dictionary with the corresponding value
        /// 
        /// </summary>
        /// <param name="stringToProcess"></param>
        /// <param name="tokenKeyValuePairs"></param>
        /// <returns>String with token values replaced</returns>
        public String GetTokenFilledString(String stringToProcess, Dictionary<String, String> tokenKeyValuePairs)
        {
            string tokenFilledText = stringToProcess;

            if (stringToProcess != null)
            {
                if (tokenKeyValuePairs != null)
                {
                    //replce each token with the given value 
                    foreach (KeyValuePair<string, string> currentEntry in tokenKeyValuePairs)
                    {
                        tokenFilledText = tokenFilledText.Replace(currentEntry.Key, currentEntry.Value);
                    }
                }
            }

            return tokenFilledText;
        }


        /// <summary>
        /// Replace all tokens specifed by the key of the dictionary with the corresponding value
        /// 
        /// </summary>
        /// <param name="stringsToProcess"></param>
        /// <returns>String with token values replaced</returns>
        public List<String> GetTokenFilledStrings(List<String> stringsToProcess)
        {
            return GetTokenFilledStrings(stringsToProcess, GetStandardTokenReplacementDictionary());
        }

        /// <summary>
        /// Replace all tokens specifed by the key of the dictionary with the corresponding value
        /// 
        /// </summary>
        /// <param name="stringsToProcess"></param>
        /// <param name="tokenKeyValuePairs"></param>
        /// <returns></returns>
        public List<String> GetTokenFilledStrings(List<String> stringsToProcess,
                                                  Dictionary<String, String> tokenKeyValuePairs)
        {
            var tokenFilledStrings = new List<String>();
            foreach (String currentStringToProcess in stringsToProcess)
            {
                string currentTokenFilledString = GetTokenFilledString(currentStringToProcess, tokenKeyValuePairs);

                tokenFilledStrings.Add(currentTokenFilledString);
            }

            return tokenFilledStrings;
        }

    }
}
