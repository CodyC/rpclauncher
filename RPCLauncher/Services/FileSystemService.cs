﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RPCLauncher.Services
{
    public class FileSystemService
    {
        static Timer timer { get; set; }
        public static void IndexFileSystemOnSchedule()
        {
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromHours(4);

            timer =
                new Timer(async (e) =>
                {
                    await IndexFileSystem();
                    timer.Dispose();
                },
                null,
                startTimeSpan,
                periodTimeSpan);
        }

        static readonly DirectoryInfo systemRoot = FileSystemService.GetSystemRootDrive();

        private static string GetUserHomeDirectory()
        {
            return Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.Personal)).FullName;
        }

        //static readonly HashSet<String> rootFoldersToIgnore =
        //    new HashSet<String>()
        //    {
        //        Path.Combine(systemRoot.FullName, "Windows"),
        //        Path.Combine(systemRoot.FullName, "$WINDOWS.~BT"),
        //        Path.Combine(systemRoot.FullName, "Windows.old"),
        //        Path.Combine(systemRoot.FullName, "$Recycle.Bin"),
        //        Path.Combine(systemRoot.FullName, "System Volume Information"),
        //        Path.Combine(systemRoot.FullName, "Config.Msi"),
        //        Path.Combine(systemRoot.FullName, "Program Files"),
        //        Path.Combine(systemRoot.FullName, "Program Files (x86)"),
        //        Path.Combine(systemRoot.FullName, "ProgramData"),
        //        Path.Combine(systemRoot.FullName, "OneDriveTemp"),
        //    };

        /// <summary>
        /// Writes a list of all accessible files and directories on the system root hard drive to the path specified by
        /// FileSystemService.GetDriveIndexFilePath()
        /// </summary>
        /// <summary>
        /// Writes a list of all accessible files and directories on the system root hard drive to the path specified by
        /// FileSystemService.GetDriveIndexFilePath()
        /// </summary>
        public static async Task IndexFileSystem()
        {
            await Task.Run(() =>
            {
                var subDirectories = FileSystemService.GetSubdirectories(systemRoot.FullName);

                var subDirectoriesToSearch = FilterSystemDirectories(subDirectories.ToList());
                //.AsParallel()
                //.Where(x => !rootFoldersToIgnore.Contains(x))
                //.ToList<String>();

                ConcurrentQueue<String> fileIndexQueue = new ConcurrentQueue<String>();

                TraverseTreeParallelForEach(subDirectoriesToSearch,
                                            (f) =>
                                            {
                                                try
                                                {
                                                    fileIndexQueue.Enqueue(f);
                                                }
                                                catch (Exception)
                                                {
                                                    //Console.WriteLine("Error Exception: " + exc.Message);
                                                    //Console.WriteLine("Error processing filename: " + f);
                                                    //Console.WriteLine("Skipping for now");
                                                }
                                            });

                String fileIndexText = String.Join(Environment.NewLine, fileIndexQueue);

                try
                {
                    File.WriteAllText(GetDriveIndexFilePath(), fileIndexText);
                }
                catch (Exception)
                {
                }

            });
        }

        public static String GetDriveIndexFilePath()
        {
            String configDirectory = Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ApplicationBase);
            return Path.Combine(configDirectory, "driveIndex.dat");
        }

        public static DirectoryInfo GetSystemRootDrive()
        {
            return Directory.GetParent(Environment.GetEnvironmentVariable("SystemRoot"));
        }

        public static void TraverseTreeParallelForEach(List<string> rootDirsToCheck, Action<string> action)
        {
            //Count of files traversed and timer for diagnostic output
            int fileCount = 0;

            //for diagnostics
            //var sw = Stopwatch.StartNew();

            // Determine whether to parallelize file processing on each folder based on processor count.
            int procCount = Environment.ProcessorCount;

            // Data structure to hold names of subfolders to be examined for files.
            Stack<string> dirs = new Stack<string>();

            foreach (String currentDirectory in rootDirsToCheck)
            {
                if (!Directory.Exists(currentDirectory))
                {
                    throw new ArgumentException();
                }
                dirs.Push(currentDirectory);

                while (dirs.Count > 0)
                {
                    string currentDir = dirs.Pop();
                    string[] subDirs = { };
                    string[] files = { };

                    try
                    {
                        subDirs = Directory.GetDirectories(currentDir);

                        var subDirsToSearch = FilterSystemDirectories(subDirs.ToList());

                        foreach (var currentSubdir in subDirsToSearch)
                        {
                            action(currentSubdir);
                        }
                    }
                    // Thrown if we do not have discovery permission on the directory.
                    catch (UnauthorizedAccessException)
                    {
                        continue;
                    }
                    // Thrown if another process has deleted the directory after we retrieved its name.
                    catch (DirectoryNotFoundException)
                    {
                        continue;
                    }

                    try
                    {
                        files = Directory.GetFiles(currentDir);
                    }
                    catch (UnauthorizedAccessException)
                    {
                        continue;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        continue;
                    }
                    catch (IOException)
                    {
                        continue;
                    }

                    // Execute in parallel if there are enough files in the directory.
                    // Otherwise, execute sequentially.  Files are opened and processed
                    // synchronously but this could be modified to perform async I/O.
                    try
                    {
                        if (files.Length < procCount)
                        {
                            foreach (var file in files)
                            {
                                action(file);
                                fileCount++;
                            }
                        }
                        else
                        {
                            Parallel.ForEach(files, () => 0, (file, loopState, localCount) =>
                            {
                                action(file);
                                return (int)++localCount;
                            },
                            (c) =>
                            {
                                Interlocked.Add(ref fileCount, c);
                            });
                        }
                    }
                    catch (AggregateException ae)
                    {
                        ae.Handle((ex) =>
                        {
                            if (ex is UnauthorizedAccessException)
                            {
                                // Here we just output a message and go on.
                                Console.WriteLine(ex.Message);
                                return true;
                            }
                            // Handle other exceptions here if necessary...

                            return false;
                        });
                    }

                    // Push the subdirectories onto the stack for traversal.
                    // This could also be done before handing the files.
                    var localSubDirsToSearch = FilterSystemDirectories(subDirs.ToList());
                    foreach (string str in localSubDirsToSearch)
                    {
                        dirs.Push(str);
                    }
                }
            }

            // For diagnostic purposes.
            //var timeTaken = sw.ElapsedMilliseconds.ToString();
            //Console.WriteLine("Processed {0} files in {1} milliseconds", fileCount, timeTaken);
        }

        public static List<String> FilterSystemDirectories(List<string> rootDirsToCheck)
        {
            return rootDirsToCheck
                    .Where(x => !x.Contains("$"))
                    .Where(x => !x.Contains("__pycache__"))
                    .Where(x => !x.Contains("node_modules"))
                    .Where(x => !x.Contains(".nuget"))
                    .Where(x => !x.Contains(".dotnet"))
                    .Where(x => !x.EndsWith(".dll"))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "Windows")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "$WINDOWS.~BT")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "Windows.old")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "$Recycle.Bin")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "System Volume Information")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "Config.Msi")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "Program Files")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "Program Files (x86)")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "ProgramData")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "OneDriveTemp")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "MSOCache")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, @"\Users\All Users")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "Dell")))
                    .Where(x => !x.Contains(Path.Combine(systemRoot.FullName, "Intel")))
                    .Where(x => !x.Contains(Path.Combine(GetUserHomeDirectory(), "AppData")))
                    .ToList();

            //List<String> filteredDirectories = new List<String>();

            //foreach (var currentFilteredRootDirToCheck in filteredRootDirsToCheck)
            //{
            //    foreach (var currentRootFolderToIgnore in rootFoldersToIgnore)
            //    {
            //        if (!currentFilteredRootDirToCheck.StartsWith(currentRootFolderToIgnore))
            //        {
            //            filteredDirectories.Add(currentFilteredRootDirToCheck);
            //        }
            //    }
            //}

            //return filteredDirectories;
        }

        ///// <summary>
        ///// List all files in the given directories
        ///// Only lists files that the current process can access.
        ///// 
        ///// </summary>
        ///// <returns>A List of accessible files and directories in the given directories to search </returns>
        //public static HashSet<String> GetFilesAndDirectories(HashSet<String> directoriesToSearch)
        //{
        //    HashSet<String> foundFiles = new HashSet<String>();

        //    foreach (String currentDirectoryToSearch in directoriesToSearch)
        //    {
        //        try
        //        {
        //            DirectoryInfo directoryInfo = new DirectoryInfo(currentDirectoryToSearch);
        //            var filesInDirectory = directoryInfo.EnumerateFileSystemInfos("*", SearchOption.AllDirectories);

        //            var fileNamesInDirectory =
        //            filesInDirectory
        //                .AsParallel()
        //                .Where(x => x.Attributes != FileAttributes.Hidden)
        //                .Select(x => x.FullName)
        //                .ToList();

        //            foundFiles.UnionWith(fileNamesInDirectory);
        //            foundFiles.Add(currentDirectoryToSearch);
        //        }
        //        catch (Exception)
        //        {
        //            //Move on, we just can't see/access that directory.  
        //            //It is most likely a permissions issue, so we don't want to try and open the file.
        //        }
        //    }

        //    return foundFiles;
        //}

        /// <summary>
        /// Get all accessible directories in a given subdirectory.
        /// Only lists files that the current process can access.
        /// </summary>
        /// <param name="directoryToSearch"></param>
        /// <returns>A List of accessible files and directories in the given directory </returns>
        public static String[] GetSubdirectories(string directoryToSearch)
        {
            String[] subDirectories = { };

            try
            {
                subDirectories = Directory.GetDirectories(directoryToSearch);
            }
            catch (Exception)
            {
                //Move on, we just can't see/access that directory.  
                //It is most likely a permissions issue, so we don't want to try and open the file.
            }

            return subDirectories;
        }
    }
}
