﻿using RPCLauncher.Domain;
using RPCLauncher.Utils;
using System;

namespace RPCLauncher.Services
{
    public class MessageGenerator
    {

        /// <summary>
        /// Get all possible common uses of clipboard text.
        /// </summary>
        /// <param name="clipboardText"></param>
        /// <param name="currentLanguage"></param>
        /// <returns></returns>
        public PossibleGeneratedStatements GetPossibleGeneratedStatements(String clipboardText,
                                                                          String currentLanguage,
                                                                          LanguageStylePreference languageStylePreference)
        {

            PossibleGeneratedStatements possibleGeneratedStatements = new PossibleGeneratedStatements();
            possibleGeneratedStatements.RandomGUID = Guid.NewGuid().ToString();
            DateTimeOffset dateTimeOffset = new DateTimeOffset(DateTime.Now);

            possibleGeneratedStatements.CurrentUnixTimestamp = dateTimeOffset.ToUnixTimeSeconds().ToString();

            string trimmedClipboardText = clipboardText.Trim();

            string doubleQuoteString = '"'.ToString();

            bool hasLeadingAndTrailingDoubleQuotes =
                !String.IsNullOrWhiteSpace(trimmedClipboardText)
                    && trimmedClipboardText.StartsWith(doubleQuoteString)
                    && trimmedClipboardText.EndsWith(doubleQuoteString);

            //remove the leading and trailing quotes
            if (hasLeadingAndTrailingDoubleQuotes)
            {
                trimmedClipboardText = trimmedClipboardText.Remove(0, 1);
                trimmedClipboardText = trimmedClipboardText.Remove(trimmedClipboardText.Length - 1);
            }

            possibleGeneratedStatements.DebugStatement = GenerateDebugMessage(trimmedClipboardText, currentLanguage, languageStylePreference);
            possibleGeneratedStatements.ObjectDeclaration = GenerateObjectDeclaration(trimmedClipboardText, currentLanguage);

            bool isFilePathTextInClipboardText =
                trimmedClipboardText.Length >= 2
                    &&
                trimmedClipboardText[1].Equals(':')
                    &&
                (trimmedClipboardText[2].ToString().Equals(@"\")
                    || trimmedClipboardText[2].Equals('/'));

            if (isFilePathTextInClipboardText)
            {
                possibleGeneratedStatements.CygwinConvertedPath = convertToCygwinPath(trimmedClipboardText);
            }

            if (isValidName(trimmedClipboardText))
            {
                possibleGeneratedStatements.SqlSelectStatement = GenerateSqlSelectStatement(trimmedClipboardText);
                possibleGeneratedStatements.SqlInsertStatement = GenerateSqlInsertStatement(trimmedClipboardText);
                possibleGeneratedStatements.SqlUpdateStatement = GenerateSqlUpdateStatement(trimmedClipboardText);
            }

            return possibleGeneratedStatements;
        }

        public String GenerateSqlUpdateStatement(String tableName)
        {
            String generatedSqlStatement =
                "UPDATE " + tableName +
                Environment.NewLine +
                "SET " +
                Environment.NewLine +
                "WHERE = ";

            return SqlUtils.PrettyPrintSql(generatedSqlStatement);
        }

        public String GenerateSqlInsertStatement(String tableName)
        {
            String generatedSqlStatement =
                "INSERT INTO " + tableName +
                Environment.NewLine +
                " () " +
                Environment.NewLine +
                " VALUES()";

            return SqlUtils.PrettyPrintSql(generatedSqlStatement);
        }

        public String GenerateSqlSelectStatement(String tableName)
        {
            return SqlUtils.PrettyPrintSql("SELECT * FROM " + tableName);
        }

        /// <summary>
        /// Converts a given full Windows file path to the equivalent Cygwin file path
        /// </summary>
        /// <param name="pathToConvert"></param>
        /// <returns>The converted path, "" if invalid or empty</returns>
        public String convertToCygwinPath(String pathToConvert)
        {
            string cygwinConvertedPath = "";

            if (!String.IsNullOrWhiteSpace(pathToConvert))
            {
                cygwinConvertedPath = pathToConvert.Trim();

                cygwinConvertedPath =
                    "/cygdrive/" +
                    cygwinConvertedPath
                        .Replace(":", "")
                        .Replace(@"\", "/")
                        .Replace(@"\", "/");

                //wrap the path in quotes if we have a space in the path.
                if (cygwinConvertedPath.Contains(" "))
                {
                    cygwinConvertedPath = '"' + cygwinConvertedPath + '"';
                }

            }

            return cygwinConvertedPath;
        }

        /// <summary>
        /// Build a debug message in the given logging type eg
        /// 
        /// logger.debug("foo = " + foo);
        /// For those times you are stuck printing values like a plebian
        /// 
        /// </summary>
        /// <param name="variableName"></param>
        /// <param name="loggingType"></param>
        /// <returns></returns>
        public String GenerateDebugMessage(String variableName, String loggingType, LanguageStylePreference languageStylePreference)
        {
            string debugMessage = "";

            string variableToPrint = variableName;
            if (String.IsNullOrWhiteSpace(variableName))
            {
                variableToPrint = "";
            }

            if (!String.IsNullOrWhiteSpace(loggingType))
            {
                //Replace " with the literal escaped value \" and ' with \' in the string to log
                String escapedVariableName =
                    variableName
                        .Replace('"'.ToString(), @"\" + '"')
                        .Replace("'", @"\" + "'")
                        .Replace(";", "");

                //Should appear as "variableName = " + variableName
                string doubleQuotedTextToPrint = '"' + escapedVariableName + " = \" + " + variableName;

                // For languages that support it, we will use the second argument with the variable
                // value so we can use better formatting/conversion to a string.
                //The best example is in JS the browser when it will just show [object, object]
                string doubleQuotedTextToPrintWithSecondArgument =
                    ('"' + escapedVariableName + " = \", " + variableName)
                    .Replace(";", "");

                switch (loggingType)
                {
                    case Languages.C_SHARP:
                        debugMessage = "Console.WriteLine(" + doubleQuotedTextToPrint + ");";
                        break;

                    case Languages.JAVA:

                        if (languageStylePreference.AvailableLibraries != null
                                && languageStylePreference.AvailableLibraries.Contains(JavaLibraryChoices.LOG_4_J))
                        {
                            debugMessage = "logger.debug(" + doubleQuotedTextToPrint + ");";
                        }
                        else
                        {
                            debugMessage = "System.out.println(" + doubleQuotedTextToPrint + ");";
                        }

                        break;

                    case Languages.JAVASCRIPT:
                        debugMessage = "console.log(" + doubleQuotedTextToPrintWithSecondArgument + ");";
                        break;

                    case Languages.PYTHON:
                        debugMessage = "print(" + doubleQuotedTextToPrintWithSecondArgument + ")";
                        break;
                }
            }

            return debugMessage;
        }

        /// <summary>
        /// Build a declartion to declare a new object in the given language.
        /// 
        /// eg
        /// WorldDestroyer worldDestroyer = new WorldDestroyer();
        /// 
        /// </summary>
        /// <param name="className"></param>
        /// <param name="languageName"></param>
        /// <returns></returns>
        public String GenerateObjectDeclaration(String className, String languageName)
        {
            string declarationStatement = "";

            if (!String.IsNullOrWhiteSpace(className)
                && !String.IsNullOrWhiteSpace(languageName)
                && isValidName(className))
            {
                //Capitalize the first letter
                string objectName = "";

                if (!String.IsNullOrWhiteSpace(className) && className.Length >= 1)
                {
                    objectName = className[0].ToString().ToLower() + className.Substring(1);
                }
                else
                {
                    objectName = className[0].ToString().ToLower();
                }

                string defaultDeclarationMessage = className + " " + objectName + " = new " + className + "();";

                switch (languageName)
                {
                    //Purposeful fallthrough.  
                    //This will likely change some but a switch gives us more flexibility.
                    case Languages.C_SHARP:
                    case Languages.JAVA:
                    case Languages.JAVASCRIPT:
                    default:
                        declarationStatement = defaultDeclarationMessage;
                        break;

                    case Languages.PYTHON:
                        declarationStatement = objectName + " = " + className + "()";
                        break;
                }
            }

            return declarationStatement;
        }

        /// <summary>
        /// Determines whether the given name is a valid variable, class or table name.
        /// These are best-practice guidelines for validation in most languages and databases
        /// Edge-cases will most likely occur, however a sane developer should find them rare.
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns>bool representing status of name validity</returns>
        public bool isValidName(String name)
        {
            //Almost all languages and databases enforce the following naming rules.  
            //Even if they didn't, these rules represent sane practices in most cases.  

            //Rules for names:
            //Can't be null or empty
            //Can't start with a number
            //Can't contain any char outside of alphaNumeric
            //Can't be a reserved word

            //For now let's just worry about the first 3

            bool isValidName = false;

            if (!String.IsNullOrWhiteSpace(name))
            {
                bool isFirstPositionAlpha = Char.IsLetter(name[0]);

                bool isOnlyAlphaNumeric = false;
                if (isFirstPositionAlpha)
                {
                    foreach (char currentChar in name.ToCharArray())
                    {
                        if (!Char.IsLetterOrDigit(currentChar))
                        {
                            isOnlyAlphaNumeric = false;
                            break;
                        }

                        isOnlyAlphaNumeric = true;
                    }
                }

                isValidName = isFirstPositionAlpha && isOnlyAlphaNumeric;
            }

            return isValidName;
        }

    }

}
