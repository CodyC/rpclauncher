﻿using RPCLauncher.Domain;
using System;
using System.Collections.Generic;

namespace RPCLauncher.Services
{
    public class DataGridEntryService
    {
        public List<DataGridEntry> GetEntries(List<String> itemsToConvert)
        {
            var quickPasteItems = new List<DataGridEntry>();

            if (itemsToConvert != null)
            {
                foreach (String currentString in itemsToConvert)
                {
                    var currentQuickPasteItem = new DataGridEntry();
                    currentQuickPasteItem.DataGridItem = currentString;

                    quickPasteItems.Add(currentQuickPasteItem);
                }
            }

            return quickPasteItems;
        }

        public List<String> GetItems(ICollection<DataGridEntry> itemsToConvert)
        {
            var quickPasteItems = new List<String>();

            if (itemsToConvert != null)
            {
                foreach (DataGridEntry currentQuickPasteEntry in itemsToConvert)
                {
                    if (currentQuickPasteEntry != null)
                    {
                        quickPasteItems.Add(currentQuickPasteEntry.DataGridItem);
                    }
                }
            }

            return quickPasteItems;
        }
    }
}
