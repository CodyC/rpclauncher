﻿using RPCLauncher.Domain;
using RPCLauncher.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RPCLauncher.Services
{
    public class HighlightService
    {
        private const String HIGHLIGHTS_SAVE_DIR_RELATIVE_PATH = "./Highlights/";
        private const String HIGHLIGHTS_JSON_RELATIVE_PATH = HIGHLIGHTS_SAVE_DIR_RELATIVE_PATH + "Highlights.json";
        private const String HIGHLIGHTS_EXPORT_FORMAT_RELATIVE_PATH = HIGHLIGHTS_SAVE_DIR_RELATIVE_PATH + "HighlightsExportFormat.json";

        public List<DisplayableHighlight> GetDisplayableHighlights()
        {
            var gridDisplayableHighlights = new List<DisplayableHighlight>();

            var highlightsList = GetHighlights();

            if (highlightsList != null)
            {
                foreach (Highlight currentHightlight in highlightsList)
                {
                    var gridDisplayableHighlight =
                        new DisplayableHighlight(currentHightlight);

                    gridDisplayableHighlights.Add(gridDisplayableHighlight);
                }
            }

            return gridDisplayableHighlights;
        }

        public List<Highlight> GetHighlights()
        {
            var highlightsList = new List<Highlight>();

            bool highlightsFileExists = File.Exists(HIGHLIGHTS_JSON_RELATIVE_PATH);
            if (highlightsFileExists)
            {
                String highlightsJsonFileText =
                    File.ReadAllText(HIGHLIGHTS_JSON_RELATIVE_PATH);

                highlightsList =
                    JSONUtils.Deserialize<List<Highlight>>(highlightsJsonFileText);
            }

            return highlightsList;
        }

        /// <summary>
        /// Appends the given highlight to the backing json file for storage.
        /// </summary>
        /// <param name="highlightToSave"></param>
        public void AppendHighlightToSaveFile(Highlight highlightToSave)
        {
            var highlightsList = new List<Highlight>();

            //If it exists, read the existing JSON file into a variable
            bool highlightsFileExists = File.Exists(HIGHLIGHTS_JSON_RELATIVE_PATH);

            if (highlightsFileExists)
            {
                String highlightsJsonFileText =
                    File.ReadAllText(HIGHLIGHTS_JSON_RELATIVE_PATH);

                highlightsList =
                    JSONUtils.Deserialize<List<Highlight>>(highlightsJsonFileText);
            }

            //Add the new highlightToSave to the list
            highlightsList.Add(highlightToSave);

            SaveHighlightsToFile(highlightsList);
        }

        /// <summary>
        /// Saves the given highlights to the backing JSON file,
        /// Overwrites the existing file each time.
        /// </summary>
        /// <param name="highlightsList"></param>
        public void SaveHighlightsToFile(List<Highlight> highlightsList)
        {
            //Serialize the object
            String highlightsListToSaveJson = JSONUtils.Serialize(highlightsList);

            //Pretty print it
            String prettyPrintedJSONToSave =
                JSONUtils.PrettyPrintJSON(highlightsListToSaveJson);

            createHighlightsSaveDirectory();

            //Overwrite the file with the new contents.
            File.WriteAllText(HIGHLIGHTS_JSON_RELATIVE_PATH, prettyPrintedJSONToSave);
        }

        /// <summary>
        /// Creates the default directory to save 
        /// the highlights backing file
        /// </summary>
        private static void createHighlightsSaveDirectory()
        {
            if (!Directory.Exists(HIGHLIGHTS_SAVE_DIR_RELATIVE_PATH))
            {
                Directory.CreateDirectory(HIGHLIGHTS_SAVE_DIR_RELATIVE_PATH);
            }
        }

        /// <summary>
        /// Returns a valid deafult format string for exporting highlights
        /// </summary>
        /// <returns></returns>
        public String GetDefaultHighlightExportFormat()
        {
            return
                "Id:" + HighlightExportFormattingReplacementTokens.ID_REPLACEMENT_TOKEN +
                Environment.NewLine +
                "Timestamp:" + HighlightExportFormattingReplacementTokens.TIMESTAMP_REPLACEMENT_TOKEN +
                Environment.NewLine +
                "Highlight:" + HighlightExportFormattingReplacementTokens.HIGHLIGHT_REPLACEMENT_TOKEN +
                Environment.NewLine +
                "Notes:" + HighlightExportFormattingReplacementTokens.NOTES_REPLACEMENT_TOKEN +
                Environment.NewLine +
                "Source:" + HighlightExportFormattingReplacementTokens.SOURCE_REPLACEMENT_TOKEN +
                Environment.NewLine +
                "Tags:" + HighlightExportFormattingReplacementTokens.TAGS_REPLACEMENT_TOKEN +
                Environment.NewLine +
                "################################################" +
                Environment.NewLine;
        }

        /// <summary>
        /// Builds a Sring of the provided highlights 
        /// according to the given export format
        /// </summary>
        /// <param name="highlightsToExport"></param>
        /// <param name="exportFormat"></param>
        /// <returns>Exported String of formatted highlights</returns>
        public String ExportHighlights(List<DisplayableHighlight> highlightsToExport,
                                       String exportFormat)
        {
            String exportedHighlights = "";
            StringBuilder exportedHighlightsStringBuilder = new StringBuilder();

            if (highlightsToExport != null)
            {
                foreach (DisplayableHighlight currentHighlight in highlightsToExport)
                {
                    String currentExportedHighlightText =
                        exportFormat
                            .Replace(HighlightExportFormattingReplacementTokens.ID_REPLACEMENT_TOKEN, currentHighlight.Id)
                            .Replace(HighlightExportFormattingReplacementTokens.TIMESTAMP_REPLACEMENT_TOKEN, currentHighlight.UnixTimestamp.ToString())
                            .Replace(HighlightExportFormattingReplacementTokens.HIGHLIGHT_REPLACEMENT_TOKEN, currentHighlight.HighlightText)
                            .Replace(HighlightExportFormattingReplacementTokens.NOTES_REPLACEMENT_TOKEN, currentHighlight.Notes)
                            .Replace(HighlightExportFormattingReplacementTokens.SOURCE_REPLACEMENT_TOKEN, currentHighlight.Source)
                            .Replace(HighlightExportFormattingReplacementTokens.TAGS_REPLACEMENT_TOKEN, currentHighlight.DisplayableTags);

                    exportedHighlightsStringBuilder.Append(currentExportedHighlightText);
                }

                exportedHighlights = exportedHighlightsStringBuilder.ToString();
            }

            return exportedHighlights;
        }

        /// <summary>
        /// Get the format text that is either the user defined saved version
        /// or the default
        /// </summary>
        /// <returns>String holding the export format text</returns>
        public String GetExportFormatText()
        {
            String exportFormatText = "";

            if (File.Exists(HIGHLIGHTS_EXPORT_FORMAT_RELATIVE_PATH))
            {
                exportFormatText = File.ReadAllText(HIGHLIGHTS_EXPORT_FORMAT_RELATIVE_PATH);
            }

            if (String.IsNullOrWhiteSpace(exportFormatText))
            {
                exportFormatText = GetDefaultHighlightExportFormat();
            }

            return exportFormatText;
        }

        /// <summary>
        /// Saves the given format to a backing file if it differs from
        /// the default format.
        /// </summary>
        /// <param name="formattingText"></param>
        public void SaveHighlightExportFormat(String formattingText)
        {
            String defaultFormattingText = GetDefaultHighlightExportFormat();

            bool formattingTextHasBeenChanged =
                !String.IsNullOrWhiteSpace(formattingText)
                    && formattingText != defaultFormattingText;

            if (formattingTextHasBeenChanged)
            {
                //Save the user specified formatting to a file
                createHighlightsSaveDirectory();

                File.WriteAllText(HIGHLIGHTS_EXPORT_FORMAT_RELATIVE_PATH, formattingText);
            }
        }

        public void DeleteHighlight(String idToDelete)
        {
            var highlights = GetHighlights();

            if (highlights != null)
            {
                highlights.RemoveAll(x => x.Id == idToDelete);
                SaveHighlightsToFile(highlights);
            }
        }
    }
}
