﻿using RPCLauncher.Utils;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RPCLauncher.Services
{
    public class ClipboardManagementService
    {
        public List<String> GetClipboardHistoryFromConfig()
        {
            String clipboardHistoryJSONValue = ConfigUtils.GetConfigValue(ConfigUtils.CLIPBOARD_HISTORY_JSON_KEY);

            List<String> loadedClipboardHistory = new List<string>();

            if (!String.IsNullOrWhiteSpace(clipboardHistoryJSONValue))
            {
                loadedClipboardHistory = JSONUtils.Deserialize<List<String>>(clipboardHistoryJSONValue);
            }

            return loadedClipboardHistory;
        }

        public void SaveClipboardHistoryToConfig(List<String> clipboardHistory)
        {
            String clipboardHistoryJson = JSONUtils.Serialize(clipboardHistory);
            ConfigUtils.UpdateSetting(ConfigUtils.CLIPBOARD_HISTORY_JSON_KEY, clipboardHistoryJson);
        }

        public void HandleClipboardChange(List<String> clipboardHistory)
        {
            if (Clipboard.ContainsText())
            {
                string clipboardText = Clipboard.GetText();

                //Remove dupes if they exist and add the new item at the top of the list
                clipboardHistory.RemoveAll(x => x.Equals(clipboardText));

                //Cap off how many items in clipboard history we will save
                const int MAX_CLIPBOARD_HISTORY_ITEMS = 50;
                if (clipboardHistory != null && clipboardHistory.Count >= MAX_CLIPBOARD_HISTORY_ITEMS)
                {
                    int itemCountToRemove = clipboardHistory.Count - MAX_CLIPBOARD_HISTORY_ITEMS;
                    clipboardHistory.RemoveRange(MAX_CLIPBOARD_HISTORY_ITEMS, itemCountToRemove);
                }

                if (!String.IsNullOrWhiteSpace(clipboardText))
                {
                    clipboardHistory.Insert(0, clipboardText);
                }
            }
        }
    }
}
