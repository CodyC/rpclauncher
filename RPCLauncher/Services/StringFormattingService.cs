﻿using RPCLauncher.Utils;
using System;
using System.Collections.Generic;

namespace RPCLauncher.Services
{
    public class CaseConversionService
    {
        public List<String> buildCaseConvertedStringsList(string stringToConvert)
        {
            var caseConvertedStrings = new List<String>();

            String constantCasedClipboardText = StringUtils.ConvertToConstantCase(stringToConvert);
            caseConvertedStrings.Add(constantCasedClipboardText);

            String convertedFromCamelCaseString = StringUtils.ConvertFromCamelCase(stringToConvert);
            caseConvertedStrings.Add(convertedFromCamelCaseString);

            String pascalCasedClipboardText = StringUtils.ConvertToPascalCase(stringToConvert);
            caseConvertedStrings.Add(pascalCasedClipboardText);

            String camelCasedClipboardText = StringUtils.ConvertToCamelCase(stringToConvert);
            caseConvertedStrings.Add(camelCasedClipboardText);

            String snakeCasedClipboardText = StringUtils.ConvertToSnakeCase(stringToConvert);
            caseConvertedStrings.Add(snakeCasedClipboardText);

            String kebabCasedClipboardText = StringUtils.ConvertToKebabCase(stringToConvert);
            caseConvertedStrings.Add(kebabCasedClipboardText);

            //Remove dupes from the generated items.
            var uniqueConvertedStrings = new HashSet<String>(caseConvertedStrings);
            caseConvertedStrings = new List<String>(uniqueConvertedStrings);

            return caseConvertedStrings;
        }
    }
}
