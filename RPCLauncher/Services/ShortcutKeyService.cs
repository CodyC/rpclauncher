﻿using RPCLauncher.GlobalMontiors;
using RPCLauncher.Utils;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RPCLauncher.Services
{
    public class ShortcutKeyService
    {
        public static Dictionary<String, Keys> shortcutKeysDictionary = ConfigUtils.GetShortcutKeysDictionary();

        private static ShortcutKeyMonitor fileSearchShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.FILE_SEARCH_COMMAND_JSON_KEY], true, true, false, false);
        public static ShortcutKeyMonitor FileSearchShortcutKey
        {
            get { return fileSearchShortcutKey; }
            set
            {
                if (fileSearchShortcutKey.Registered)
                {
                    fileSearchShortcutKey.Unregister();
                }

                fileSearchShortcutKey = value;
            }
        }

        public static ShortcutKeyMonitor highlightsShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.HIGHLIGHT_COMMAND_JSON_KEY], true, true, false, false);
        public static ShortcutKeyMonitor HighlightsShortcutKey
        {
            get { return highlightsShortcutKey; }
            set
            {
                if (highlightsShortcutKey.Registered)
                {
                    highlightsShortcutKey.Unregister();
                }

                highlightsShortcutKey = value;
            }
        }
        public static ShortcutKeyMonitor quickPasteItemsShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.QUICK_PASTE_ITEMS_COMMAND_JSON_KEY], true, true, false, false);
        public static ShortcutKeyMonitor QuickPasteItemsShortcutKey
        {
            get { return quickPasteItemsShortcutKey; }
            set
            {
                if (quickPasteItemsShortcutKey.Registered)
                {
                    quickPasteItemsShortcutKey.Unregister();
                }

                quickPasteItemsShortcutKey = value;
            }
        }
        public static ShortcutKeyMonitor appendToClipboardExecutionHotKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.APPEND_TO_CLIPBOARD_COMMAND_JSON_KEY], true, true, false, false);
        public static ShortcutKeyMonitor AppendToClipboardExecutionShortcutKey
        {
            get { return appendToClipboardExecutionHotKey; }
            set
            {
                if (appendToClipboardExecutionHotKey.Registered)
                {
                    appendToClipboardExecutionHotKey.Unregister();
                }

                appendToClipboardExecutionHotKey = value;
            }
        }
        public static ShortcutKeyMonitor generatedStatementsShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.GENERATED_STATEMENTS_COMMAND_JSON_KEY], true, true, false, false);
        public static ShortcutKeyMonitor GeneratedStatementsShortcutKey
        {
            get { return generatedStatementsShortcutKey; }
            set
            {
                if (generatedStatementsShortcutKey.Registered)
                {
                    generatedStatementsShortcutKey.Unregister();
                }

                generatedStatementsShortcutKey = value;
            }
        }
        public static ShortcutKeyMonitor clipboardHistoryShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.CLIPBOARD_HISTORY_COMMAND_JSON_KEY], true, true, false, false);
        public static ShortcutKeyMonitor ClipboardHistoryShortcutKey
        {
            get { return clipboardHistoryShortcutKey; }
            set
            {
                if (clipboardHistoryShortcutKey.Registered)
                {
                    clipboardHistoryShortcutKey.Unregister();
                }

                clipboardHistoryShortcutKey = value;
            }
        }
        public static ShortcutKeyMonitor formattingToolsShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.FORMATTING_TOOLS_COMMAND_JSON_KEY], true, true, false, false);
        public static ShortcutKeyMonitor FormattingToolsShortcutKey
        {
            get { return formattingToolsShortcutKey; }
            set
            {
                if (formattingToolsShortcutKey.Registered)
                {
                    formattingToolsShortcutKey.Unregister();
                }

                formattingToolsShortcutKey = value;
            }
        }
        public static ShortcutKeyMonitor commandExecutionShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.COMMAND_EXECUTION_JSON_KEY], true, true, false, false);
        public static ShortcutKeyMonitor CommandExecutionShortcutKey
        {
            get { return commandExecutionShortcutKey; }
            set
            {
                if (commandExecutionShortcutKey.Registered)
                {
                    commandExecutionShortcutKey.Unregister();
                }

                commandExecutionShortcutKey = value;
            }
        }

        public HashSet<ShortcutKeyMonitor> GetAllShortcutKeys()
        {
            return
                new HashSet<ShortcutKeyMonitor>()
                {
                    HighlightsShortcutKey,
                    QuickPasteItemsShortcutKey,
                    GeneratedStatementsShortcutKey,
                    ClipboardHistoryShortcutKey,
                    FileSearchShortcutKey,
                    FormattingToolsShortcutKey,
                    CommandExecutionShortcutKey,
                    AppendToClipboardExecutionShortcutKey,
                };
        }

        public void RegisterAllShortcutKeys(Control controlToRegisterWith, Dictionary<String, Keys> shortcutKeysDictionary = null)
        {
            if (shortcutKeysDictionary == null)
            {
                shortcutKeysDictionary = ConfigUtils.GetShortcutKeysDictionary();
            }

            foreach (var currentShortcutKey in GetAllShortcutKeys())
            {
                currentShortcutKey.Register(controlToRegisterWith);
            }
        }

        public void UnregisterAllShortcutKeys(Dictionary<String, Keys> shortcutKeysDictionary = null)
        {
            if (shortcutKeysDictionary == null)
            {
                shortcutKeysDictionary = ConfigUtils.GetShortcutKeysDictionary();
            }

            HighlightsShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.HIGHLIGHT_COMMAND_JSON_KEY], true, true, false, false);
            QuickPasteItemsShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.QUICK_PASTE_ITEMS_COMMAND_JSON_KEY], true, true, false, false);
            GeneratedStatementsShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.GENERATED_STATEMENTS_COMMAND_JSON_KEY], true, true, false, false);
            ClipboardHistoryShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.CLIPBOARD_HISTORY_COMMAND_JSON_KEY], true, true, false, false);
            FileSearchShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.FILE_SEARCH_COMMAND_JSON_KEY], true, true, false, false);
            FormattingToolsShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.FORMATTING_TOOLS_COMMAND_JSON_KEY], true, true, false, false);
            CommandExecutionShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.COMMAND_EXECUTION_JSON_KEY], true, true, false, false);
            AppendToClipboardExecutionShortcutKey = new ShortcutKeyMonitor(shortcutKeysDictionary[ConfigUtils.APPEND_TO_CLIPBOARD_COMMAND_JSON_KEY], true, true, false, false);

            foreach (var currentShortcutKey in GetAllShortcutKeys())
            {
                if (currentShortcutKey.Registered)
                {
                    currentShortcutKey.Unregister();
                }
            }
        }
    }
}
