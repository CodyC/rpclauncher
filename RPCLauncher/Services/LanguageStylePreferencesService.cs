﻿using RPCLauncher.Domain;
using RPCLauncher.Utils;
using System;
using System.Collections.Generic;

namespace RPCLauncher.Services
{
    public class LanguageStylePreferencesService
    {
        public void SaveSpacingStyleForSelectedLanguage(string selectedLanguage,
                                                        string spacingStyleDisplayValue,
                                                        List<LanguageStylePreference> languageStylePreferences)
        {
            LanguageStylePreference selectedLanguageStylePreference = GetLanguageStylePreferenceByLanguageName(selectedLanguage, languageStylePreferences);

            //change the brace value for that language
            var selectedSpacingStyleKey =
                SpacingStyles
                .GetAllSpacingStylesValueAndDisplayValuePairs()
                .Find(x => x.Value.Equals(spacingStyleDisplayValue))
                .Key;

            selectedLanguageStylePreference.SpacingStyleKey = selectedSpacingStyleKey;

            //serialize as JSON
            String languageStylePreferencesJSON = JSONUtils.Serialize(languageStylePreferences);

            //write to config
            ConfigUtils.UpdateSetting(ConfigUtils.LANGUAGE_STYLE_PREFERENCES_JSON_KEY, languageStylePreferencesJSON);
        }

        public void SaveBraceStyleForSelectedLanguage(string selectedLanguage,
                                                      string braceStyleDisplayValue,
                                                      List<LanguageStylePreference> languageStylePreferences)
        {
            //find its place in the language preferences list
            var selectedLanguageStylePreference = GetLanguageStylePreferenceByLanguageName(selectedLanguage, languageStylePreferences);

            //change the brace value for that language
            var selectedBraceStyleKey =
                BraceStyles
                .GetAllBraceStylesValueAndDisplayValuePairs()
                .Find(x => x.Value.Equals(braceStyleDisplayValue))
                .Key;

            selectedLanguageStylePreference.BraceStyleKey = selectedBraceStyleKey;

            //serialize as JSON
            String languageStylePreferencesJSON = JSONUtils.Serialize(languageStylePreferences);

            //write to config
            ConfigUtils.UpdateLanguageStylePreferencesJSON(languageStylePreferencesJSON);
        }

        public void SaveLibrariesUsedForSelectedLanguage(string selectedLanguage,
                                                         List<string> librariesUsed,
                                                         List<LanguageStylePreference> languageStylePreferences)
        {
            //find its place in the language preferences list
            var selectedLanguageStylePreference = GetLanguageStylePreferenceByLanguageName(selectedLanguage, languageStylePreferences);

            var currentLibrariesUsed =
                languageStylePreferences
                    .Find(x => x.LanguageName.Equals(selectedLanguage))
                    .AvailableLibraries;

            selectedLanguageStylePreference.AvailableLibraries = librariesUsed;

            //serialize as JSON
            String languageStylePreferencesJSON = JSONUtils.Serialize(languageStylePreferences);

            //write to config
            ConfigUtils.UpdateLanguageStylePreferencesJSON(languageStylePreferencesJSON);
        }

        /// <summary>
        /// Tries to find the LanguageStylePreferences object with the given name in the provided languageStylePreferences,
        /// if one is not found, an entry with that name will be created.
        /// 
        /// </summary>
        /// <param name="selectedLanguage"></param>
        /// <returns></returns>
        public LanguageStylePreference GetLanguageStylePreferenceByLanguageName(string selectedLanguage, List<LanguageStylePreference> languageStylePreferences)
        {
            //find its place in the language preferences list
            var selectedLanguageStylePreference = languageStylePreferences.Find(x => x.LanguageName.Equals(selectedLanguage));

            if (selectedLanguageStylePreference == null)
            {
                //If we don't find the language, add an entry for it.
                var currentlySelectedLanguageStylePreference = new LanguageStylePreference();
                currentlySelectedLanguageStylePreference.LanguageName = selectedLanguage;

                languageStylePreferences.Add(currentlySelectedLanguageStylePreference);

                selectedLanguageStylePreference = languageStylePreferences.Find(x => x.LanguageName.Equals(selectedLanguage));
            }

            return selectedLanguageStylePreference;
        }

        /// <summary>
        /// Find if given library is used in the supplied LanguageStylePreference
        /// </summary>
        /// <param name="languageStylePreference"></param>
        /// <param name="libraryName"></param>
        /// <returns></returns>
        public bool IsLibraryUsed(LanguageStylePreference languageStylePreference, String libraryName)
        {
            return
                languageStylePreference.AvailableLibraries != null
                        && languageStylePreference.AvailableLibraries.Contains(libraryName);
        }

    }

}
